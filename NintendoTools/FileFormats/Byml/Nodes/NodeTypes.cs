﻿namespace NintendoTools.FileFormats.Byml;

/// <summary>
/// Class that holds BYML node types.
/// </summary>
public static class NodeTypes
{
    public const byte HashDictionary       = 0x20;
    public const byte HashValueDictionary  = 0x21;

    public const byte String            = 0xA0;
    public const byte BinaryData        = 0xA1;
    public const byte AlignedBinaryData = 0xA2;

    public const byte Array                = 0xC0;
    public const byte Dictionary           = 0xC1;
    public const byte StringTable          = 0xC2;
    public const byte PathTable            = 0xC3;
    public const byte RelocatedStringTable = 0xC5;
    public const byte SingleTypedArray     = 0xC8;

    public const byte Bool = 0xD0;
    public const byte S32  = 0xD1;
    public const byte F32  = 0xD2;
    public const byte U32  = 0xD3;
    public const byte S64  = 0xD4;
    public const byte U64  = 0xD5;
    public const byte F64  = 0xD6;

    public const byte Null = 0xFF;
}