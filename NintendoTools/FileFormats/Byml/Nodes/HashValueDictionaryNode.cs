﻿namespace NintendoTools.FileFormats.Byml;

/// <summary>
/// A class for a dictionary-type node with hash keys.
/// </summary>
public class HashValueDictionaryNode : DictionaryNode
{
    /// <summary>
    /// Initializes a new instance of the <see cref="HashDictionaryNode"/> class.
    /// </summary>
    public HashValueDictionaryNode() : base(NodeTypes.HashValueDictionary)
    { }
}