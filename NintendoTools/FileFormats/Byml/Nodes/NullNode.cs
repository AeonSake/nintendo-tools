﻿namespace NintendoTools.FileFormats.Byml;

/// <summary>
/// A class for a null-type node.
/// </summary>
public class NullNode : INode
{
    /// <inheritdoc/>
    public byte Type => NodeTypes.Null;
}