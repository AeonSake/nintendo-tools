﻿using System;
using System.IO;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Byml;

/// <summary>
/// A class for parsing BYML files.
/// </summary>
public class BymlFileParser : IFileParser<BymlFile>
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public BymlFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a BYML file.");

        var bymlFile = new BymlFile
        {
            Version = reader.ReadInt16At(0x02),
            RootNode = new NullNode()
        };

        //read header
        ReadHeader(reader, out var tables, out var rootNodeOffset);
        if (rootNodeOffset == 0) return bymlFile;

        bymlFile.RootNode = ReadNode(reader, rootNodeOffset, reader.ReadByteAt(rootNodeOffset), tables);

        return bymlFile;
    }
    #endregion

    #region private methods
    //verifies that the file is a BYML file
    private static bool CanParse(FileReader reader)
    {
        if (reader.Length < 2) return false;

        switch (reader.ReadStringAt(0, 2, Encoding.ASCII))
        {
            case "BY":
                reader.IsBigEndian = true;
                return true;
            case "YB":
                reader.IsBigEndian = false;
                return true;
            default:
                return false;
        }
    }

    //parses header and tables
    private static void ReadHeader(FileReader reader, out Tables tables, out long rootNodeOffset)
    {
        tables = new Tables();

        var nameTableOffset = reader.ReadInt32At(0x04);
        var stringTableOffset = reader.ReadInt32();
        var pathTableOffset = reader.ReadInt32();
        var hasPaths = false;

        if (nameTableOffset > 0)
        {
            var type = reader.ReadByteAt(nameTableOffset);
            if (type == NodeTypes.StringTable) tables.Names = ReadStringTable(reader, reader.Position);
        }

        if (stringTableOffset > 0)
        {
            var type = reader.ReadByteAt(stringTableOffset);
            if (type == NodeTypes.StringTable) tables.Strings = ReadStringTable(reader, reader.Position);
        }

        if (pathTableOffset > 0) //only appears to exist in MarioKart 8
        {
            var type = reader.ReadByteAt(pathTableOffset);
            if (type == NodeTypes.PathTable)
            {
                hasPaths = true;
                var length = reader.ReadInt32(3);

                var offsets = new uint[length + 1];
                for (var i = 0; i < offsets.Length; ++i) offsets[i] = reader.ReadUInt32();

                tables.Paths = new PathNode[length];
                for (var i = 0; i < length; ++i)
                {
                    tables.Paths[i] = new PathNode
                    {
                        PositionX = reader.ReadSingleAt(offsets[i]),
                        PositionY = reader.ReadSingle(),
                        PositionZ = reader.ReadSingle(),
                        NormalX = reader.ReadSingle(),
                        NormalY = reader.ReadSingle(),
                        NormalZ = reader.ReadSingle()
                    };
                }
            }
        }

        rootNodeOffset = reader.ReadUInt32At(hasPaths ? 0x10 : 0x0C);
    }

    //parses a string table
    private static string[] ReadStringTable(FileReader reader, long offset)
    {
        var length = reader.ReadUInt32At(offset, 3);
        var strings = new string[length];

        var offsets = new uint[length + 1];
        for (var i = 0; i < offsets.Length; ++i) offsets[i] = reader.ReadUInt32();

        for (var i = 0; i < length; ++i) strings[i] = reader.ReadString((int) (offsets[i + 1] - offsets[i]));

        return strings;
    }

    //parses a generic node
    private static INode ReadNode(FileReader reader, long offset, byte type, Tables tables) => type switch
    {
        NodeTypes.HashDictionary      => ReadHashDictionaryNode(reader, offset, tables),
        NodeTypes.HashValueDictionary => ReadHashValueDictionaryNode(reader, offset, tables),

        NodeTypes.String            => ReadStringNode(reader, offset, tables),
        NodeTypes.BinaryData        => tables.Paths is null ? ReadBinaryNode(reader, offset) : ReadPathNode(reader, offset, tables),
        NodeTypes.AlignedBinaryData => ReadAlignedBinaryDataNode(reader, offset),

        NodeTypes.Array            => ReadArrayNode(reader, type, offset, tables),
        NodeTypes.Dictionary       => ReadDictionaryNode(reader, offset, tables),
        NodeTypes.SingleTypedArray => ReadArrayNode(reader, type, offset, tables),

        NodeTypes.Bool => ReadBoolNode(reader, offset),
        NodeTypes.S32  => ReadIntNode(reader, offset),
        NodeTypes.F32  => ReadFloatNode(reader, offset),
        NodeTypes.U32  => ReadUIntNode(reader, offset),
        NodeTypes.S64  => ReadLongNode(reader, offset),
        NodeTypes.U64  => ReadULongNode(reader, offset),
        NodeTypes.F64  => ReadDoubleNode(reader, offset),

        NodeTypes.Null => new NullNode(),
        _ => throw new InvalidDataException($"Unknown node type: 0x{type:x2}")
    };

    //parses an array node
    private static ArrayNode ReadArrayNode(FileReader reader, byte type, long offset, Tables tables)
    {
        var length = reader.ReadUInt32At(offset + 1, 3);
        var types = reader.ReadBytes((int) length);
        reader.Align(4);

        var valueOffset = reader.Position;

        var node = new ArrayNode(type);
        for (uint i = 0; i < length; ++i)
        {
            var nodeOffset = valueOffset + i * 4;

            var value = IsContainer(types[i]) ? reader.ReadUInt32At(nodeOffset) : nodeOffset;

            var childNode = ReadNode(reader, value, types[i], tables);
            node.Add(childNode);
        }

        return node;
    }

    //parses a dictionary node
    private static DictionaryNode ReadDictionaryNode(FileReader reader, long offset, Tables tables)
    {
        var length = reader.ReadUInt32At(offset + 1, 3);

        var node = new DictionaryNode();
        for (uint i = 0; i < length; ++i)
        {
            var nodeOffset = offset + 4 + i * 8;

            var name = tables.Names[reader.ReadInt32At(nodeOffset, 3)];
            var type = reader.ReadByte();
            var value = IsContainer(type) ? reader.ReadUInt32() : nodeOffset + 4;

            var childNode = ReadNode(reader, value, type, tables);
            node.Add(name, childNode);
        }

        return node;
    }

    //parses a hash dictionary node
    private static HashDictionaryNode ReadHashDictionaryNode(FileReader reader, long offset, Tables tables)
    {
        var length = reader.ReadUInt32At(offset + 1, 3);
        var types = reader.ReadBytesAt(offset + 4 + 8 * length, (int) length);

        var node = new HashDictionaryNode();
        for (uint i = 0; i < length; ++i)
        {
            var nodeOffset = offset + 4 + i * 8;
            var nodeType = types[i];

            var name = "0x" + reader.ReadHexStringAt(nodeOffset, 4);
            var value = IsContainer(nodeType) ? reader.ReadUInt32() : nodeOffset + 4;

            var childNode = ReadNode(reader, value, nodeType, tables);
            node.Add(name, childNode);
        }

        return node;
    }

    //parses a hash value dictionary node
    private static HashValueDictionaryNode ReadHashValueDictionaryNode(FileReader reader, long offset, Tables tables)
    {
        var length = reader.ReadUInt32At(offset + 1, 3);
        var types = reader.ReadBytesAt(offset + 4 + 12 * length, (int) length);

        var node = new HashValueDictionaryNode();
        for (uint i = 0; i < length; ++i)
        {
            var nodeOffset = offset + 4 + i * 8;
            var nodeType = types[i];

            var value = IsContainer(nodeType) ? reader.ReadUInt32() : nodeOffset;
            var name = "0x" + reader.ReadHexStringAt(nodeOffset + 4, 4);

            var childNode = ReadNode(reader, value, nodeType, tables);
            node.Add(name, childNode);
        }

        return node;
    }

    //parses a string value node
    private static ValueNode<string> ReadStringNode(FileReader reader, long offset, Tables tables)
    {
        return new ValueNode<string>(NodeTypes.String) {Value = tables.Strings[reader.ReadInt32At(offset)]};
    }

    //parses a path value node
    private static PathNode ReadPathNode(FileReader reader, long offset, Tables tables)
    {
        return tables.Paths![reader.ReadInt32At(offset)];
    }

    //parses a binary value node
    private static BinaryDataNode ReadBinaryNode(FileReader reader, long offset)
    {
        var size = reader.ReadInt32At(reader.ReadInt32At(offset));

        return new BinaryDataNode
        {
            Size = size,
            Data = reader.ReadBytes(size)
        };
    }

    //parses a binary param value node
    private static AlignedBinaryDataNode ReadAlignedBinaryDataNode(FileReader reader, long offset)
    {
        var size = reader.ReadInt32At(reader.ReadInt32At(offset));

        return new AlignedBinaryDataNode
        {
            Size = size,
            Alignment = reader.ReadInt32(),
            Data = reader.ReadBytes(size)
        };
    }

    //parses a bool value node
    private static ValueNode<bool> ReadBoolNode(FileReader reader, long offset)
    {
        return new ValueNode<bool>(NodeTypes.Bool) {Value = reader.ReadUInt32At(offset) == 1};
    }

    //parses an int value node
    private static ValueNode<int> ReadIntNode(FileReader reader, long offset)
    {
        return new ValueNode<int>(NodeTypes.S32) {Value = reader.ReadInt32At(offset)};
    }

    //parses a float value node
    private static ValueNode<float> ReadFloatNode(FileReader reader, long offset)
    {
        return new ValueNode<float>(NodeTypes.F32) {Value = reader.ReadSingleAt(offset)};
    }

    //parses an uint value node
    private static ValueNode<uint> ReadUIntNode(FileReader reader, long offset)
    {
        return new ValueNode<uint>(NodeTypes.U32) {Value = reader.ReadUInt32At(offset)};
    }

    //parses an long value node
    private static ValueNode<long> ReadLongNode(FileReader reader, long offset)
    {
        return new ValueNode<long>(NodeTypes.S64) {Value = reader.ReadInt64At(reader.ReadUInt32At(offset))};
    }

    //parses an ulong value node
    private static ValueNode<ulong> ReadULongNode(FileReader reader, long offset)
    {
        return new ValueNode<ulong>(NodeTypes.U64) {Value = reader.ReadUInt64At(reader.ReadUInt32At(offset))};
    }

    //parses an double value node
    private static ValueNode<double> ReadDoubleNode(FileReader reader, long offset)
    {
        return new ValueNode<double>(NodeTypes.F64) {Value = reader.ReadDoubleAt(reader.ReadUInt32At(offset))};
    }

    //checks whether a node type is a data structure/container
    private static bool IsContainer(byte type) => type is NodeTypes.HashDictionary or NodeTypes.HashValueDictionary or NodeTypes.Array or NodeTypes.Dictionary or NodeTypes.SingleTypedArray;
    #endregion

    #region helper classes
    private class Tables
    {
        public string[] Names { get; set; } = [];
        public string[] Strings { get; set; } = [];
        public PathNode[]? Paths { get; set; }
    }
    #endregion
}