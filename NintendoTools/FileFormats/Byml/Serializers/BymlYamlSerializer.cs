﻿using System;
using System.IO;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Byml;

/// <summary>
/// A class for serializing <see cref="BymlFile"/> objects to YAML.
/// </summary>
public class BymlYamlSerializer : IFileSerializer<BymlFile>
{
    #region IFileSerializer interface
    /// <inheritdoc/>
    public void Serialize(TextWriter writer, BymlFile file)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(writer, nameof(writer));
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        #else
        if (writer is null) throw new ArgumentNullException(nameof(writer));
        if (file is null) throw new ArgumentNullException(nameof(file));
        #endif

        using var yamlWriter = new YamlTextWriter(writer);

        yamlWriter.WriteStartDocument();
        yamlWriter.WritePropertyName("version");
        yamlWriter.WriteValue(file.Version);

        yamlWriter.WritePropertyName("rootNode");
        WriteNode(yamlWriter, file.RootNode);

        yamlWriter.Flush();
        yamlWriter.Close();
    }
    #endregion

    #region private methods
    //writes the YAML elements for a given node
    private static void WriteNode(YamlTextWriter writer, INode node)
    {
        switch (node)
        {
            case DictionaryNode dict:
                writer.WriteStartDictionary();
                foreach (var item in dict)
                {
                    writer.WritePropertyName(item.Key);
                    WriteNode(writer, item.Value);
                }
                writer.WriteEndDictionary();
                break;
            case ArrayNode array:
                writer.WriteStartArray();
                foreach (var item in array) WriteNode(writer, item);
                writer.WriteEndArray();
                break;
            case IValueNode value:
                writer.WriteValue(value.GetValue());
                break;
            case PathNode path:
                writer.WriteStartDictionary();
                writer.WritePropertyName("positionX");
                writer.WriteValue(path.PositionX);
                writer.WritePropertyName("positionY");
                writer.WriteValue(path.PositionY);
                writer.WritePropertyName("positionZ");
                writer.WriteValue(path.PositionZ);
                writer.WritePropertyName("normalX");
                writer.WriteValue(path.NormalX);
                writer.WritePropertyName("normalY");
                writer.WriteValue(path.NormalY);
                writer.WritePropertyName("normalZ");
                writer.WriteValue(path.NormalZ);
                writer.WriteEndDictionary();
                break;
            case BinaryDataNode binary:
                writer.WriteStartDictionary();
                writer.WritePropertyName("size");
                writer.WriteValue(binary.Size);
                if (binary is AlignedBinaryDataNode alignedBinary)
                {
                    writer.WritePropertyName("alignment");
                    writer.WriteValue(alignedBinary.Alignment);
                }
                writer.WritePropertyName("data");
                writer.WriteValue(binary.Data.ToHexString(true));
                writer.WriteEndDictionary();
                break;
            case NullNode:
            case null:
                writer.WriteNull();
                break;
        }
    }
    #endregion
}