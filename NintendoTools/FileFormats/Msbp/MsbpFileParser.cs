﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class for parsing MSBP files.
/// </summary>
public class MsbpFileParser : IFileParser<MsbpFile>
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public MsbpFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a MSBP file.");

        //parse file metadata and header
        GetMetaData(reader, out var sectionCount, out _, out var version, out var encodingType);

        //parse content
        var msbpFile = new MsbpFile
        {
            BigEndian = reader.IsBigEndian,
            Version = version,
            EncodingType = encodingType
        };

        var colors = Array.Empty<ColorDefinition>();
        var colorLabels = Array.Empty<string>();
        var attributes = Array.Empty<AttributeDefinition>();
        var attributeListIndices = Array.Empty<int>();
        var attributeLabels = Array.Empty<string>();
        var attributeValues = Array.Empty<string[]>();
        var tagGroups = Array.Empty<TagGroupDefinition>();
        var tagIndices = Array.Empty<int[]>();
        var tags = Array.Empty<TagDefinition>();
        var tagParameterIndices = Array.Empty<int[]>();
        var tagParameters = Array.Empty<TagParameterDefinition>();
        var tagParameterListIndices = Array.Empty<int[]>();
        var tagParameterValues = Array.Empty<string>();
        var styles = Array.Empty<StyleDefinition>();
        var styleLabels = Array.Empty<string>();

        long sectionOffset = 0x20;
        for (var i = 0; i < sectionCount; ++i)
        {
            reader.JumpTo(sectionOffset);
            reader.Align(16);

            var type = reader.ReadString(4, Encoding.ASCII);
            var sectionSize = reader.ReadUInt32();
            sectionOffset += 0x10 + (sectionSize + 0xF & ~0xF);

            switch (type)
            {
                case "CLR1":
                    ParseClr1(reader, out colors);
                    msbpFile.Colors = colors;
                    break;
                case "CLB1":
                    ParseLabels(reader, out colorLabels);
                    break;
                case "ATI2":
                    ParseAti2(reader, out attributes, out attributeListIndices);
                    msbpFile.Attributes = attributes;
                    break;
                case "ALB1":
                    ParseLabels(reader, out attributeLabels);
                    break;
                case "ALI2":
                    ParseAti2(reader, out attributeValues);
                    break;
                case "TGG2":
                    ParseTgg2(reader, version, out tagGroups, out tagIndices);
                    msbpFile.TagGroups = tagGroups;
                    break;
                case "TAG2":
                    ParseTag2(reader, out tags, out tagParameterIndices);
                    break;
                case "TGP2":
                    ParseTgp2(reader, out tagParameters, out tagParameterListIndices);
                    break;
                case "TGL2":
                    ParseTgl2(reader, out tagParameterValues);
                    break;
                case "SYL3":
                    ParseSyl3(reader, out styles);
                    msbpFile.Styles = styles;
                    break;
                case "SLB1":
                    ParseLabels(reader, out styleLabels);
                    break;
                case "CTI1":
                    ParseCti1(reader, out var sourceFiles);
                    msbpFile.SourceFiles = sourceFiles;
                    break;
                default:
                    throw new InvalidDataException($"Unknown section type: {type}");
            }
        }

        for (var i = 0; i < colorLabels.Length; ++i) colors[i].Name = colorLabels[i];
        for (var i = 0; i < attributeLabels.Length; ++i) attributes[i].Name = attributeLabels[i];
        for (var i = 0; i < styleLabels.Length; ++i) styles[i].Name = styleLabels[i];

        if (attributeValues.Length > 0)
        {
            for (var i = 0; i < attributes.Length; ++i)
            {
                if (attributes[i].Type != ValueType.Enum) continue;
                attributes[i].EnumValues = attributeValues[attributeListIndices[i]];
            }
        }

        if (tagParameterValues.Length > 0)
        {
            for (var i = 0; i < tagParameters.Length; ++i)
            {
                if (tagParameters[i].Type != ValueType.Enum) continue;

                var indices = tagParameterListIndices[i];
                var values = new string[indices.Length];
                for (var j = 0; j < values.Length; ++j) values[j] = tagParameterValues[indices[j]];
                tagParameters[i].EnumValues = values;
            }
        }
        for (var i = 0; i < tags.Length; ++i)
        {
            var indices = tagParameterIndices[i];
            var parameters = new TagParameterDefinition[indices.Length];
            for (var j = 0; j < parameters.Length; ++j) parameters[j] = tagParameters[indices[j]];
            tags[i].Parameters = parameters;
        }
        for (var i = 0; i < tagGroups.Length; ++i)
        {
            var indices = tagIndices[i];
            var groupTags = new TagDefinition[indices.Length];
            for (var j = 0; j < groupTags.Length; ++j) groupTags[j] = tags[indices[j]];
            tagGroups[i].Tags = groupTags;
        }

        return msbpFile;
    }
    #endregion

    #region private methods
    //verifies that the file is a MSBP file
    private static bool CanParse(FileReader reader) => reader.Length > 8 && reader.ReadStringAt(0, 8, Encoding.ASCII) == "MsgPrjBn";

    //parses meta data
    private static void GetMetaData(FileReader reader, out int sectionCount, out uint fileSize, out int version, out EncodingType encodingType)
    {
        var byteOrder = reader.ReadByteAt(8);
        if (byteOrder == 0xFE) reader.IsBigEndian = true;

        encodingType = reader.ReadByteAt(12) switch
        {
            0 => EncodingType.UTF8,
            1 => EncodingType.UTF16,
            2 => EncodingType.UTF32,
            _ => EncodingType.UTF16
        };

        version = reader.ReadByte();
        sectionCount = reader.ReadUInt16();
        fileSize = reader.ReadUInt32At(18);
    }

    //parse label sections
    private static void ParseLabels(FileReader reader, out string[] labels)
    {
        reader.Skip(8);
        var position = reader.Position;
        var entryCount = reader.ReadUInt32();

        var labelValues = new List<string>();
        var indices = new List<uint>();

        for (var i = 0; i < entryCount; ++i)
        {
            //group header
            reader.JumpTo(position + 4 + i * 8);
            var labelCount = reader.ReadUInt32();
            var offset = reader.ReadUInt32();

            //labels
            reader.JumpTo(position + offset);
            for (var j = 0; j < labelCount; ++j)
            {
                var length = reader.ReadByte();
                labelValues.Add(reader.ReadString(length));
                indices.Add(reader.ReadUInt32());
            }
        }

        labels = new string[indices.Count];
        for (var i = 0; i < indices.Count; ++i) labels[indices[i]] = labelValues[i];
    }

    //parse CLR1 sections
    private static void ParseClr1(FileReader reader, out ColorDefinition[] colors)
    {
        reader.Skip(8);
        var entryCount = reader.ReadUInt32();

        colors = new ColorDefinition[entryCount];
        for (var i = 0; i < entryCount; ++i)
        {
            colors[i] = new ColorDefinition
            {
                Red = reader.ReadByte(),
                Green = reader.ReadByte(),
                Blue = reader.ReadByte(),
                Alpha = reader.ReadByte()
            };
        }
    }

    //parse ATI2 sections
    private static void ParseAti2(FileReader reader, out AttributeDefinition[] attributes, out int[] attributeListIndices)
    {
        reader.Skip(8);
        var entryCount = reader.ReadUInt32();

        attributes = new AttributeDefinition[entryCount];
        attributeListIndices = new int[entryCount];
        for (var i = 0; i < entryCount; ++i)
        {
            var type = (ValueType) reader.ReadByte();
            reader.Skip(1);
            attributeListIndices[i] = reader.ReadUInt16();
            reader.Skip(4); //TEMP!

            attributes[i] = new AttributeDefinition
            {
                Type = type
            };
        }
    }

    //parse ALI2 section
    private static void ParseAti2(FileReader reader, out string[][] attributeValues)
    {
        reader.Skip(8);
        var position = reader.Position;
        var listCount = reader.ReadUInt32();

        attributeValues = new string[listCount][];
        for (var i = 0; i < listCount; ++i)
        {
            //list header
            reader.JumpTo(position + 4 + i * 4);
            var listOffset = reader.ReadUInt32();
            reader.JumpTo(position + listOffset);
            var valueCount = reader.ReadUInt32();

            //values
            var values = new string[valueCount];
            for (var j = 0; j < valueCount; ++j)
            {
                reader.JumpTo(position + listOffset + j * 4 + 4);
                var valueOffset = reader.ReadUInt32();
                reader.JumpTo(position + listOffset + valueOffset);
                values[j] = reader.ReadTerminatedString();
            }
            attributeValues[i] = values;
        }
    }

    //parse TGG2 sections
    private static void ParseTgg2(FileReader reader, int version, out TagGroupDefinition[] tagGroups, out int[][] tagIndices)
    {
        reader.Skip(8);
        var position = reader.Position;
        var entryCount = reader.ReadUInt16();

        tagGroups = new TagGroupDefinition[entryCount];
        tagIndices = new int[entryCount][];
        for (var i = 0; i < entryCount; ++i)
        {
            //group header
            reader.JumpTo(position + 4 + i * 4);
            var groupOffset = reader.ReadUInt32();
            reader.JumpTo(position + groupOffset);

            //version 4+ includes the group index
            var groupIndex = i;
            if (version > 3) groupIndex = reader.ReadUInt16();

            //tags
            var tagCount = reader.ReadUInt16();
            var indices = new int[tagCount];
            for (var j = 0; j < tagCount; ++j) indices[j] = reader.ReadUInt16();
            tagIndices[i] = indices;

            tagGroups[groupIndex] = new TagGroupDefinition
            {
                Name = reader.ReadTerminatedString()
            };
        }
    }

    //parse TAG2 sections
    private static void ParseTag2(FileReader reader, out TagDefinition[] tags, out int[][] tagParameterIndices)
    {
        reader.Skip(8);
        var position = reader.Position;
        var entryCount = reader.ReadUInt16();

        tags = new TagDefinition[entryCount];
        tagParameterIndices = new int[entryCount][];
        for (var i = 0; i < entryCount; ++i)
        {
            //tag header
            reader.JumpTo(position + 4 + i * 4);
            var groupOffset = reader.ReadUInt32();
            reader.JumpTo(position + groupOffset);

            //parameters
            var tagCount = reader.ReadUInt16();
            var indices = new int[tagCount];
            for (var j = 0; j < tagCount; ++j) indices[j] = reader.ReadUInt16();
            tagParameterIndices[i] = indices;

            tags[i] = new TagDefinition
            {
                Name = reader.ReadTerminatedString()
            };
        }
    }

    //parse TGP2 sections
    private static void ParseTgp2(FileReader reader, out TagParameterDefinition[] tagParameters, out int[][] tagParameterListIndices)
    {
        reader.Skip(8);
        var position = reader.Position;
        var entryCount = reader.ReadUInt16();

        tagParameters = new TagParameterDefinition[entryCount];
        tagParameterListIndices = new int[entryCount][];
        for (var i = 0; i < entryCount; ++i)
        {
            reader.JumpTo(position + 4 + i * 4);
            var parameterOffset = reader.ReadUInt32();
            reader.JumpTo(position + parameterOffset);

            var type = (ValueType) reader.ReadByte();
            if (type == ValueType.Enum)
            {
                reader.Skip(1);
                var valueCount = reader.ReadUInt16();
                var valueIndices = new int[valueCount];
                for (var j = 0; j < valueCount; ++j) valueIndices[j] = reader.ReadUInt16();
                tagParameterListIndices[i] = valueIndices;
            }

            tagParameters[i] = new TagParameterDefinition
            {
                Name = reader.ReadTerminatedString(),
                Type = type
            };
        }
    }

    //parse TGL2 section
    private static void ParseTgl2(FileReader reader, out string[] tagParameterValues)
    {
        reader.Skip(8);
        var position = reader.Position;
        var entryCount = reader.ReadUInt32();

        tagParameterValues = new string[entryCount];
        for (var i = 0; i < entryCount; ++i)
        {
            reader.JumpTo(position + 4 + i * 4);
            var valueOffset = reader.ReadUInt32();
            reader.JumpTo(position + valueOffset);
            tagParameterValues[i] = reader.ReadTerminatedString();
        }
    }

    //parse SYL3 section
    private static void ParseSyl3(FileReader reader, out StyleDefinition[] styles)
    {
        reader.Skip(8);
        var entryCount = reader.ReadUInt32();

        styles = new StyleDefinition[entryCount];
        for (var i = 0; i < entryCount; ++i)
        {
            styles[i] = new StyleDefinition
            {
                RegionWidth = reader.ReadUInt32(),
                LineNumber = reader.ReadUInt32(),
                FontIndex = reader.ReadInt32(),
                BaseColorIndex = reader.ReadInt32()
            };
        }
    }

    //parse CTI1 sections
    private static void ParseCti1(FileReader reader, out string[] sourceFiles)
    {
        reader.Skip(8);
        var position = reader.Position;
        var entryCount = reader.ReadUInt32();

        sourceFiles = new string[entryCount];
        for (var i = 0; i < entryCount; ++i)
        {
            reader.JumpTo(position + 4 + i * 4);
            var fileNameOffset = reader.ReadUInt32();
            reader.JumpTo(position + fileNameOffset);
            sourceFiles[i] = reader.ReadTerminatedString();
        }
    }
    #endregion
}