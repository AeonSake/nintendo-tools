﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class representing a color definition entry in a MSBP file.
/// </summary>
public class ColorDefinition
{
    /// <summary>
    /// The name of the color.
    /// Only set if the MSBP file contained a CLB1 section.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// The red channel value of the color.
    /// </summary>
    public byte Red { get; set; }

    /// <summary>
    /// The green channel value of the color.
    /// </summary>
    public byte Green { get; set; }

    /// <summary>
    /// The blue channel value of the color.
    /// </summary>
    public byte Blue { get; set; }

    /// <summary>
    /// The alpha channel value of the color.
    /// </summary>
    public byte Alpha { get; set; }
}