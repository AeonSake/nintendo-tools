﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class representing a tag group definition entry in a MSBP file.
/// </summary>
public class TagGroupDefinition
{
    /// <summary>
    /// The name of the tag group.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// The list of tags in this tag group.
    /// </summary>
    public TagDefinition[] Tags { get; set; } = [];
}