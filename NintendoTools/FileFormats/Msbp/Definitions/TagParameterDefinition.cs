﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class representing a tag parameter definition entry in a MSBP file.
/// </summary>
public class TagParameterDefinition
{
    /// <summary>
    /// The name of the tag parameter.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// The data type of the tag parameter.
    /// </summary>
    public ValueType Type { get; set; }

    /// <summary>
    /// The list of valid enum values.
    /// Only set if the type is <see cref="ValueType.Enum"/>.
    /// </summary>
    public string[] EnumValues { get; set; } = [];
}