﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class representing a style definition entry in a MSBP file.
/// </summary>
public class StyleDefinition
{
    /// <summary>
    /// The name of the tag parameter.
    /// Only set if the MSBP file contained a SLB1 section.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    public uint RegionWidth { get; set; }

    public uint LineNumber { get; set; }

    public int FontIndex { get; set; }

    public int BaseColorIndex { get; set; }
}