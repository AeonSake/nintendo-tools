﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class representing a tag definition entry in a MSBP file.
/// </summary>
public class TagDefinition
{
    /// <summary>
    /// The name of the tag.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// The list of parameters of the tag.
    /// </summary>
    public TagParameterDefinition[] Parameters { get; set; } = [];
}