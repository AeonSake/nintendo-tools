﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// An <see langword="enum"/> of valid types for attribute and tag parameter values.
/// </summary>
public enum ValueType : byte
{
    /// <summary>
    /// A single byte.
    /// </summary>
    Byte = 0,

    /// <summary>
    /// An unsigned 16bit integer.
    /// </summary>
    UInt16 = 1,

    /// <summary>
    /// An unsigned 32bit integer.
    /// </summary>
    UInt32 = 2,

    /// <summary>
    /// A signed single byte.
    /// </summary>
    SByte = 3,

    /// <summary>
    /// A signed 16bit integer.
    /// </summary>
    Int16 = 4,

    /// <summary>
    /// A signed 32bit integer.
    /// </summary>
    Int32 = 5,

    /// <summary>
    /// A 32bit floating point number.
    /// </summary>
    Single = 6,

    //?? = 7,

    /// <summary>
    /// A string value.
    /// Encoded as 2 bytes for the length followed by the string in the encoding of the file.
    /// </summary>
    String = 8,

    /// <summary>
    /// An enum encoded as a single byte.
    /// Valid enum values are provided in <see cref="AttributeDefinition.EnumValues"/> or <see cref="TagParameterDefinition.EnumValues"/>.
    /// </summary>
    Enum = 9

    //maybe more?
}