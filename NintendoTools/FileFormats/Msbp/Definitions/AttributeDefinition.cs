﻿namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class representing an attribute definition entry in a MSBP file.
/// </summary>
public class AttributeDefinition
{
    /// <summary>
    /// The name of the attribute.
    /// Only set if the MSBP file contained a ALB1 section.
    /// </summary>
    public string Name { get; set; } = string.Empty;

    /// <summary>
    /// The data type of the attribute.
    /// </summary>
    public ValueType Type { get; set; }

    /// <summary>
    /// The list of valid enum values.
    /// Only set if the type is <see cref="ValueType.Enum"/>.
    /// </summary>
    public string[] EnumValues { get; set; } = [];
}