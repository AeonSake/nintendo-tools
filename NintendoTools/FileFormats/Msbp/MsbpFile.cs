﻿using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Msbp;

/// <summary>
/// A class holding information about a MSBP file.
/// </summary>
public class MsbpFile : IVariableEndianFile, IEncodedFile
{
    #region private members
    private bool _bigEndian;
    private EncodingType _encodingType = EncodingType.UTF8;
    #endregion

    /// <inheritdoc/>
    public bool BigEndian
    {
        get => _bigEndian;
        set
        {
            Encoding = EncodingList.GetEncoding(_encodingType, value);
            _bigEndian = value;
        }
    }

    /// <summary>
    /// Gets the version of the MSBT file.
    /// </summary>
    public int Version { get; set; }

    /// <summary>
    /// <inheritdoc/>
    /// Only <see cref="EncodingType.UTF8"/>, <see cref="EncodingType.UTF16"/>, and <see cref="EncodingType.UTF32"/> are valid for MSBP.
    /// </summary>
    public EncodingType EncodingType
    {
        get => _encodingType;
        set
        {
            Encoding = EncodingList.GetEncoding(value, _bigEndian);
            _encodingType = value;
        }
    }

    /// <inheritdoc/>
    public Encoding Encoding { get; private set; } = EncodingList.UTF8;

    /// <summary>
    /// The list of color definitions defined in a CLR1 section.
    /// </summary>
    public ColorDefinition[] Colors { get; set; } = [];

    /// <summary>
    /// The list of attribute definitions defined in a ATI2 section.
    /// </summary>
    public AttributeDefinition[] Attributes { get; set; } = [];

    /// <summary>
    /// The list of tag group definitions defined in a TGG2 section.
    /// </summary>
    public TagGroupDefinition[] TagGroups { get; set; } = [];

    /// <summary>
    /// The list of style definitions defined in a SYL3 section.
    /// </summary>
    public StyleDefinition[] Styles { get; set; } = [];

    /// <summary>
    /// The list of source files used to create the MSBT files defined in a CTI1 section.
    /// </summary>
    public string[] SourceFiles { get; set; } = [];
}