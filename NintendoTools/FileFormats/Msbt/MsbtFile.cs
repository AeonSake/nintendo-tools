﻿using System.Collections.Generic;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Msbt;

/// <summary>
/// A class holding information about a MSBT file.
/// </summary>
public class MsbtFile : IVariableEndianFile, IEncodedFile
{
    #region private members
    private bool _bigEndian;
    private EncodingType _encodingType = EncodingType.UTF8;
    #endregion

    /// <inheritdoc/>
    public bool BigEndian
    {
        get => _bigEndian;
        set
        {
            Encoding = EncodingList.GetEncoding(_encodingType, value);
            _bigEndian = value;
        }
    }

    /// <summary>
    /// Gets the version of the MSBT file.
    /// </summary>
    public int Version { get; set; }

    /// <summary>
    /// <inheritdoc/>
    /// Only <see cref="EncodingType.UTF8"/>, <see cref="EncodingType.UTF16"/>, and <see cref="EncodingType.UTF32"/> are valid for MSBT.
    /// </summary>
    public EncodingType EncodingType
    {
        get => _encodingType;
        set
        {
            Encoding = EncodingList.GetEncoding(value, _bigEndian);
            _encodingType = value;
        }
    }

    /// <inheritdoc/>
    public Encoding Encoding { get; private set; } = EncodingList.UTF8;

    /// <summary>
    /// Whether the file contains a NLI1 section.
    /// </summary>
    public bool HasNli1 { get; set; }

    /// <summary>
    /// Whether the file contains a LBL1 section.
    /// </summary>
    public bool HasLbl1 { get; set; }

    /// <summary>
    /// The number of label groups, if the file contains a LBL1 section.
    /// If the value is set to <c>0</c>, the number of label groups is calculated automatically.
    /// </summary>
    public int LabelGroups { get; set; }

    /// <summary>
    /// Whether the file contains an ATR1 section.
    /// </summary>
    public bool HasAtr1 { get; set; }

    /// <summary>
    /// Additional data in the ATR1 section that was not identified.
    /// </summary>
    public byte[] AdditionalAttributeData { get; set; } = [];

    /// <summary>
    /// Whether the file contains an ATO1 section.
    /// </summary>
    public bool HasAto1 { get; set; }

    /// <summary>
    /// The data from the ATO1 section.
    /// Only present if the file has an ATO1 section.
    /// </summary>
    public byte[] Ato1Data { get; set; } = [];

    /// <summary>
    /// Whether the file contains a TSY1 section.
    /// </summary>
    public bool HasTsy1 { get; set; }

    /// <summary>
    /// Whether the file uses TXTW instead of TXT2.
    /// </summary>
    public bool HasTxtW { get; set; }

    /// <summary>
    /// The list of messages in the MSBT file.
    /// </summary>
    public List<MsbtMessage> Messages { get; set; } = [];
}