﻿using System.Diagnostics.CodeAnalysis;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Msbt;

/// <summary>
/// A class containing information about a padding data type.
/// </summary>
public class PaddingDataType : DataType
{
    /// <summary>
    /// Initializes a new instance of the <see cref="PaddingDataType"/> class.
    /// </summary>
    /// <param name="value">The padding value to use.</param>
    [SetsRequiredMembers]
    public PaddingDataType(byte[] value)
    {
        PaddingValue = value;
        Name = value.ToHexString(true);
        Length = value.Length;
        Serialize = (_, _, _) => value;
        Deserialize = (_, _, _, _) => (string.Empty, value.Length);
    }

    /// <summary>
    /// Gets the value used for padding.
    /// </summary>
    public byte[] PaddingValue { get; }
}