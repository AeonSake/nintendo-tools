﻿namespace NintendoTools.FileFormats;

/// <summary>
/// An enum of different newline encoding types.
/// </summary>
// ReSharper disable InconsistentNaming
public enum NewlineType
{
    /// <summary>
    /// Single line feed (\n) character.
    /// </summary>
    LF,

    /// <summary>
    /// Combination of carriage return and line feed (\r\n) characters.
    /// </summary>
    CRLF,

    /// <summary>
    /// Single carriage return (\r) character.
    /// </summary>
    CR
}