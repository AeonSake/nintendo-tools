﻿using System.Text;

namespace NintendoTools.FileFormats;

/// <summary>
/// An interface for files that require encoding.
/// </summary>
public interface IEncodedFile
{
    /// <summary>
    /// The encoding type used for the file.
    /// </summary>
    public EncodingType EncodingType { get; set; }

    /// <summary>
    /// Gets the encoding used for the file.
    /// </summary>
    public Encoding Encoding { get; }
}