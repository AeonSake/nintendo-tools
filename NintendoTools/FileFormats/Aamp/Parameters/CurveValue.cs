﻿namespace NintendoTools.FileFormats.Aamp;

/// <summary>
/// A class representing a curve parameter value in an AAMP file.
/// </summary>
public class CurveValue
{
    //???
    public required uint[] IntValues { get; set; }

    //???
    public required float[] FloatValues { get; set; }
}