﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Aamp;

/// <summary>
/// A class representing a parameter list in an AAMP file.
/// </summary>
public class ParameterList
{
    #region public properties
    /// <summary>
    /// Gets or sets the name of the list.
    /// </summary>
    public required string Name { get; set; }

    /// <summary>
    /// Gets a list of <see cref="ParameterObject"/> instances.
    /// </summary>
    public List<ParameterObject> Objects { get; } = [];

    /// <summary>
    /// Gets a list of <see cref="ParameterList"/> instances.
    /// </summary>
    public List<ParameterList> Lists { get; } = [];
    #endregion
}