﻿using System.Collections.Generic;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Bmg;

/// <summary>
/// A class holding information about a BMG file.
/// </summary>
public class BmgFile : IVariableEndianFile, IEncodedFile
{
    #region private members
    private bool _bigEndian;
    private EncodingType _encodingType = EncodingType.UTF8;
    #endregion

    /// <inheritdoc/>
    public bool BigEndian
    {
        get => _bigEndian;
        set
        {
            Encoding = EncodingList.GetEncoding(_encodingType, value);
            _bigEndian = value;
        }
    }

    /// <summary>
    /// Whether the file uses big endian magic and section labels.
    /// </summary>
    public bool BigEndianLabels { get; set; }

    /// <summary>
    /// <inheritdoc/>
    /// Only <see cref="EncodingType.Latin1"/>, <see cref="EncodingType.ShiftJIS"/>, <see cref="EncodingType.UTF8"/> and <see cref="EncodingType.UTF16"/> are valid for BMG.
    /// </summary>
    public EncodingType EncodingType
    {
        get => _encodingType;
        set
        {
            Encoding = EncodingList.GetEncoding(value, _bigEndian);
            _encodingType = value;
        }
    }

    /// <inheritdoc/>
    public Encoding Encoding { get; private set; } = EncodingList.Latin1;

    /// <summary>
    /// The ID of the BMG file. Usually unused.
    /// </summary>
    public int FileId { get; set; }

    /// <summary>
    /// The ID of the default text color.
    /// </summary>
    public int DefaultColor { get; set; }

    /// <summary>
    /// Whether the file contains a MID1 section.
    /// </summary>
    public bool HasMid1 { get; set; }

    /// <summary>
    /// Special MID1 format data.
    /// </summary>
    public byte[] Mid1Format { get; set; } = [];

    /// <summary>
    /// Whether the file contains a STR1 section.
    /// </summary>
    public bool HasStr1 { get; set; }

    /// <summary>
    /// Whether the file contains FLW1/FLI1 sections.
    /// </summary>
    public bool HasFlw1 { get; set; }

    /// <summary>
    /// The specific flow node data from FLW1/FLI1.
    /// </summary>
    public BmgFlowData? FlowData { get; set; }

    /// <summary>
    /// The list of messages in the BMG file.
    /// </summary>
    public List<BmgMessage> Messages { get; set; } = [];
}