﻿using System;
using System.IO;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Bwav;

/// <summary>
/// A class for parsing BWAV files.
/// </summary>
public class BwavFileParser : IFileParser<BwavFile>
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public BwavFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a BWAV file.");

        //read header
        ReadHeader(reader, out var version, out var hash, out var prefetch, out var channelCount);

        var bwavFile = new BwavFile
        {
            Version = version,
            Hash = hash,
            Prefetch = prefetch,
            Channels = new ChannelData[channelCount]
        };

        var position = reader.Position;
        for (var i = 0; i < channelCount; ++i)
        {
            bwavFile.Channels[i] = ReadChannelData(reader, position + i * 0x4C);
        }

        return bwavFile;
    }
    #endregion

    #region private methods
    //verifies that the file is a BWAV file
    private static bool CanParse(FileReader reader) => reader.Length > 4 && reader.ReadStringAt(0, 4) == "BWAV";

    //parses header
    private static void ReadHeader(FileReader reader, out string version, out string hash, out bool prefetch, out int channelCount)
    {
        reader.IsBigEndian = reader.ReadUInt16At(4) == 0xFFFE;

        version = $"{reader.ReadByte()}.{reader.ReadByte()}";

        hash = reader.ReadHexString(4);

        prefetch = reader.ReadUInt16() == 1;

        channelCount = reader.ReadUInt16();
    }

    //parse channel data
    private static ChannelData ReadChannelData(FileReader reader, long position)
    {
        var data = new ChannelData
        {
            Pan = (ChannelPan) reader.ReadUInt16At(position + 0x02),
            SampleRate = reader.ReadUInt32(),
            Samples = reader.ReadUInt32At(position + 0x0C),
            Loop = reader.ReadUInt32At(position + 0x038) == 1,
            LoopEnd = reader.ReadUInt32(),
            LoopStart = reader.ReadUInt32()
        };

        /*
            0x02 	u16 	Channel Pan. 0 for left, 1 for right, 2 for middle
            0x04 	u32 	Sample Rate
            0x08 	u32 	Number of samples in non-prefetch file
            0x0C 	u32 	Number of samples in this file
            0x10 	s16[8][2] 	DSP-ADPCM Coefficients
            0x30 	u32 	Absolute start offset of the sample data in non-prefetch file
            0x34 	u32 	Absolute start offset of the sample data in this file
            0x38 	u32 	Is 1 if the channel loops
            0x3C 	u32 	Loop End Sample (0xFFFFFFFF if doesn't loop)
            0x40 	u32 	Loop Start Sample (0 if doesn't loop)
            0x44 	u16 	Predictor Scale?
            0x46 	u16 	History Sample 1?
            0x48 	u16 	History Sample 2?
            0x4A 	u16 	Padding?
        */

        return data;
    }
    #endregion
}