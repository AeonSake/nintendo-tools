﻿using System;
using System.IO;

namespace NintendoTools.FileFormats.Bcsv;

/// <summary>
/// A class for serializing <see cref="BcsvFile"/> objects to CSV.
/// </summary>
public class BcsvCsvSerializer : IFileSerializer<BcsvFile>
{
    #region public properties
    /// <summary>
    /// Gets or sets the separator character that should be used.
    /// The default value is '<c>,</c>'.
    /// </summary>
    public string Separator { get; set; } = ",";
    #endregion

    #region IFileSerializer interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="FormatException"></exception>
    public void Serialize(TextWriter writer, BcsvFile file)
    {
        if (string.IsNullOrEmpty(Separator)) throw new FormatException("CSV separator cannot be empty.");
        if (Separator.Contains('=')) throw new FormatException($"\"{Separator}\" cannot be used as CSV separator.");
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(writer, nameof(writer));
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        #else
        if (writer is null) throw new ArgumentNullException(nameof(writer));
        if (file is null) throw new ArgumentNullException(nameof(file));
        #endif

        for (var i = 0; i < file.Columns; ++i)
        {
            if (i > 0) writer.Write(Separator);
            writer.Write(file.HeaderInfo[i].NewHeaderName);
        }
        writer.WriteLine();

        foreach (var entry in file)
        {
            for (var i = 0; i < entry.Length; ++i)
            {
                if (i > 0) writer.Write(Separator);
                if (entry[i] is null) continue;

                var text = entry[i]?.ToString() ?? string.Empty;
                var wrapText = text.Contains(Separator) || text.Contains('\n');
                if (wrapText && text.Contains('"')) text = text.Replace("\"", "\"\"");
                writer.Write(wrapText ? '"' + text + '"' : text);
            }

            writer.WriteLine();
        }

        writer.Flush();
        writer.Close();
    }
    #endregion
}