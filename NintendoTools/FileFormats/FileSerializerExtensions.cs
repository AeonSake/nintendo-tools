﻿using System;
using System.IO;

namespace NintendoTools.FileFormats;

/// <summary>
/// A collection of extension methods for <see cref="IFileSerializer{T}"/> classes.
/// </summary>
public static class FileSerializerExtensions
{
    /// <summary>
    /// Serializes a file object.
    /// </summary>
    /// <param name="serializer">The <see cref="IFileSerializer{T}"/> instance to use.</param>
    /// <param name="file">The file to serialize.</param>
    /// <returns>The serialized string.</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static string Serialize<T>(this IFileSerializer<T> serializer, T file) where T : class
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(serializer, nameof(serializer));
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        #else
        if (serializer is null) throw new ArgumentNullException(nameof(serializer));
        if (file is null) throw new ArgumentNullException(nameof(file));
        #endif

        using var writer = new StringWriter();
        serializer.Serialize(writer, file);
        return writer.ToString();
    }
}