﻿namespace NintendoTools.FileFormats;

/// <summary>
/// An enum of different text encoding types used across various file types.
/// </summary>
// ReSharper disable InconsistentNaming
public enum EncodingType
{
    /// <summary>
    /// ASCII (UTF-7) encoding.
    /// </summary>
    ASCII,

    /// <summary>
    /// Basic Latin 1 / ISO/IEC 8859-1 code page, supporting most Latin symbols.
    /// </summary>
    Latin1,

    /// <summary>
    /// Shift JIS encoding for Japanese symbols.
    /// </summary>
    ShiftJIS,

    /// <summary>
    /// UTF-8 encoding.
    /// </summary>
    UTF8,

    /// <summary>
    /// UTF-16 encoding.
    /// </summary>
    UTF16,

    /// <summary>
    /// UTF-32 encoding.
    /// </summary>
    UTF32
}