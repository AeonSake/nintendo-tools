﻿namespace NintendoTools.FileFormats;

/// <summary>
/// An interface for files that can be encoded in either little or big endian.
/// </summary>
public interface IVariableEndianFile
{
    /// <summary>
    /// Whether the file is encoded in big endian.
    /// </summary>
    public bool BigEndian { get; set; }
}