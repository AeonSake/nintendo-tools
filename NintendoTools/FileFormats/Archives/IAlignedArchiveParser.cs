﻿using System.Collections.Generic;
using System.IO;

namespace NintendoTools.FileFormats;

/// <summary>
/// An interface for parsers of archive files that use alignment tables.
/// </summary>
public interface IAlignedArchiveParser
{
    /// <summary>
    /// Gets a list of alignment data of all files contained in the archive.
    /// This can be used to figure out the correct alignment values.
    /// </summary>
    /// <param name="fileStream">The stream to parse</param>
    /// <param name="table">The alignment table to use.</param>
    /// <returns>A collection of <see cref="AlignmentInfo"/> entries for all containing files.</returns>
    public List<AlignmentInfo> CheckAlignment(Stream fileStream, AlignmentTable table);
}