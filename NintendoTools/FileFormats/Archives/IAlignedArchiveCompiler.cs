﻿namespace NintendoTools.FileFormats;

/// <summary>
/// An interface for compilers of archive files that use alignment tables.
/// </summary>
public interface IAlignedArchiveCompiler
{
    /// <summary>
    /// Gets or sets the alignment table to use.
    /// </summary>
    public AlignmentTable Alignment { get; set; }
}