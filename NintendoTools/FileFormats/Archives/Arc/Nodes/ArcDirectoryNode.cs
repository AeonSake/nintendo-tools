﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Arc;

/// <summary>
/// A class for a directory-type node.
/// </summary>
public class ArcDirectoryNode : IArcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// A list of child nodes of this directory.
    /// </summary>
    public List<IArcNode> Children { get; set; } = [];
}