﻿namespace NintendoTools.FileFormats.Arc;

/// <summary>
/// The base interface for all ARC nodes.
/// </summary>
public interface IArcNode
{
    /// <summary>
    /// The name of the file or directory.
    /// </summary>
    public string Name { get; }
}