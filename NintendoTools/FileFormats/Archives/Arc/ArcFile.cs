﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Arc;

/// <summary>
/// A class holding information about an ARC archive file.
/// </summary>
public class ArcFile
{
    /// <summary>
    /// The content of the ARC archive.
    /// </summary>
    public List<IArcNode> Content { get; set; } = [];
}