﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Arc;

/// <summary>
/// A class for compiling ARC/U8 archives.
/// </summary>
public class ArcFileCompiler : IFileCompiler<ArcFile>
{
    #region private members
    private static readonly ArcNodeComparer NodeComparer = new();
    #endregion

    #region IFileCompiler interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public void Compile(ArcFile file, Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (file is null) throw new ArgumentNullException(nameof(file));
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var writer = new FileWriter(fileStream, true);
        writer.IsBigEndian = true;

        //write header
        writer.Write([0x55, 0xAA, 0x38, 0x2D]);
        writer.Write(0x20);
        writer.Pad(24);

        var sortedNodes = SortNodes(file.Content);
        var dirNodeDepths = BuildDirDepths(sortedNodes);

        //write root node
        writer.Write((byte) 0x01);
        writer.Pad(7);
        writer.Write(sortedNodes.Length + 1);

        //write node metadata
        var stringTable = "\0";
        var nodeOffsetPosition = writer.Position;
        for (var i = 0; i < sortedNodes.Length; ++i)
        {
            var node = sortedNodes[i];

            switch (node)
            {
                case ArcFileNode fileNode:
                    writer.Write((byte) 0x00);
                    writer.Pad(1);
                    writer.Write((ushort) stringTable.Length);
                    writer.Pad(4);
                    writer.Write(fileNode.Data.Length);
                    break;
                case ArcDirectoryNode dirNode:
                    writer.Write((byte) 0x01);
                    writer.Pad(1);
                    writer.Write((ushort) stringTable.Length);
                    writer.Write(dirNodeDepths[dirNode]);
                    writer.Write(FindLastNodeIndex(dirNode, sortedNodes, i) + 1);
                    break;
                default:
                    throw new InvalidDataException($"Unknown node type {node.GetType()}.");
            }

            stringTable += node.Name + "\0";
        }

        //write string table
        writer.Write(stringTable, Encoding.ASCII);
        var headerLength = writer.Position - 0x20;
        writer.Align(32);

        //write file data
        var dataOffsetPosition = writer.Position;
        for (var i = 0; i < sortedNodes.Length; ++i)
        {
            if (sortedNodes[i] is not ArcFileNode fileNode) continue;

            var dataOffset = writer.Position;
            writer.JumpTo(nodeOffsetPosition + 4 + i * 12);
            writer.Write((uint) dataOffset);
            writer.JumpTo(dataOffset);
            writer.Write(fileNode.Data);
            writer.Align(32);
        }

        //add remaining header data
        writer.JumpTo(0x08);
        writer.Write((uint) headerLength);
        writer.Write((uint) dataOffsetPosition);
    }
    #endregion

    #region private methods
    private static IArcNode[] SortNodes(IEnumerable<IArcNode> nodes)
    {
        var sortedNodes = new List<IArcNode>();
        SortChildNodes(nodes);
        return [..sortedNodes];

        void SortChildNodes(IEnumerable<IArcNode> childNodes)
        {
            foreach (var node in childNodes.OrderBy(n => n, NodeComparer))
            {
                sortedNodes.Add(node);

                if (node is ArcDirectoryNode dirNode) SortChildNodes(dirNode.Children);
            }
        }
    }

    private static Dictionary<ArcDirectoryNode, int> BuildDirDepths(IEnumerable<IArcNode> nodes)
    {
        var depths = new Dictionary<ArcDirectoryNode, int>();

        foreach (var node in nodes)
        {
            if (node is not ArcDirectoryNode dirNode) continue;

            var found = false;
            foreach (var (parentDirNode, depth) in depths)
            {
                if (!parentDirNode.Children.Contains(node)) continue;

                depths.Add(dirNode, depth + 1);
                found = true;
                break;
            }

            if (!found) depths.Add(dirNode, 0);
        }

        return depths;
    }

    private static int FindLastNodeIndex(ArcDirectoryNode node, IArcNode[] nodes, int startIndex)
    {
        while (true)
        {
            IArcNode? lastNode = null;
            var lastNodeIndex = startIndex;
            var count = node.Children.Count;
            foreach (var childNode in node.Children)
            {
                var childIndex = Array.IndexOf(nodes, childNode, lastNodeIndex + 1, count);
                if (childIndex < 0) continue;

                lastNode = childNode;
                lastNodeIndex = childIndex;
                count = node.Children.Count + startIndex - lastNodeIndex;
            }

            switch (lastNode)
            {
                case null:
                    return startIndex + 1;
                case ArcFileNode:
                    return lastNodeIndex + 1;
                case ArcDirectoryNode dirNode:
                    node = dirNode;
                    startIndex = lastNodeIndex;
                    continue;
            }

            //should never be reached
            return startIndex + 1;
        }
    }
    #endregion

    #region helper class
    private class ArcNodeComparer : IComparer<IArcNode>
    {
        public int Compare(IArcNode? x, IArcNode? y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if (x is null) return 1;
            if (y is null) return -1;
            if (x.GetType() == y.GetType()) return string.CompareOrdinal(x.Name, y.Name);
            return x is ArcFileNode ? -1 : 1;
        }
    }
    #endregion
}