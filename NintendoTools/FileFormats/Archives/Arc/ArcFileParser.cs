﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Arc;

/// <summary>
/// A class for parsing ARC/U8 archives.
/// </summary>
public class ArcFileParser : IFileParser<ArcFile>
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public ArcFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        reader.IsBigEndian = true;
        if (!CanParse(reader)) throw new InvalidDataException("File is not an ARC file.");

        var nodeCount = reader.ReadInt32At(0x20 + 8);
        var stringTableOffset = 0x20 + nodeCount * 12;

        var rootNode = new ArcDirectoryNode
        {
            Name = string.Empty,
            Children = []
        };
        var dirStack = new Stack<(ArcDirectoryNode, int)>();
        dirStack.Push((rootNode, nodeCount));
        for (var i = 1; i < nodeCount; ++i)
        {
            //ensure current directory is the correct depth
            (ArcDirectoryNode, int) currentDir;
            while ((currentDir = dirStack.Peek()).Item2 <= i) dirStack.Pop();

            reader.JumpTo(0x20 + i * 12);
            var nameOffset = reader.ReadUInt32();
            var type = nameOffset >> 24;
            nameOffset &= 0x00FFFFFF;
            var fileOffset = reader.ReadInt32();
            var fileLength = reader.ReadInt32();
            var name = reader.ReadTerminatedStringAt(stringTableOffset + nameOffset, Encoding.ASCII);

            switch (type)
            {
                case 0x00:
                    currentDir.Item1.Children.Add(new ArcFileNode
                    {
                        Name = name,
                        Data = reader.ReadBytesAt(fileOffset, fileLength)
                    });
                    break;
                case 0x01:
                    var dirNode = new ArcDirectoryNode
                    {
                        Name = name,
                        Children = []
                    };
                    currentDir.Item1.Children.Add(dirNode);
                    dirStack.Push((dirNode, fileLength));
                    break;
                default:
                    throw new InvalidDataException($"Unknown node type 0x{type:X2}.");
            }
        }

        return new ArcFile {Content = rootNode.Children};
    }
    #endregion

    #region private members
    //verifies that the file is a ARC archive
    private static bool CanParse(FileReader reader) => reader.Length > 4 && reader.ReadBytesAt(0, 4) is [0x55, 0xAA, 0x38, 0x2D];
    #endregion
}