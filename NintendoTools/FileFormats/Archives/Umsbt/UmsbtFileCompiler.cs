﻿using System;
using System.Collections.Generic;
using System.IO;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Umsbt;

/// <summary>
/// A class for compiling UMSBT files.
/// </summary>
public class UmsbtFileCompiler : IFileCompiler<IList<MsbtFile>>
{
    #region public properties
    /// <summary>
    /// Gets or sets the compiler used for compiling the MSBT files.
    /// </summary>
    public MsbtFileCompiler Compiler { get; set; } = new();
    #endregion

    #region IFileCompiler interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public void Compile(IList<MsbtFile> files, Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(files, nameof(files));
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (files is null) throw new ArgumentNullException(nameof(files));
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var writer = new FileWriter(fileStream, true);
        writer.Pad(files.Count * 8);
        writer.Align(16);
        writer.Pad(16);

        for (var i = 0; i < files.Count; ++i)
        {
            using var subStream = new MemoryStream();
            Compiler.Compile(files[i], subStream);

            var startPos = writer.Position;
            var size = subStream.Length;

            subStream.Position = 0;
            subStream.CopyTo(writer.BaseStream);
            var endPos = writer.Position;

            writer.JumpTo(i * 8);
            writer.Write((uint) startPos);
            writer.Write((uint) size);
            writer.JumpTo(endPos);
        }
    }
    #endregion
}