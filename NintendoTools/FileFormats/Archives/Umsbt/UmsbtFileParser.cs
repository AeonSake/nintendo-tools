﻿using System;
using System.Collections.Generic;
using System.IO;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Umsbt;

/// <summary>
/// A class for parsing UMSBT archives.
/// </summary>
public class UmsbtFileParser : IFileParser<List<MsbtFile>>
{
    #region private members
    private static readonly MsbtFileParser MsbtParser = new();
    #endregion

    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public List<MsbtFile> Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);

        //find file offsets and sizes
        GetMetaData(reader, out var offsets, out var sizes);

        //read UMSBT files
        var result = new List<MsbtFile>();
        for (var i = 0; i < offsets.Length; ++i)
        {
            var stream = new StreamSpan(reader.BaseStream, offsets[i], sizes[i]);
            try
            {
                result.Add(MsbtParser.Parse(stream));
            }
            catch (Exception ex)
            {
                throw new InvalidDataException("File is not an UMSBT file.", ex);
            }
        }

        return result;
    }
    #endregion

    #region private methods
    //verifies that the file is a UMSBT file
    private static bool CanParse(FileReader reader)
    {
        GetMetaData(reader, out var offsets, out var sizes);
        if (offsets.Length == 0) return false;

        for (var i = 0; i < offsets.Length; ++i)
        {
            var stream = new StreamSpan(reader.BaseStream, offsets[i], sizes[i]);
            if (!MsbtParser.CanParse(stream)) return false;
        }

        return true;
    }

    //parses meta data
    private static void GetMetaData(FileReader reader, out uint[] offsets, out uint[] sizes)
    {
        var offsetList = new List<uint>();
        var sizeList = new List<uint>();
        offsets = [];
        sizes = [];

        try
        {
            var dataStart = reader.ReadUInt32At(0);
            reader.Position = 0;

            while (reader.Position < dataStart)
            {
                var offset = reader.ReadUInt32();
                var size = reader.ReadUInt32();

                if (offset + size > reader.Length) return;
                if (offset == 0 || size == 0) break;

                offsetList.Add(offset);
                sizeList.Add(size);
            }

            offsets = [..offsetList];
            sizes = [..sizeList];
        }
        catch
        {
            //ignored
        }
    }
    #endregion
}