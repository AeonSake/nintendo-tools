﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NintendoTools.Utils;
using FileOffset = (uint Start, int Length);

namespace NintendoTools.FileFormats.Narc;

/// <summary>
/// A class for parsing NARC archives.
/// </summary>
public class NarcFileParser : IFileParser<NarcFile>
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public NarcFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a NARC file.");

        //parse file metadata and header
        GetMetaData(reader, out var version, out _, out var headerSize, out var sectionCount);

        var fileOffsets = Array.Empty<FileOffset>();
        var nodes = new List<INarcNode>();
        var files = new List<NarcFileNode>();
        var hasFileNames = true;

        var sectionOffset = (long) headerSize;
        for (var i = 0; i < sectionCount; ++i)
        {
            reader.JumpTo(sectionOffset);

            var type = reader.ReadString(4, Encoding.ASCII);
            sectionOffset += reader.ReadUInt32();

            switch (type)
            {
                case "BTAF":
                    ParseBtaf(reader, out fileOffsets);
                    break;
                case "BTNF":
                    ParseBtnf(reader, out nodes, out files, out hasFileNames);
                    break;
                case "GMIF":
                    ParseGmif(reader, fileOffsets, files);
                    break;
                default:
                    throw new InvalidDataException($"Unknown section type: {type}");
            }
        }

        return new NarcFile
        {
            BigEndian = reader.IsBigEndian,
            Version = version,
            HasFileNames = hasFileNames,
            Content = nodes.Count > 0 ? nodes : [..files]
        };
    }
    #endregion

    #region private methods
    //verifies that the file is a NARC archive
    private static bool CanParse(FileReader reader) => reader.Length > 4 && reader.ReadStringAt(0, 4, Encoding.ASCII) == "NARC";

    //parses meta data
    private static void GetMetaData(FileReader reader, out int version, out uint fileSize, out int headerSize, out int sectionCount)
    {
        if (reader.ReadUInt16At(4) == 0xFEFF) reader.IsBigEndian = true;

        version = reader.ReadUInt16();
        fileSize = reader.ReadUInt32();
        headerSize = reader.ReadUInt16();
        sectionCount = reader.ReadUInt16();
    }

    //parse BTAF sections (file allocation table)
    private static void ParseBtaf(FileReader reader, out FileOffset[] offsets)
    {
        var fileCount = reader.ReadUInt16();
        reader.Skip(2);

        offsets = new FileOffset[fileCount];
        for (var i = 0; i < fileCount; ++i)
        {
            var start = reader.ReadUInt32();
            var end = reader.ReadUInt32();
            offsets[i] = (start, (int) (end - start));
        }
    }

    //parse BTNF sections (file name table)
    private static void ParseBtnf(FileReader reader, out List<INarcNode> nodes, out List<NarcFileNode> files, out bool hasFileNames)
    {
        var nameTableOffset = reader.Position + reader.ReadUInt32();
        var firstFileId = reader.ReadUInt16();
        var dirCount = reader.ReadUInt16();

        nodes = [];
        files = [];
        hasFileNames = false;
        if (nameTableOffset == 4) return;

        var dirNodes = new NarcDirectoryNode[dirCount];
        dirNodes[0] = new NarcDirectoryNode {Name = string.Empty};

        for (var i = 1; i < dirCount; ++i)
        {
            reader.Skip(6); //4 bytes sub-table offset, 2 bytes ID of first file
            var parentDirId = reader.ReadUInt16() & 0xFFF;

            var node = new NarcDirectoryNode {Name = string.Empty};
            dirNodes[i] = node;
            dirNodes[parentDirId].Children.Add(node);
        }

        var dirIndex = 0;
        while (dirIndex < dirCount)
        {
            var nameLength = reader.ReadByte();

            if ((nameLength & 0x80) > 0) //directory name
            {
                var name = reader.ReadString(nameLength & 0x7F, Encoding.UTF8);
                var index = reader.ReadUInt16() & 0xFFF;
                dirNodes[index].Name = name;
            }
            else if (nameLength > 0) //file name
            {
                var name = reader.ReadString(nameLength, Encoding.UTF8);
                var node = new NarcFileNode {Name = name, Data = []};
                dirNodes[dirIndex].Children.Add(node);
                files.Add(node);
            }
            else //end of sub-table
            {
                ++dirIndex;
            }
        }

        nodes = dirNodes[0].Children;
        hasFileNames = true;
    }

    //parse GMIF sections (file data)
    private static void ParseGmif(FileReader reader, FileOffset[] fileOffsets, List<NarcFileNode> files)
    {
        var dataOffset = reader.Position;
        for (var i = 0; i < fileOffsets.Length; ++i)
        {
            var data = reader.ReadBytesAt(dataOffset + fileOffsets[i].Start, fileOffsets[i].Length);
            if (i >= files.Count) files.Add(new NarcFileNode {Name = string.Empty, Data = data});
            else files[i].Data = data;
        }
    }
    #endregion
}