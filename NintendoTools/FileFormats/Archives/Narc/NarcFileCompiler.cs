﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Narc;

/// <summary>
/// A class for compiling NARC archives.
/// </summary>
public class NarcFileCompiler : IFileCompiler<NarcFile>
{
    #region IFileCompiler interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public void Compile(NarcFile file, Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (file is null) throw new ArgumentNullException(nameof(file));
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var writer = new FileWriter(fileStream, true);
        writer.IsBigEndian = file.BigEndian;

        //write header
        writer.Write("NARC", Encoding.ASCII);
        writer.Write((ushort) 0xFFFE);
        writer.Write((ushort) file.Version);
        writer.Pad(4);
        writer.Write((ushort) 0x10);
        writer.Write((ushort) 3);

        //remap files and directories
        var dirEntries = new List<DirectoryEntry>();
        var fileNodes = new List<NarcFileNode>();
        var dirStack = new Stack<NarcDirectoryNode>();
        dirStack.Push(new NarcDirectoryNode {Name = string.Empty, Children = file.Content});
        while (dirStack.Count > 0)
        {
            var dirNode = dirStack.Pop();

            var entry = new DirectoryEntry
            {
                Node = dirNode,
                SortedChildren = [..dirNode.Children]
            };
            entry.SortedChildren.Sort((f1, f2) => string.CompareOrdinal(f1.Name, f2.Name));
            dirEntries.Add(entry);

            //sort files
            foreach (var node in entry.SortedChildren)
            {
                if (node is not NarcFileNode fileNode) continue;
                if (entry.FirstFileId < 0) entry.FirstFileId = fileNodes.Count;
                fileNodes.Add(fileNode);
            }

            //add directories in reverse order
            for (var i = entry.SortedChildren.Count - 1; i >= 0; --i)
            {
                var node = entry.SortedChildren[i];
                if (node is not NarcDirectoryNode dirNode2) continue;
                dirStack.Push(dirNode2);
            }

            //find parent ID
            for (var i = 0; i < dirEntries.Count - 1; ++i)
            {
                var dirEntry = dirEntries[i];
                if (!dirEntry.Node.Children.Contains(dirNode)) continue;

                entry.ParentId = i;
                if (entry.FirstFileId < 0) break;

                //fix missing file IDs
                while (dirEntry.FirstFileId < 0)
                {
                    dirEntry.FirstFileId = entry.FirstFileId;

                    var parentDir = dirEntries[dirEntry.ParentId];
                    if (parentDir == dirEntry) break;
                    dirEntry = parentDir;
                }
                break;
            }
        }

        //write sections
        WriteBtaf(writer, fileNodes);
        WriteBtnf(writer, dirEntries, file.HasFileNames);
        WriteGmif(writer, fileNodes);

        var size = (uint) writer.Position;
        writer.JumpTo(8);
        writer.Write(size);
    }
    #endregion

    #region private methods
    private static long StartSection(FileWriter writer, string sectionName)
    {
        var sectionStart = writer.Position;
        writer.Write(sectionName, Encoding.ASCII);
        writer.Pad(4);
        return sectionStart;
    }

    private static void EndSection(FileWriter writer, long sectionStart)
    {
        writer.Align(4, 0xFF);
        var endPos = writer.Position;
        writer.JumpTo(sectionStart + 4);
        writer.Write((int) (endPos - sectionStart));
        writer.JumpTo(endPos);
    }

    private static void WriteBtaf(FileWriter writer, List<NarcFileNode> files)
    {
        var startPos = StartSection(writer, "BTAF");

        writer.Write((ushort) files.Count);
        writer.Pad(2);

        var offset = 0;
        foreach (var file in files)
        {
            writer.Write(offset);
            offset += file.Data.Length;
            writer.Write(offset);
            offset += BinaryUtils.GetOffset(offset, 4);
        }

        EndSection(writer, startPos);
    }

    private static void WriteBtnf(FileWriter writer, List<DirectoryEntry> directories, bool hasFileNames)
    {
        var startPos = StartSection(writer, "BTNF");

        if (hasFileNames)
        {
            var tableStart = writer.Position;
            var tableOffset = directories.Count * 8;
            writer.Pad(tableOffset);

            for (var i = 0; i < directories.Count; ++i)
            {
                var entry = directories[i];
                writer.JumpTo(tableStart + i * 8);

                writer.Write(tableOffset);
                if (entry.FirstFileId < 0) writer.Write((ushort) 0);
                else writer.Write((ushort) entry.FirstFileId);
                if (i == 0) writer.Write((ushort) directories.Count);
                else writer.Write((ushort) (entry.ParentId | 0xF000));

                //write sub-table
                writer.JumpTo(tableStart + tableOffset);
                foreach (var node in entry.SortedChildren)
                {
                    if (node is NarcDirectoryNode dirNode)
                    {
                        writer.Write((byte) (node.Name.Length | 0x80));
                        writer.Write(node.Name, Encoding.UTF8);
                        for (var j = 0; j < directories.Count; ++j)
                        {
                            if (directories[j].Node != dirNode) continue;
                            writer.Write((ushort) (j | 0xF000));
                            break;
                        }
                    }
                    else if (node is NarcFileNode)
                    {
                        writer.Write((byte) node.Name.Length);
                        writer.Write(node.Name, Encoding.UTF8);
                    }
                }
                writer.Write((byte) 0);

                tableOffset = (int) (writer.Position - tableStart);
            }
        }
        else
        {
            writer.Write(4);
            writer.Write((ushort) 0);
            writer.Write((short) 1);
        }

        EndSection(writer, startPos);
    }

    private static void WriteGmif(FileWriter writer, List<NarcFileNode> files)
    {
        var startPos = StartSection(writer, "GMIF");

        foreach (var file in files)
        {
            writer.Write(file.Data);
            writer.Align(4, 0xFF);
        }

        EndSection(writer, startPos);
    }
    #endregion

    #region helper class
    private class DirectoryEntry
    {
        public required NarcDirectoryNode Node { get; init; }

        public required List<INarcNode> SortedChildren { get; init; }

        public int ParentId { get; set; }

        public int FirstFileId { get; set; } = -1;
    }
    #endregion
}