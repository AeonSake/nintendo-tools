﻿namespace NintendoTools.FileFormats.Narc;

/// <summary>
/// A class for a file-type node.
/// </summary>
public class NarcFileNode : INarcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// The content of the file as <see cref="byte"/> array.
    /// </summary>
    public required byte[] Data { get; set; }
}