﻿namespace NintendoTools.FileFormats.Narc;

/// <summary>
/// The base interface for all NARC nodes.
/// </summary>
public interface INarcNode
{
    /// <summary>
    /// The name of the file or directory.
    /// </summary>
    public string Name { get; }
}