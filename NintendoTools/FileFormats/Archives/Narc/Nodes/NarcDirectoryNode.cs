﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Narc;

/// <summary>
/// A class for a directory-type node.
/// </summary>
public class NarcDirectoryNode : INarcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// A list of child nodes of this directory.
    /// </summary>
    public List<INarcNode> Children { get; set; } = [];
}