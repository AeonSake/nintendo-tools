﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Narc;

/// <summary>
/// A class holding information about an NARC archive file.
/// </summary>
public class NarcFile : IVariableEndianFile
{
    /// <inheritdoc/>
    public bool BigEndian { get; set; }

    /// <summary>
    /// The version of the NARC file.
    /// </summary>
    public int Version { get; set; }

    /// <summary>
    /// Whether the files and directories have names.
    /// </summary>
    public bool HasFileNames { get; set; }

    /// <summary>
    /// The content of the NARC archive.
    /// </summary>
    public List<INarcNode> Content { get; set; } = [];
}