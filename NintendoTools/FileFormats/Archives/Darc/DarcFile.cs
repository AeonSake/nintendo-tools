﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Darc;

/// <summary>
/// A class holding information about an DARC archive file.
/// </summary>
public class DarcFile : IVariableEndianFile
{
    /// <inheritdoc/>
    public bool BigEndian { get; set; }

    /// <summary>
    /// The version of the DARC file.
    /// </summary>
    public int Version { get; set; }

    /// <summary>
    /// The content of the DARC archive.
    /// </summary>
    public List<IDarcNode> Content { get; set; } = [];
}