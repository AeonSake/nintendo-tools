﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NintendoTools.Utils;
using FileEntry = (uint NameOffset, uint FileOffset, uint FileLength, NintendoTools.FileFormats.Darc.IDarcNode Node);
using DirEntry = (uint FileCount, NintendoTools.FileFormats.Darc.DarcDirectoryNode Node);

namespace NintendoTools.FileFormats.Darc;

/// <summary>
/// A class for parsing DARC archives.
/// </summary>
public class DarcFileParser : IFileParser<DarcFile>, IAlignedArchiveParser
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }

    /// <inheritdoc cref="IAlignedArchiveParser.CheckAlignment"/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public static List<AlignmentInfo> CheckAlignmentStatic(Stream fileStream, AlignmentTable table)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a DARC file.");

        //parse file metadata and header
        GetMetaData(reader, out _, out _, out _, out var fileTableOffset, out var fileTableLength, out _);

        //read file table
        reader.JumpTo(fileTableOffset);
        var fileOffsets = new List<FileEntry>();
        FileEntry rootEntry = (reader.ReadUInt32() & 0x00, reader.ReadUInt32(), reader.ReadUInt32(), new DarcDirectoryNode {Name = string.Empty});
        fileOffsets.Add(rootEntry);
        var dirStack = new Stack<DirEntry>();
        dirStack.Push((rootEntry.FileLength, (DarcDirectoryNode) rootEntry.Node));
        while (dirStack.Count > 0)
        {
            var (fileCount, dirNode) = dirStack.Peek();
            if (fileOffsets.Count >= fileCount)
            {
                dirStack.Pop();
                continue;
            }

            var nameOffset = reader.ReadUInt32();
            var type = nameOffset >> 24;
            nameOffset &= 0x00FFFFFF;
            var fileOffset = reader.ReadUInt32();
            var fileLength = reader.ReadUInt32();

            IDarcNode node;
            switch (type)
            {
                case 0x00: //file
                    node = new DarcFileNode {Name = string.Empty, Data = []};
                    break;
                case 0x01: //directory
                    node = new DarcDirectoryNode {Name = string.Empty};
                    dirStack.Push((fileLength, (DarcDirectoryNode) node));
                    break;
                default:
                    throw new InvalidDataException($"Unknown node type: 0x{type:X2}");
            }

            fileOffsets.Add((nameOffset, fileOffset, fileLength, node));
            dirNode.Children.Add(node);
        }

        var infos = new List<AlignmentInfo>();

        //read file names and data
        var nameListOffset = reader.Position;
        var lastDataEnd = fileTableOffset + fileTableLength;
        foreach (var (nameOffset, dataOffset, dataLength, node) in fileOffsets.OrderBy(i => i.FileOffset))
        {
            if (node is not DarcFileNode) continue;

            var name = reader.ReadTerminatedStringAt(nameListOffset + nameOffset, Encoding.Unicode);
            var alignment = table.GetFromName(name);

            infos.Add(new AlignmentInfo
            {
                Name = name,
                DataStart = dataOffset,
                DataEnd = dataOffset + dataLength,
                Padding = dataOffset - lastDataEnd,
                Alignment = alignment,
                ExpectedDataStart = lastDataEnd + BinaryUtils.GetOffset(lastDataEnd, alignment)
            });

            lastDataEnd = dataOffset + dataLength;
        }

        return infos;
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public DarcFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a DARC file.");

        //parse file metadata and header
        GetMetaData(reader, out var version, out _, out _, out var fileTableOffset, out _, out _);

        //read file table
        reader.JumpTo(fileTableOffset);
        var fileOffsets = new List<FileEntry>();
        FileEntry rootEntry = (reader.ReadUInt32() & 0x00, reader.ReadUInt32(), reader.ReadUInt32(), new DarcDirectoryNode {Name = string.Empty});
        fileOffsets.Add(rootEntry);
        var dirStack = new Stack<DirEntry>();
        dirStack.Push((rootEntry.FileLength, (DarcDirectoryNode) rootEntry.Node));
        while (dirStack.Count > 0)
        {
            var (fileCount, dirNode) = dirStack.Peek();
            if (fileOffsets.Count >= fileCount)
            {
                dirStack.Pop();
                continue;
            }

            var nameOffset = reader.ReadUInt32();
            var type = nameOffset >> 24;
            nameOffset &= 0x00FFFFFF;
            var fileOffset = reader.ReadUInt32();
            var fileLength = reader.ReadUInt32();

            IDarcNode node;
            switch (type)
            {
                case 0x00: //file
                    node = new DarcFileNode {Name = string.Empty, Data = []};
                    break;
                case 0x01: //directory
                    node = new DarcDirectoryNode {Name = string.Empty};
                    dirStack.Push((fileLength, (DarcDirectoryNode) node));
                    break;
                default:
                    throw new InvalidDataException($"Unknown node type: 0x{type:X2}");
            }

            fileOffsets.Add((nameOffset, fileOffset, fileLength, node));
            dirNode.Children.Add(node);
        }

        //read file names and data
        var nameListOffset = reader.Position;
        foreach (var (nameOffset, dataOffset, dataLength, node) in fileOffsets)
        {
            var name = reader.ReadTerminatedStringAt(nameListOffset + nameOffset, Encoding.Unicode);

            switch (node)
            {
                case DarcFileNode fileNode:
                    fileNode.Name = name;
                    fileNode.Data = reader.ReadBytesAt(dataOffset, (int) dataLength);
                    break;
                case DarcDirectoryNode dirNode:
                    dirNode.Name = name;
                    break;
            }
        }

        //find root node
        var rootNode = (DarcDirectoryNode) rootEntry.Node;
        if (fileOffsets.Count > 1 && fileOffsets[1].Node.Name == ".") rootNode = (DarcDirectoryNode) fileOffsets[1].Node;

        return new DarcFile
        {
            BigEndian = reader.IsBigEndian,
            Version = version,
            Content = rootNode.Children
        };
    }
    #endregion

    #region IAlignedArchiveParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public List<AlignmentInfo> CheckAlignment(Stream fileStream, AlignmentTable table) => CheckAlignmentStatic(fileStream, table);
    #endregion

    #region private methods
    //verifies that the file is a DARC archive
    private static bool CanParse(FileReader reader) => reader.Length > 4 && reader.ReadStringAt(0, 4, Encoding.ASCII) == "darc";

    //parses meta data
    private static void GetMetaData(FileReader reader, out int version, out uint fileSize, out int headerSize, out uint fileTableOffset, out uint fileTableLength, out uint fileDataOffset)
    {
        if (reader.ReadUInt16At(4) == 0xFFFE) reader.IsBigEndian = true;

        headerSize = reader.ReadUInt16();
        version = reader.ReadInt32();
        fileSize = reader.ReadUInt32();
        fileTableOffset = reader.ReadUInt32();
        fileTableLength = reader.ReadUInt32();
        fileDataOffset = reader.ReadUInt32();
    }
    #endregion
}