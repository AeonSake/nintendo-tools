﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Darc;

/// <summary>
/// A class for a directory-type node.
/// </summary>
public class DarcDirectoryNode : IDarcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// A list of child nodes of this directory.
    /// </summary>
    public List<IDarcNode> Children { get; set; } = [];
}