﻿namespace NintendoTools.FileFormats.Darc;

/// <summary>
/// The base interface for all DARC nodes.
/// </summary>
public interface IDarcNode
{
    /// <summary>
    /// The name of the file or directory.
    /// </summary>
    public string Name { get; }
}