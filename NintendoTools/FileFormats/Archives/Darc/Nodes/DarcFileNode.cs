﻿namespace NintendoTools.FileFormats.Darc;

/// <summary>
/// A class for a file-type node.
/// </summary>
public class DarcFileNode : IDarcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// The content of the file as <see cref="byte"/> array.
    /// </summary>
    public required byte[] Data { get; set; }
}