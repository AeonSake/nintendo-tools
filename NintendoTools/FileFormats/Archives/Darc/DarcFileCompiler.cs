﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Darc;

/// <summary>
/// A class for compiling DARC archives.
/// </summary>
public class DarcFileCompiler : IFileCompiler<DarcFile>, IAlignedArchiveCompiler
{
    #region private members
    private static readonly DarcNodeComparer NodeComparer = new();
    #endregion

    #region IAlignedArchiveCompiler interface
    /// <inheritdoc/>
    public AlignmentTable Alignment { get; set; } = new() {Default = 8};
    #endregion

    #region IFileCompiler interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public void Compile(DarcFile file, Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (file is null) throw new ArgumentNullException(nameof(file));
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var writer = new FileWriter(fileStream, true);
        writer.IsBigEndian = file.BigEndian;

        //write header
        writer.Write("darc", Encoding.ASCII);
        writer.Write((ushort) 0xFEFF);
        writer.Write((ushort) 0x1C);
        writer.Write(file.Version);
        writer.Pad(4);
        writer.Write(0x1C);
        writer.Pad(8);

        //build file table
        var nodes = new List<IDarcNode>();
        var dirEntries = new Dictionary<DarcDirectoryNode, IDarcNode?>();
        var dirStack = new Stack<DarcDirectoryNode>();
        var nameTableLength = 6u;
        dirStack.Push(new DarcDirectoryNode {Name = ".", Children = file.Content});
        dirStack.Push(new DarcDirectoryNode {Name = string.Empty});
        while (dirStack.Count > 0)
        {
            var dirNode = dirStack.Pop();
            nodes.Add(dirNode);

            //sort files
            var sortedNodes = dirNode.Children.ToArray();
            Array.Sort(sortedNodes, NodeComparer);
            dirEntries.Add(dirNode, sortedNodes.Length > 0 ? sortedNodes[^1] : null);

            //add files
            foreach (var node in sortedNodes)
            {
                nameTableLength += (uint) Encoding.Unicode.GetByteCount(node.Name) + 2;
                if (node is not DarcFileNode fileNode) continue;
                nodes.Add(fileNode);
            }

            //add directories in reverse order
            for (var i = sortedNodes.Length - 1; i >= 0; --i)
            {
                var node = sortedNodes[i];
                if (node is not DarcDirectoryNode dirNode2) continue;
                dirStack.Push(dirNode2);
            }
        }

        //write file table
        var nameTableOffset = 0u;
        var dataOffset = 0x1C + (uint) nodes.Count * 12 + nameTableLength;
        for (var i = 0; i < nodes.Count; ++i)
        {
            var node = nodes[i];

            switch (node)
            {
                case DarcDirectoryNode:
                    writer.Write(nameTableOffset | (1 << 24));
                    writer.Write(i < 2 ? 0 : GetParentId(nodes, node, i));
                    writer.Write(i < 2 ? nodes.Count : GetLastNodeIndex(nodes, dirEntries, i) + 1);
                    break;
                case DarcFileNode fileNode:
                    writer.Write(nameTableOffset);
                    dataOffset += (uint) BinaryUtils.GetOffset(dataOffset, Alignment.GetFromName(fileNode.Name));
                    writer.Write(dataOffset);
                    dataOffset += (uint) fileNode.Data.Length;
                    writer.Write(fileNode.Data.Length);
                    break;
            }

            nameTableOffset += (uint) Encoding.Unicode.GetByteCount(node.Name) + 2;
        }
        foreach (var node in nodes)
        {
            writer.WriteTerminated(node.Name, Encoding.Unicode);
        }
        var fileTableLength = (uint) writer.Position - 0x1C;

        //write file data
        foreach (var node in nodes)
        {
            if (node is not DarcFileNode fileNode) continue;

            writer.Align(Alignment.GetFromName(fileNode.Name));
            if (dataOffset > writer.Position) dataOffset = (uint) writer.Position;
            writer.Write(fileNode.Data);
        }

        //write header values
        var size = (uint) writer.Position;
        writer.JumpTo(0x0C);
        writer.Write(size);
        writer.Skip(4);
        writer.Write(fileTableLength);
        writer.Write(dataOffset);
    }
    #endregion

    #region private methods
    private static int GetParentId(List<IDarcNode> nodes, IDarcNode node, int startIndex)
    {
        for (var i = startIndex - 1; i >= 0; --i)
        {
            if (nodes[i] is not DarcDirectoryNode dirNode) continue;
            if (dirNode.Children.Contains(node)) return i;
        }

        return 0;
    }

    private static int GetLastNodeIndex(List<IDarcNode> nodes, Dictionary<DarcDirectoryNode, IDarcNode?> dirEntries, int startIndex)
    {
        while (true)
        {
            var node = nodes[startIndex];

            if (node is DarcDirectoryNode dirNode)
            {
                node = dirEntries[dirNode];
                if (node is null) return startIndex;
            }

            for (var i = startIndex; i < nodes.Count; ++i)
            {
                if (nodes[i] != node) continue;
                if (node is DarcFileNode) return i;
                startIndex = i;
                break;
            }
        }
    }
    #endregion

    #region helper class
    private class DarcNodeComparer : IComparer<IDarcNode>
    {
        public int Compare(IDarcNode? x, IDarcNode? y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if (x is null) return 1;
            if (y is null) return -1;
            if (x.GetType() == y.GetType()) return string.Compare(x.Name, y.Name, StringComparison.OrdinalIgnoreCase);
            return x is DarcFileNode ? -1 : 1;
        }
    }
    #endregion
}