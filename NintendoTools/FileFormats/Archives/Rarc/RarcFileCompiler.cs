﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// A class for compiling RARC archives.
/// </summary>
public class RarcFileCompiler : IFileCompiler<RarcFile>
{
    #region private members
    private static readonly RarcNodeComparer NodeComparer = new();
    #endregion

    #region IFileCompiler interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public void Compile(RarcFile file, Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(file, nameof(file));
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (file is null) throw new ArgumentNullException(nameof(file));
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var writer = new FileWriter(fileStream, true);
        writer.IsBigEndian = file.BigEndian;

        //write header
        writer.Write(file.BigEndian ? "RARC" : "CRAR", Encoding.ASCII);
        writer.Pad(4);
        writer.Write(0x20);
        writer.Pad(20);

        //build node maps
        var stringTable = BuildStringTable(file.RootNode);
        var dirNodes = new List<RarcDirectoryNode>();
        var dirMap = new Dictionary<RarcDirectoryNode, int>();
        var nodes = new List<IRarcNode>();
        var mramNodes = new List<RarcFileNode>();
        var aramNodes = new List<RarcFileNode>();
        var dvdNodes = new List<RarcFileNode>();
        var dataSize = 0;
        var mramSize = 0;
        var aramSize = 0;
        var nodeQueue = new Queue<IRarcNode>();
        nodeQueue.Enqueue(file.RootNode);
        while (nodeQueue.Count > 0)
        {
            var node = nodeQueue.Dequeue();
            if (node != file.RootNode) nodes.Add(node);

            if (node is RarcFileNode fileNode)
            {
                dataSize += BinaryUtils.GetOffset(dataSize, 0x20) + fileNode.Data.Length;

                switch (fileNode.Preload)
                {
                    case PreloadType.MRAM:
                        mramNodes.Add(fileNode);
                        mramSize += BinaryUtils.GetOffset(mramSize, 0x20) + fileNode.Data.Length;
                        break;
                    case PreloadType.ARAM:
                        aramNodes.Add(fileNode);
                        aramSize += BinaryUtils.GetOffset(aramSize, 0x20) + fileNode.Data.Length;
                        break;
                    case PreloadType.DVD:
                        dvdNodes.Add(fileNode);
                        break;
                    default:
                        throw new InvalidDataException("Unknown preload type: " + fileNode.Preload);
                }
            }
            else if (node is RarcDirectoryNode dirNode)
            {
                if (node.Name is "." or "..") continue;

                var currentDirNode = new RarcDirectoryNode {Name = "."};
                var parentDirNode = new RarcDirectoryNode {Name = ".."};
                nodeQueue.EnqueueRange(dirNode.Children.OrderBy(n => n, NodeComparer));
                nodeQueue.Enqueue(currentDirNode);
                nodeQueue.Enqueue(parentDirNode);

                dirNodes.Add(dirNode);
                dirMap.Add(dirNode, dirNodes.Count - 1);
                dirMap.Add(currentDirNode, dirNodes.Count - 1);
                var parentIndex = -1;
                for (var i = 0; i < dirNodes.Count; ++i)
                {
                    if (dirNodes[i].Children.Contains(dirNode))
                    {
                        parentIndex = i;
                        break;
                    }
                }
                dirMap.Add(parentDirNode, parentIndex);
            }
            else throw new InvalidDataException($"Unknown node type: {node.GetType()}");
        }

        //write info header
        writer.Write(dirNodes.Count);
        writer.Write(0x20);
        writer.Write(nodes.Count);
        var nodeOffset = dirNodes.Count * 0x10 + BinaryUtils.GetOffset(dirNodes.Count * 0x10, 0x20) + 0x20;
        writer.Write(nodeOffset);
        var stringTableLength = 0;
        foreach (var name in stringTable.Keys) stringTableLength += name.Length + 1;
        writer.Write(stringTableLength + BinaryUtils.GetOffset(stringTableLength, 0x20));
        var stringTableOffset = nodeOffset + nodes.Count * 0x14 + BinaryUtils.GetOffset(nodes.Count * 0x14, 0x20);
        writer.Write(stringTableOffset);
        writer.Write((ushort) nodes.Count);
        writer.Write((byte) (file.SyncedIds ? 1 : 0));
        writer.Pad(5);

        //write dir nodes
        foreach (var dirNode in dirNodes)
        {
            var type = dirNode == file.RootNode ? "ROOT" : dirNode.Name.Length > 3 ? dirNode.Name[..4].ToUpper() : dirNode.Name.ToUpper().PadRight(4, ' ');
            writer.Write(writer.IsBigEndian ? type : type.Reverse());

            writer.Write(stringTable[dirNode.Name]);
            writer.Write(GetNameHash(dirNode.Name));

            writer.Write((ushort) (dirNode.Children.Count + 2));
            if (dirNode == file.RootNode || dirNode.Children.Count == 0) writer.Write(0);
            else writer.Write(nodes.IndexOf(dirNode.Children[0]));
        }

        //write nodes
        writer.Align(0x20);
        for (var i = 0; i < nodes.Count; ++i)
        {
            var node = nodes[i];

            var id = (ushort) i;
            var hash = GetNameHash(node.Name);
            var nameOffset = stringTable[node.Name];
            var flags = 0;
            var offset = 0;
            var size = 0;

            if (node is RarcFileNode fileNode)
            {
                flags = 0x01;

                //get preload flag + data offset
                switch (fileNode.Preload)
                {
                    case PreloadType.MRAM:
                        flags |= 0x10;
                        offset = GetDataOffset(mramNodes, fileNode);
                        break;
                    case PreloadType.ARAM:
                        flags |= 0x20;
                        offset = mramSize + BinaryUtils.GetOffset(mramSize, 0x20) + GetDataOffset(aramNodes, fileNode);
                        break;
                    case PreloadType.DVD:
                        flags |= 0x40;
                        offset = mramSize + BinaryUtils.GetOffset(mramSize, 0x20) + aramSize + BinaryUtils.GetOffset(aramSize, 0x20) + GetDataOffset(dvdNodes, fileNode);
                        break;
                }

                //get compression flag
                if (fileNode.Data.Length > 3)
                {
                    switch (Encoding.ASCII.GetString(fileNode.Data, 0, 4))
                    {
                        case "Yay0":
                            flags |= 0x04;
                            break;
                        case "Yaz0":
                            flags |= 0x04;
                            flags |= 0x80;
                            break;
                    }
                }

                size = fileNode.Data.Length;
            }
            else if (node is RarcDirectoryNode dirNode)
            {
                id = 0xFFFF;
                flags = 0x02;
                offset = dirMap[dirNode];
                size = 0x10;
            }

            writer.Write(id);
            writer.Write(hash);
            writer.Write(nameOffset | (flags << 24));
            writer.Write(offset);
            writer.Write(size);
            writer.Pad(4);
        }

        //write string table
        writer.Align(0x20);
        foreach (var name in stringTable.Keys) writer.Write(name + "\0");

        //write data
        writer.Align(0x20);
        var dataOffset = writer.Position;
        foreach (var node in mramNodes)
        {
            writer.Align(0x20);
            writer.Write(node.Data);
        }
        foreach (var node in aramNodes)
        {
            writer.Align(0x20);
            writer.Write(node.Data);
        }
        foreach (var node in dvdNodes)
        {
            writer.Align(0x20);
            writer.Write(node.Data);
        }

        //add remaining header data
        var fileSize = writer.Position;
        writer.JumpTo(0x04);
        writer.Write((uint) fileSize);
        writer.Skip(4);
        writer.Write((uint) dataOffset - 0x20);
        writer.Write((uint) dataSize);
        writer.Write((uint) mramSize);
        writer.Write((uint) aramSize);
    }
    #endregion

    #region private methods
    private static Dictionary<string, int> BuildStringTable(RarcDirectoryNode rootNode)
    {
        var table = new Dictionary<string, int>
        {
            {".", 0},
            {"..", 2}
        };
        var offset = 5;

        var nodeStack = new Stack<IRarcNode>();
        nodeStack.Push(rootNode);
        while (nodeStack.Count > 0)
        {
            var node = nodeStack.Pop();
            table.TryAdd(node.Name, offset);
            offset += node.Name.Length + 1;

            if (node is RarcDirectoryNode dirNode)
            {
                nodeStack.PushRange(dirNode.Children.OrderBy(n => n, NodeComparer).Reverse());
            }
        }

        return table;
    }

    private static ushort GetNameHash(string name)
    {
        ushort hash = 0;
        var multiplier = (ushort) (name.Length > 1 ? 3 : name.Length == 1 ? 2 : 1);

        foreach (var c in name)
        {
            hash *= multiplier;
            hash += c;
        }

        return hash;
    }

    private static int GetDataOffset(List<RarcFileNode> nodes, RarcFileNode node)
    {
        var offset = 0;

        foreach (var fileNode in nodes)
        {
            if (fileNode == node) break;
            offset += fileNode.Data.Length + BinaryUtils.GetOffset(fileNode.Data.Length, 0x20);
        }

        return offset;
    }
    #endregion

    #region helper class
    private class RarcNodeComparer : IComparer<IRarcNode>
    {
        public int Compare(IRarcNode? x, IRarcNode? y)
        {
            if (ReferenceEquals(x, y)) return 0;
            if (x is null) return 1;
            if (y is null) return -1;
            if (x.GetType() == y.GetType()) return string.Compare(x.Name, y.Name, StringComparison.OrdinalIgnoreCase);
            return x is RarcFileNode ? -1 : 1;
        }
    }
    #endregion
}