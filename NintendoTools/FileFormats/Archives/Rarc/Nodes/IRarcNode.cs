﻿namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// The base interface for all RARC nodes.
/// </summary>
public interface IRarcNode
{
    /// <summary>
    /// The name of the file or directory.
    /// </summary>
    public string Name { get; }
}