﻿using System.Collections.Generic;

namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// A class for a directory-type node.
/// </summary>
public class RarcDirectoryNode : IRarcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// A list of child nodes of this directory.
    /// </summary>
    public List<IRarcNode> Children { get; set; } = [];
}