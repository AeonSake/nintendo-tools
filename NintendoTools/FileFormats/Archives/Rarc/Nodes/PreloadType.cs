﻿namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// An <see langword="enum"/> of preload types for RARC files.
/// </summary>
public enum PreloadType
{
    /// <summary>
    /// Preload content into MRAM.
    /// </summary>
    MRAM,

    /// <summary>
    /// Preload content into ARAM.
    /// </summary>
    ARAM,

    /// <summary>
    /// Don't preload content, load from DVD directly into MRAM.
    /// </summary>
    DVD
}