﻿namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// A class for a file-type node.
/// </summary>
public class RarcFileNode : IRarcNode
{
    /// <inheritdoc/>
    public required string Name { get; set; }

    /// <summary>
    /// Specifies how the file should be loaded.
    /// </summary>
    public PreloadType Preload { get; set; }

    /// <summary>
    /// The content of the file as <see cref="byte"/> array.
    /// </summary>
    public required byte[] Data { get; set; }
}