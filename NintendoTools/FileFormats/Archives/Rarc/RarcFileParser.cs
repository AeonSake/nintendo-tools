﻿using System;
using System.IO;
using System.Text;
using NintendoTools.Utils;

namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// A class for parsing RARC archives.
/// </summary>
public class RarcFileParser : IFileParser<RarcFile>
{
    #region public methods
    /// <inheritdoc cref="IFileParser.CanParse"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanParseStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream, true);
        return CanParse(reader);
    }
    #endregion

    #region IFileParser interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanParse(Stream fileStream) => CanParseStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public RarcFile Parse(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        using var reader = new FileReader(fileStream);
        if (!CanParse(reader)) throw new InvalidDataException("File is not a RARC file.");

        var headerSize = reader.ReadUInt32At(0x08);
        var dataOffset = reader.ReadUInt32() + headerSize;
        var dirCount = reader.ReadUInt32At(headerSize);
        var dirOffset = reader.ReadUInt32() + headerSize;
        var nodeCount = reader.ReadInt32();
        var nodeOffset = reader.ReadUInt32() + headerSize;
        var stringTableOffset = reader.ReadUInt32At(headerSize + 0x14) + headerSize;
        var syncedIds = reader.ReadByteAt(headerSize + 0x1A);

        //parse root directories
        RarcDirectoryNode? rootNode = null;
        var dirEntries = new DirEntry[dirCount];
        for (var i = 0; i < dirCount; ++i)
        {
            reader.JumpTo(dirOffset + i * 0x10);

            var type = reader.ReadString(4, Encoding.ASCII);
            var nameOffset = reader.ReadUInt32();
            reader.Skip(2);
            var fileCount = reader.ReadUInt16();
            var firstFile = reader.ReadInt32();
            var name = reader.ReadTerminatedStringAt(stringTableOffset + nameOffset);

            var node = new RarcDirectoryNode {Name = name};
            if (type is "ROOT" or "TOOR") rootNode = node;

            dirEntries[i] = new DirEntry
            {
                FileCount = fileCount,
                FirstFile = firstFile,
                Node = node
            };
        }
        if (rootNode is null) throw new InvalidDataException("Missing root node.");

        //parse nodes
        var nodes = new IRarcNode?[nodeCount];
        for (var i = 0; i < nodeCount; ++i)
        {
            reader.JumpTo(nodeOffset + i * 0x14 + 4);

            var flags = reader.ReadUInt32();
            var nameOffset = flags & 0x00FFFFFF;
            flags >>= 24;

            var pos = reader.Position;
            var name = reader.ReadTerminatedStringAt(stringTableOffset + nameOffset);
            if (name is "." or "..") continue;

            reader.JumpTo(pos);
            if ((flags & 0x01) > 0) //file
            {
                var fileOffset = reader.ReadUInt32();
                var fileSize = reader.ReadUInt32();
                var data = reader.ReadBytesAt(dataOffset + fileOffset, (int) fileSize);

                //get preload type
                PreloadType preload;
                if ((flags | 0x10) > 0) preload = PreloadType.MRAM;
                else if ((flags | 0x20) > 0) preload = PreloadType.ARAM;
                else if ((flags | 0x40) > 0) preload = PreloadType.DVD;
                else throw new InvalidDataException($"Invalid preload flag: 0x{flags:X2}");

                nodes[i] = new RarcFileNode
                {
                    Name = name,
                    Preload = preload,
                    Data = data
                };
            }
            else if ((flags & 0x02) > 0) //directory
            {
                var folderIndex = reader.ReadUInt32();
                nodes[i] = dirEntries[folderIndex].Node;
            }
            else throw new InvalidDataException("Unknown node type.");
        }

        //build node tree
        foreach (var entry in dirEntries)
        {
            for (var i = entry.FirstFile; i < entry.FirstFile + entry.FileCount; ++i)
            {
                var node = nodes[i];
                if (node is null) continue;
                entry.Node.Children.Add(node);
            }
        }

        return new RarcFile
        {
            BigEndian = reader.IsBigEndian,
            SyncedIds = syncedIds > 0,
            RootNode = rootNode
        };
    }
    #endregion

    #region private members
    //verifies that the file is a RARC archive
    private static bool CanParse(FileReader reader)
    {
        if (reader.Length < 4) return false;

        switch (reader.ReadStringAt(0, 4, Encoding.ASCII))
        {
            case "RARC":
                reader.IsBigEndian = true;
                return true;
            case "CRAR":
                return true;
            default:
                return false;
        }
    }
    #endregion

    #region helper class
    private class DirEntry
    {
        public required int FileCount { get; init; }

        public required int FirstFile { get; init; }

        public required RarcDirectoryNode Node { get; init; }
    }
    #endregion
}