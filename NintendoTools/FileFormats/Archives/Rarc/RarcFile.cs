﻿namespace NintendoTools.FileFormats.Rarc;

/// <summary>
/// A class holding information about an RARC archive file.
/// </summary>
public class RarcFile : IVariableEndianFile
{
    /// <inheritdoc/>
    public bool BigEndian { get; set; }

    /// <summary>
    /// Whether to sync file IDs to file index.
    /// </summary>
    public bool SyncedIds { get; set; }

    /// <summary>
    /// The root node of the RARC archive.
    /// </summary>
    public required RarcDirectoryNode RootNode { get; set; }
}