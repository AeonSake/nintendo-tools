﻿using System;
using System.IO;

namespace NintendoTools.Utils;

/// <summary>
/// A class for wrapping a stream with a fixed working window.
/// </summary>
public sealed class StreamSpan : Stream
{
    #region private members
    private readonly Stream _baseStream;
    private readonly long _offset;
    private readonly bool _writable;
    #endregion

    #region constructor
    /// <summary>
    /// Initializes a new instance of the <see cref="StreamSpan"/> class.
    /// </summary>
    /// <param name="baseStream">The base <see cref="Stream"/> to use for reading/writing.</param>
    /// <param name="offset">The absolute offset into the <see cref="Stream"/> for the working window.</param>
    /// <param name="length">The length of the working window.</param>
    /// <param name="writable">Whether the <see cref="StreamSpan"/> can be written to.</param>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="ArgumentException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public StreamSpan(Stream baseStream, long offset, long length, bool writable = true)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(baseStream, nameof(baseStream));
        #else
        if (baseStream is null) throw new ArgumentNullException(nameof(baseStream));
        #endif
        if (offset < 0 || offset >= baseStream.Length) throw new ArgumentOutOfRangeException(nameof(offset));
        if (length < 0 || offset + length > baseStream.Length) throw new ArgumentOutOfRangeException(nameof(length));

        _baseStream = baseStream;
        _baseStream.Position = offset;
        _offset = offset;
        _writable = writable;
        Length = length;
    }
    #endregion

    #region Stream interface
    /// <inheritdoc/>
    public override bool CanRead => _baseStream.CanRead;

    /// <inheritdoc/>
    public override bool CanSeek => _baseStream.CanSeek;

    /// <inheritdoc/>
    public override bool CanTimeout => _baseStream.CanTimeout;

    /// <inheritdoc/>
    public override bool CanWrite => _writable && _baseStream.CanWrite;

    /// <inheritdoc/>
    public override long Length { get; }

    /// <inheritdoc/>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public override long Position
    {
        get => _baseStream.Position - _offset;
        set
        {
            if (value < 0 || value >= Length) throw new ArgumentOutOfRangeException(nameof(Position));
            _baseStream.Position = _offset + value;
        }
    }

    /// <inheritdoc/>
    public override int ReadTimeout
    {
        get => _baseStream.ReadTimeout;
        set => _baseStream.ReadTimeout = value;
    }

    /// <inheritdoc/>
    public override int WriteTimeout
    {
        get => _baseStream.WriteTimeout;
        set => _baseStream.WriteTimeout = value;
    }

    /// <inheritdoc/>
    public override void Flush() => _baseStream.Flush();

    /// <inheritdoc/>
    public override int Read(byte[] buffer, int offset, int count)
    {
        if (Position + count >= Length) count = (int) (Length - Position);
        return count < 1 ? 0 : _baseStream.Read(buffer, offset, count);
    }

    /// <inheritdoc/>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public override long Seek(long offset, SeekOrigin origin)
    {
        if (origin is SeekOrigin.Begin)
        {
            if (offset < 0 || offset >= Length) throw new ArgumentOutOfRangeException(nameof(offset));
            return _baseStream.Seek(_offset + offset, SeekOrigin.Begin);
        }
        if (origin is SeekOrigin.End)
        {
            if (offset < 0 || offset >= Length) throw new ArgumentOutOfRangeException(nameof(offset));
            return _baseStream.Seek(_baseStream.Length - _offset - Length + offset, SeekOrigin.End);
        }

        if (Position + offset < 0 || Position + offset >= Length) throw new ArgumentOutOfRangeException(nameof(offset));
        return _baseStream.Seek(offset, origin);
    }

    /// <inheritdoc/>
    /// <exception cref="NotSupportedException"></exception>
    public override void SetLength(long value) => throw new NotSupportedException();

    /// <inheritdoc/>
    /// <exception cref="InvalidOperationException"></exception>
    /// <exception cref="ArgumentOutOfRangeException"></exception>
    public override void Write(byte[] buffer, int offset, int count)
    {
        if (count == 0) return;
        if (!_writable) throw new InvalidOperationException("Stream span is read-only.");
        if (Position + count >= Length) throw new ArgumentOutOfRangeException(nameof(count));
        _baseStream.Write(buffer, offset, count);
    }
    #endregion
}