﻿using System;
using System.Collections.Generic;
using System.Runtime.CompilerServices;

namespace NintendoTools.Utils;

//https://github.com/CommunityToolkit/dotnet/blob/657c6971a8d42655c648336b781639ed96c2c49f/src/CommunityToolkit.Diagnostics/Extensions/TypeExtensions.cs
internal static class TypeExtensions
{
    private static readonly IReadOnlyDictionary<Type, string> BuiltInTypesMap = new Dictionary<Type, string>
    {
        [typeof(bool)]    = "bool",
        [typeof(byte)]    = "byte",
        [typeof(sbyte)]   = "sbyte",
        [typeof(short)]   = "short",
        [typeof(ushort)]  = "ushort",
        [typeof(char)]    = "char",
        [typeof(int)]     = "int",
        [typeof(uint)]    = "uint",
        [typeof(float)]   = "float",
        [typeof(long)]    = "long",
        [typeof(ulong)]   = "ulong",
        [typeof(double)]  = "double",
        [typeof(decimal)] = "decimal",
        [typeof(object)]  = "object",
        [typeof(string)]  = "string",
        [typeof(void)]    = "void"
    };

    private static readonly ConditionalWeakTable<Type, string> DisplayNames = new();

    public static string ToTypeString(this Type type)
    {
        return DisplayNames.GetValue(type, t =>
        {
            if (t.IsByRef)
            {
                t = t.GetElementType()!;
                return $"{FormatDisplayString(t, 0, t.GetGenericArguments())}&";
            }

            if (t.IsPointer)
            {
                var depth = 0;
                while (t.IsPointer)
                {
                    depth++;
                    t = t.GetElementType()!;
                }

                return $"{FormatDisplayString(t, 0, t.GetGenericArguments())}{new string('*', depth)}";
            }

            return FormatDisplayString(t, 0, t.GetGenericArguments());
        });
    }

    private static string FormatDisplayString(Type type, int genericTypeOffset, ReadOnlySpan<Type> typeArguments)
    {
        if (BuiltInTypesMap.TryGetValue(type, out var typeName)) return typeName!;

        if (type.IsArray)
        {
            var elementType = type.GetElementType()!;
            var rank = type.GetArrayRank();
            return $"{FormatDisplayString(elementType, 0, elementType.GetGenericArguments())}[{new string(',', rank - 1)}]";
        }

        if (type.IsGenericType)
        {
            var genericTypeDefinition = type.GetGenericTypeDefinition();

            if (genericTypeDefinition == typeof(Nullable<>))
            {
                var nullableArguments = type.GetGenericArguments();
                return $"{FormatDisplayString(nullableArguments[0], 0, nullableArguments)}?";
            }

            if (genericTypeDefinition == typeof(ValueTuple<>) ||
                genericTypeDefinition == typeof(ValueTuple<,>) ||
                genericTypeDefinition == typeof(ValueTuple<,,>) ||
                genericTypeDefinition == typeof(ValueTuple<,,,>) ||
                genericTypeDefinition == typeof(ValueTuple<,,,,>) ||
                genericTypeDefinition == typeof(ValueTuple<,,,,,>) ||
                genericTypeDefinition == typeof(ValueTuple<,,,,,,>) ||
                genericTypeDefinition == typeof(ValueTuple<,,,,,,,>))
            {
                var tupleArguments = type.GetGenericArguments();
                if (tupleArguments[0].IsGenericParameter) return $"({new string(',', tupleArguments.Length - 1)})";

                var formattedTypes = FormatDisplayStringForAllTypes(tupleArguments);
                return $"({string.Join(", ", formattedTypes)})";
            }
        }

        string displayName;
        if (type.Name.AsSpan().IndexOf('`') != -1)
        {
            var tokens = type.Name.Split('`');
            var genericArgumentsCount = int.Parse(tokens[1]);
            var typeArgumentsOffset = typeArguments.Length - genericTypeOffset - genericArgumentsCount;
            var currentTypeArguments = typeArguments.Slice(typeArgumentsOffset, genericArgumentsCount).ToArray();

            // Special case generic type parameters (same as with tuples)
            if (currentTypeArguments[0].IsGenericParameter)
            {
                displayName = $"{tokens[0]}<{new string(',', currentTypeArguments.Length - 1)}>";
            }
            else
            {
                var formattedTypes = FormatDisplayStringForAllTypes(currentTypeArguments);
                displayName = $"{tokens[0]}<{string.Join(", ", formattedTypes)}>";
            }

            genericTypeOffset += genericArgumentsCount;
        }
        else
        {
            displayName = type.Name;
        }

        if (type is {IsNested: true, IsGenericParameter: false}) return $"{FormatDisplayString(type.DeclaringType!, genericTypeOffset, typeArguments)}.{displayName}";
        return $"{type.Namespace}.{displayName}";
    }

    private static IEnumerable<string> FormatDisplayStringForAllTypes(Type[] types)
    {
        foreach (var type in types)
        {
            yield return FormatDisplayString(type, 0, type.GetGenericArguments());
        }
    }
}