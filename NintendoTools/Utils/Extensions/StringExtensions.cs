﻿using System;
using System.IO;
using System.Runtime.CompilerServices;
#if !NET6_0_OR_GREATER
using System.Text;
#endif
using NintendoTools.FileFormats;

namespace NintendoTools.Utils;

internal static class StringExtensions
{
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string Reverse(this string str)
    {
        var chars = str.ToCharArray();
        Array.Reverse(chars);
        return new string(chars);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static bool ContainsAny(this string str, params char[] values)
    {
        foreach (var c in values)
        {
            if (str.Contains(c)) return true;
        }
        return false;
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static string NormalizeLineEndings(this string str, NewlineType type)
    {
        var replacement = type switch
        {
            NewlineType.LF   => "\n",
            NewlineType.CRLF => "\r\n",
            NewlineType.CR   => "\r",
            _                => throw new InvalidDataException("Unknown newline type.")
        };

        return str.ReplaceLineEndings(replacement);
    }

    #if !NET6_0_OR_GREATER
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    private static string ReplaceLineEndings(this string str, string replacementText)
    {
        var builder = new StringBuilder();
        using var reader = new StringReader(str);
        while (true)
        {
            var line = reader.ReadLine();
            if (line is null) break;
            builder.Append(line).Append(replacementText);
        }

        return builder.ToString(0, builder.Length - replacementText.Length);
    }
    #endif
}