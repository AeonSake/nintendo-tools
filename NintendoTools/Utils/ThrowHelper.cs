﻿using System;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Runtime.CompilerServices;

namespace NintendoTools.Utils;

[DebuggerStepThrough]
internal static class ThrowHelper
{
    #region public methods
    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNull<T>(T? value, [CallerArgumentExpression(nameof(value))] string name = "")
    {
        if (value is not null) return;
        ExceptionBuilder.ThrowArgumentNullExceptionForIsNull(value, name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNullOrEmpty(string? value, [CallerArgumentExpression(nameof(value))] string name = "")
    {
        if (!string.IsNullOrEmpty(value)) return;
        ExceptionBuilder.ThrowArgumentExceptionForIsNullOrEmpty(value, name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfNullOrWhitespace(string? value, [CallerArgumentExpression(nameof(value))] string name = "")
    {
        if (!string.IsNullOrWhiteSpace(value)) return;
        ExceptionBuilder.ThrowArgumentExceptionForIsNullOrWhiteSpace(value, name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfTrue([DoesNotReturnIf(false)] bool value, [CallerArgumentExpression(nameof(value))] string name = "")
    {
        if (!value) return;
        ExceptionBuilder.ThrowArgumentExceptionForIsTrue(name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfTrue([DoesNotReturnIf(true)] bool value, string name, string message)
    {
        if (!value) return;
        ExceptionBuilder.ThrowArgumentExceptionForIsTrue(name, message);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfFalse([DoesNotReturnIf(false)] bool value, [CallerArgumentExpression(nameof(value))] string name = "")
    {
        if (value) return;
        ExceptionBuilder.ThrowArgumentExceptionForIsFalse(name);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfFalse([DoesNotReturnIf(false)] bool value, string name, string message)
    {
        if (value) return;
        ExceptionBuilder.ThrowArgumentExceptionForIsFalse(name, message);
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfEndOfStream(int value)
    {
        if (value != -1) return;
        ExceptionBuilder.ThrowEndOfStreamException();
    }

    [MethodImpl(MethodImplOptions.AggressiveInlining)]
    public static void ThrowIfEndOfStream(int requiredBytes, int actualBytes)
    {
        if (requiredBytes == actualBytes) return;
        ExceptionBuilder.ThrowEndOfStreamExceptionForNotEnoughBytes(requiredBytes, actualBytes);
    }
    #endregion

    #region private methods
    private static string AssertString(object? obj)
    {
        return obj switch
        {
            string _ => $"\"{obj}\"",
            null     => "null",
            _        => $"<{obj}>"
        };
    }
    #endregion

    #region helper class
    [StackTraceHidden]
    private static class ExceptionBuilder
    {
        [DoesNotReturn]
        public static void ThrowArgumentNullExceptionForIsNull<T>(T value, string name)
        {
            throw new ArgumentNullException(name, $"Parameter {AssertString(name)} ({typeof(T).ToTypeString()}) must be not null.");
        }

        [DoesNotReturn]
        public static void ThrowArgumentExceptionForIsNullOrEmpty(string? value, string name)
        {
            throw new ArgumentException($"Parameter {AssertString(name)} (string) must be null or empty, was {AssertString(value)}.", name);
        }

        [DoesNotReturn]
        public static void ThrowArgumentExceptionForIsNullOrWhiteSpace(string? text, string name)
        {
            throw new ArgumentException($"Parameter {AssertString(name)} (string) must be null or whitespace, was {AssertString(text)}.", name);
        }

        [DoesNotReturn]
        public static void ThrowArgumentExceptionForIsTrue(string name)
        {
            throw new ArgumentException($"Parameter {AssertString(name)} must be false, was true.", name);
        }

        [DoesNotReturn]
        public static void ThrowArgumentExceptionForIsTrue(string name, string message)
        {
            throw new ArgumentException($"Parameter {AssertString(name)} must be false, was true: {AssertString(message)}.", name);
        }

        [DoesNotReturn]
        public static void ThrowArgumentExceptionForIsFalse(string name)
        {
            throw new ArgumentException($"Parameter {AssertString(name)} must be true, was false.", name);
        }

        [DoesNotReturn]
        public static void ThrowArgumentExceptionForIsFalse(string name, string message)
        {
            throw new ArgumentException($"Parameter {AssertString(name)} must be true, was false: {AssertString(message)}.", name);
        }

        [DoesNotReturn]
        public static void ThrowEndOfStreamException()
        {
            throw new EndOfStreamException("Attempted to read beyond the end of the stream.");
        }

        [DoesNotReturn]
        public static void ThrowEndOfStreamExceptionForNotEnoughBytes(int requiredBytes, int actualBytes)
        {
            throw new EndOfStreamException($"Reached the end of the stream while attempting to read {requiredBytes} bytes. Only {actualBytes} bytes were available.");
        }
    }
    #endregion
}