﻿using System.IO;
using System.Text;
using NintendoTools.FileFormats;

namespace NintendoTools.Utils;

// ReSharper disable InconsistentNaming
internal static class EncodingList
{
    static EncodingList()
    {
        Encoding.RegisterProvider(CodePagesEncodingProvider.Instance);

        ASCII = Encoding.ASCII;
        Latin1 = Encoding.GetEncoding(1252); //might actually be code page 28591
        ShiftJIS = Encoding.GetEncoding(932);
        UTF8 = Encoding.UTF8;
        UTF16 = Encoding.Unicode;
        BigEndianUTF16 = Encoding.BigEndianUnicode;
        UTF32 = Encoding.UTF32;
        BigEndianUTF32 = new UTF32Encoding(true, true);
    }

    public static readonly Encoding ASCII;
    public static readonly Encoding Latin1;
    public static readonly Encoding ShiftJIS;
    public static readonly Encoding UTF8;
    public static readonly Encoding UTF16;
    public static readonly Encoding BigEndianUTF16;
    public static readonly Encoding UTF32;
    public static readonly Encoding BigEndianUTF32;

    public static Encoding GetEncoding(EncodingType type, bool bigEndian) => (type, bigEndian) switch
    {
        (EncodingType.ASCII, _)     => ASCII,
        (EncodingType.Latin1, _)    => Latin1,
        (EncodingType.ShiftJIS, _)  => ShiftJIS,
        (EncodingType.UTF8, _)      => UTF8,
        (EncodingType.UTF16, false) => UTF16,
        (EncodingType.UTF16, true)  => BigEndianUTF16,
        (EncodingType.UTF32, false) => UTF32,
        (EncodingType.UTF32, true)  => BigEndianUTF32,
        _                           => throw new InvalidDataException("Unknown encoding type.")
    };
}