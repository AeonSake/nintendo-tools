﻿using System;
using System.IO;
using AuroraLib.Compression.Algorithms;

namespace NintendoTools.Compression.Lz77;

/// <summary>
/// A class for LZ77 compression.
/// </summary>
public class Lz77Compressor : ICompressor
{
    #region public properties
    /// <summary>
    /// Gets or sets a value indicating whether to compress in single- or multi-chunk mode.
    /// </summary>
    public bool Chunked { get; set; }
    #endregion

    #region ICompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public void Compress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif

        var compressor = new LZ77
        {
            Type = Chunked ? LZ77.CompressionType.ChunkLZ10 : LZ77.CompressionType.LZ10
        };

        sourceStream.Seek(0, SeekOrigin.Begin);
        compressor.Compress(sourceStream, targetStream);
    }
    #endregion
}