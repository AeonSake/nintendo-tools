﻿using System;
using System.IO;

namespace NintendoTools.Compression;

/// <summary>
/// An extension class for <see cref="IDecompressor"/> types.
/// </summary>
public static class DecompressorExtensions
{
    /// <summary>
    /// Validates whether the given data can be decompressed with this decompressor instance.
    /// </summary>
    /// <param name="decompressor">The <see cref="IDecompressor"/> instance to use.</param>
    /// <param name="data">The data to check.</param>
    /// <returns><see langword="true"/> if can be decompressed; otherwise <see langword="false"/>.</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanDecompress(this IDecompressor decompressor, byte[] data)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(decompressor, nameof(decompressor));
        ArgumentNullException.ThrowIfNull(data, nameof(data));
        #else
        if (decompressor is null) throw new ArgumentNullException(nameof(decompressor));
        if (data is null) throw new ArgumentNullException(nameof(data));
        #endif

        using var stream = new MemoryStream(data, false);
        return decompressor.CanDecompress(stream);
    }

    /// <summary>
    /// Decompresses a byte array.
    /// </summary>
    /// <param name="decompressor">The <see cref="IDecompressor"/> instance to use.</param>
    /// <param name="data">The data to decompress.</param>
    /// <returns>The decompressed data.</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static byte[] Decompress(this IDecompressor decompressor, byte[] data)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(decompressor, nameof(decompressor));
        ArgumentNullException.ThrowIfNull(data, nameof(data));
        #else
        if (decompressor is null) throw new ArgumentNullException(nameof(decompressor));
        if (data is null) throw new ArgumentNullException(nameof(data));
        #endif

        using var stream = new MemoryStream(data, false);
        using var resultStream = new MemoryStream();
        decompressor.Decompress(stream, resultStream);
        return resultStream.ToArray();
    }
}