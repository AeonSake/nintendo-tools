﻿using System;
using System.IO;

namespace NintendoTools.Compression.ZLib;

/// <summary>
/// A class for ZLib decompression.
/// </summary>
public class ZLibDecompressor : IDecompressor
{
    #region public methods
    /// <inheritdoc cref="IDecompressor.CanDecompress"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanDecompressStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        fileStream.Seek(0, SeekOrigin.Begin);

        //ZLib does not have a file magic but the first two bytes contain special instructions (64 possible byte combinations)
        //Nintendo tends to only use the combination: 0x789C (max window-size, deflate, default compression level)
        //only check this combination to reduce the chance for false positives
        Span<byte> buffer = stackalloc byte[2];
        _ = fileStream.Read(buffer);
        return buffer is [0x78, 0x9C];
    }
    #endregion

    #region IDecompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanDecompress(Stream fileStream) => CanDecompressStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public void Decompress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif
        if (!CanDecompressStatic(sourceStream)) throw new InvalidDataException("Data is not ZLib compressed.");

        var decompressor = new AuroraLib.Compression.Algorithms.ZLib();

        sourceStream.Seek(0, SeekOrigin.Begin);
        decompressor.Decompress(sourceStream, targetStream);
    }
    #endregion
}