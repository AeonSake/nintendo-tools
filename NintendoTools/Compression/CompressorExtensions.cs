﻿using System;
using System.IO;

namespace NintendoTools.Compression;

/// <summary>
/// An extension class for <see cref="ICompressor"/> types.
/// </summary>
public static class CompressorExtensions
{
    /// <summary>
    /// Compresses a byte array.
    /// </summary>
    /// <param name="compressor">The <see cref="ICompressor"/> instance to use.</param>
    /// <param name="data">The data to compress.</param>
    /// <returns>The compressed data.</returns>
    /// <exception cref="ArgumentNullException"></exception>
    public static byte[] Compress(this ICompressor compressor, byte[] data)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(compressor, nameof(compressor));
        ArgumentNullException.ThrowIfNull(data, nameof(data));
        #else
        if (compressor is null) throw new ArgumentNullException(nameof(compressor));
        if (data is null) throw new ArgumentNullException(nameof(data));
        #endif

        using var stream = new MemoryStream(data, false);
        using var resultStream = new MemoryStream();
        compressor.Compress(stream, resultStream);
        return resultStream.ToArray();
    }
}