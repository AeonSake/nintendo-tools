﻿using System;
using System.IO;
using ZstdSharp;

namespace NintendoTools.Compression.Zstd;

/// <summary>
/// A class for Zstandard decompression.
/// </summary>
public class ZstdDecompressor : IDecompressor
{
    #region public properties
    /// <summary>
    /// Gets or sets a dictionary for decompression.
    /// </summary>
    public byte[]? Dict { get; set; }
    #endregion

    #region public methods
    /// <inheritdoc cref="IDecompressor.CanDecompress"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanDecompressStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        fileStream.Seek(0, SeekOrigin.Begin);

        Span<byte> buffer = stackalloc byte[4];
        _ = fileStream.Read(buffer);
        return buffer is [0x28, 0xB5, 0x2F, 0xFD];
    }
    #endregion

    #region IDecompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanDecompress(Stream fileStream) => CanDecompressStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public void Decompress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif
        if (!CanDecompressStatic(sourceStream)) throw new InvalidDataException("Data is not ZSTD compressed.");

        sourceStream.Seek(0, SeekOrigin.Begin);
        using var decompressionStream = new DecompressionStream(sourceStream);
        if (Dict is not null) decompressionStream.LoadDictionary(Dict);
        decompressionStream.CopyTo(targetStream);
    }
    #endregion
}