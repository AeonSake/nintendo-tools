﻿using System;
using System.IO;
using ZstdSharp;

namespace NintendoTools.Compression.Zstd;

/// <summary>
/// A class for Zstandard compression.
/// </summary>
public class ZstdCompressor : ICompressor
{
    #region public properties
    /// <summary>
    /// Gets or sets the compression level to use.
    /// </summary>
    public int CompressionLevel { get; set; }

    /// <summary>
    /// Gets or sets a dictionary for compression.
    /// </summary>
    public byte[]? Dict { get; set; }
    #endregion

    #region ICompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public void Compress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif

        sourceStream.Seek(0, SeekOrigin.Begin);
        using var compressionStream = new CompressionStream(targetStream, CompressionLevel);
        if (Dict is not null) compressionStream.LoadDictionary(Dict);
        compressionStream.SetPledgedSrcSize((ulong) sourceStream.Length);
        sourceStream.CopyTo(compressionStream);
    }
    #endregion
}