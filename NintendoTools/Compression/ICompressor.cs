﻿using System.IO;

namespace NintendoTools.Compression;

/// <summary>
/// The interface for decompressor types.
/// </summary>
public interface ICompressor
{
    /// <summary>
    /// Compresses a stream.
    /// </summary>
    /// <param name="sourceStream">The stream to compress.</param>
    /// <param name="targetStream">The output stream.</param>
    public void Compress(Stream sourceStream, Stream targetStream);
}