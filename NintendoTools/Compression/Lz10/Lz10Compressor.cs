﻿using System;
using System.IO;
using AuroraLib.Compression.Algorithms;

namespace NintendoTools.Compression.Lz10;

/// <summary>
/// A class for LZ10 compression.
/// </summary>
public class Lz10Compressor : ICompressor
{
    #region ICompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public void Compress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif

        var compressor = new LZ10();

        sourceStream.Seek(0, SeekOrigin.Begin);
        compressor.Compress(sourceStream, targetStream);
    }
    #endregion
}