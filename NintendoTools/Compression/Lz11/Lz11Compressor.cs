﻿using System;
using System.IO;
using AuroraLib.Compression.Algorithms;

namespace NintendoTools.Compression.Lz11;

/// <summary>
/// A class for LZ11 compression.
/// </summary>
public class Lz11Compressor : ICompressor
{
    #region ICompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public void Compress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif

        var compressor = new LZ11();

        sourceStream.Seek(0, SeekOrigin.Begin);
        compressor.Compress(sourceStream, targetStream);
    }
    #endregion
}