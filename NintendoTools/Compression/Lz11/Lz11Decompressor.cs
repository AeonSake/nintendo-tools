﻿using System;
using System.IO;
using AuroraLib.Compression.Algorithms;

namespace NintendoTools.Compression.Lz11;

/// <summary>
/// A class for LZ11 decompression.
/// </summary>
public class Lz11Decompressor : IDecompressor
{
    #region public methods
    /// <inheritdoc cref="IDecompressor.CanDecompress"/>
    /// <exception cref="ArgumentNullException"></exception>
    public static bool CanDecompressStatic(Stream fileStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(fileStream, nameof(fileStream));
        #else
        if (fileStream is null) throw new ArgumentNullException(nameof(fileStream));
        #endif

        fileStream.Seek(0, SeekOrigin.Begin);
        return LZ11.IsMatchStatic(fileStream);
    }
    #endregion

    #region IDecompressor interface
    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    public bool CanDecompress(Stream fileStream) => CanDecompressStatic(fileStream);

    /// <inheritdoc/>
    /// <exception cref="ArgumentNullException"></exception>
    /// <exception cref="InvalidDataException"></exception>
    public void Decompress(Stream sourceStream, Stream targetStream)
    {
        #if NET6_0_OR_GREATER
        ArgumentNullException.ThrowIfNull(sourceStream, nameof(sourceStream));
        ArgumentNullException.ThrowIfNull(targetStream, nameof(targetStream));
        #else
        if (sourceStream is null) throw new ArgumentNullException(nameof(sourceStream));
        if (targetStream is null) throw new ArgumentNullException(nameof(targetStream));
        #endif
        if (!CanDecompressStatic(sourceStream)) throw new InvalidDataException("Data is not LZ11 compressed.");

        var decompressor = new LZ11();

        sourceStream.Seek(0, SeekOrigin.Begin);
        decompressor.Decompress(sourceStream, targetStream);
    }
    #endregion
}