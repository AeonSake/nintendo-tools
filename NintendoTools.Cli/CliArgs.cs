﻿namespace NintendoTools.Cli;

internal sealed class CliArgs
{
    #region constructor
    private CliArgs(IList<string> commands, IDictionary<string, string> arguments, IList<string> flags)
    {
        Commands = commands;
        Arguments = arguments;
        Flags = flags;
    }
    #endregion

    #region public properties
    public IList<string> Commands { get; }

    public IDictionary<string, string> Arguments { get; }

    public IList<string> Flags { get; }
    #endregion

    #region public methods
    public static CliArgs Parse(string[] args)
    {
        var commands = new List<string>();
        var arguments = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        var flags = new List<string>();

        for (var i = 0; i < args.Length; ++i)
        {
            if (args[i].StartsWith("--")) flags.Add(CleanArg(args[i]));
            else if (args[i].StartsWith('-'))
            {
                if (args[i].Contains('='))
                {
                    var values = args[i].Split('=');
                    arguments.Add(CleanArg(values[0]), CleanValue(values[1]));
                }
                else if (i + 1 < args.Length && !args[i + 1].StartsWith('-'))
                {
                    arguments.Add(CleanArg(args[i]), CleanValue(args[++i]));
                }
            }
            else commands.Add(CleanValue(args[i]));
        }

        return new CliArgs(commands, arguments, flags);
    }
    #endregion

    #region private methods
    private static string CleanArg(string str) => str.ToLowerInvariant().Replace("_", "").TrimStart('-');

    private static string CleanValue(string str) => str.Trim('"');
    #endregion
}