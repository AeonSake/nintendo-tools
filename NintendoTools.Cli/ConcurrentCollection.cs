﻿using System.Collections;
using System.Collections.Concurrent;

namespace NintendoTools.Cli;

internal class ConcurrentCollection<T> : ICollection<T> where T : notnull
{
    private readonly ConcurrentDictionary<T, int> _items;

    #region constructors
    public ConcurrentCollection() => _items = new ConcurrentDictionary<T, int>();

    public ConcurrentCollection(IEqualityComparer<T> comparer) => _items = new ConcurrentDictionary<T, int>(comparer);
    #endregion

    #region ICollection interface
    public int Count => _items.Count;

    public bool IsReadOnly => false;

    public void Add(T item) => _items.TryAdd(item, 0);

    public bool Remove(T item) => _items.TryRemove(item, out _);

    public void Clear() => _items.Clear();

    public bool Contains(T item) => _items.ContainsKey(item);

    public void CopyTo(T[] array, int arrayIndex) => _items.Keys.CopyTo(array, arrayIndex);

    public IEnumerator<T> GetEnumerator() => _items.Keys.GetEnumerator();

    IEnumerator IEnumerable.GetEnumerator() => GetEnumerator();
    #endregion
}