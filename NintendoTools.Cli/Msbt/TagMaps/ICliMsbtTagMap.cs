﻿using NintendoTools.FileFormats.Msbt;

namespace NintendoTools.Cli;

internal interface ICliMsbtTagMap : IMsbtTagMap
{
    public string TagFormat { get; set; }

    public ConcurrentCollection<string> UnknownTags { get; }
}