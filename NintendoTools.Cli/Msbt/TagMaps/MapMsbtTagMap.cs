﻿using System.Text;
using NintendoTools.FileFormats.Msbt;

namespace NintendoTools.Cli;

internal class MapMsbtTagMap : ICliMsbtTagMap
{
    private readonly MsbtDynamicTagMap _tagMap;

    public MapMsbtTagMap(MsbtDynamicTagMap tagMap) => _tagMap = tagMap;

    public string TagFormat { get; set; } = string.Empty;

    public ConcurrentCollection<string> UnknownTags { get; } = new();

    public void GetTag(MsbtTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<MsbtTagArgument> functionArgs)
    {
        _tagMap.GetTag(tag, bigEndian, encoding, out tagName, out functionArgs);

        if (!_tagMap.TryGetInfo(tag.Group, tag.Type, out _)) UnknownTags.Add(tagName);
    }
}