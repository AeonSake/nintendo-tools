﻿using System.Text;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class CustomMsbtTagMap : ICliMsbtTagMap
{
    public string TagFormat { get; set; } = "fun_{0:X4}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();

    public void GetTag(MsbtTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<MsbtTagArgument> functionArgs)
    {
        tagName = string.Format(TagFormat, tag.Group, tag.Type);
        UnknownTags.Add(tagName);

        var argList = new List<MsbtTagArgument>();
        if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
        functionArgs = argList;
    }
}