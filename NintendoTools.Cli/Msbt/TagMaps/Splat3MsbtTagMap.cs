﻿using System.Text;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class Splat3MsbtTagMap : ICliMsbtTagMap
{
    #region private members
    private static readonly Dictionary<uint, string> StringValues = new()
    {
        {0, "PlayerName"},
        {1, "WeaponName"},
        {2, "ClothingName"},
        {3, "SeasonRank"},
        {4, "SelectedSplatfestTeam"},
        //{5, "???"}, //big man talk before parenthesis
        //{6, "???"},
        {7, "CoopEnemyName"},
        {8, "TableturfStage"},
        //{9, ""}, //not used
        {10, "TableturfOpponent"},
        //{11, "???"}, //only in EUfr, USfr, EUes, USes, EUit
        {12, "SeasonName"},
        //{13, ""}, //not used
        {14, "CoopMap"},
        {15, "ClothingBrand"},
        //{16, "???"}, //only in UnitName:DateTime02
        {17, "SelectedSplatfestRegion"},
        {18, "SelectedSplatfestLanguage"},
        {19, "MusicTitle"},
        {20, "MusicArtist"},
        {21, "SelectedXBattleDivision"},
        {22, "SelectedXBattleRegion"}
    };
    #endregion

    #region public properties
    public string TagFormat { get; set; } = "fun_{0:X4}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();
    #endregion

    #region IMsbtFunctionTable interface
    public void GetTag(MsbtTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<MsbtTagArgument> functionArgs)
    {
        tagName = string.Empty;
        var argList = new List<MsbtTagArgument>();
        var argOffset = 0;

        try
        {
            switch (tag.Group, tag.Type)
            {
                case (0, 0):
                    tagName = "ruby";
                    argList.Add(new MsbtTagArgument("charSpan", BitConverter.ToUInt16(tag.Args) / 2));
                    argOffset = 2;
                    argList.Add(new MsbtTagArgument("value", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (0, 2):
                    tagName = "size";
                    argList.Add(new MsbtTagArgument("value", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 3):
                    tagName = "color";
                    argList.Add(new MsbtTagArgument("id", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 4):
                    tagName = "pageBreak";
                    break;

                case (1, 0):
                    tagName = "delay";
                    argList.Add(new MsbtTagArgument("frames", BitConverter.ToUInt16(tag.Args)));
                    break;

                case (2, 0):
                    tagName = "number";
                    argList.Add(new MsbtTagArgument("index", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("type", tag.Args[1])); //0-7, maybe padding or max size
                    argList.Add(new MsbtTagArgument("arg3", tag.Args[2])); //0-2
                    argList.Add(new MsbtTagArgument("arg4", tag.Args[3])); //0-1
                    break;
                case (2, 1):
                    tagName = "string";
                    argList.Add(new MsbtTagArgument("index", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("arg2", tag.Args[1])); //maybe font size
                    break;

                case (3, var id):
                    tagName = "icon";
                    argList.Add(new MsbtTagArgument("id", id));
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    /*argList.Add(new MsbtFunctionArgument("arg1", function.Args[0]));
                    argList.Add(new MsbtFunctionArgument("arg2", function.Args[1]));
                    argList.Add(new MsbtFunctionArgument("arg3", function.Args[2]));
                    argList.Add(new MsbtFunctionArgument("arg4", function.Args[3]));
                    argList.Add(new MsbtFunctionArgument("arg5", function.Args[4]));
                    argList.Add(new MsbtFunctionArgument("arg6", function.Args[5]));
                    argList.Add(new MsbtFunctionArgument("arg7", function.Args[6]));
                    argList.Add(new MsbtFunctionArgument("arg8", function.Args[7]));*/
                    //additional data in args; maybe color, size, spacing?
                    break;

                case (4, var id):
                    tagName = "string";
                    argList.Add(new MsbtTagArgument("value", GetListValue(StringValues, id)));
                    if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg1", tag.Args[0]));
                    //if (function.Args.Length > 1) argList.Add(new MsbtFunctionArgument("arg2", function.Args[1])); //always 0
                    if (tag.Args.Length > 2) argList.Add(new MsbtTagArgument("arg3", tag.Args[2]));
                    //if (function.Args.Length > 3) argList.Add(new MsbtFunctionArgument("arg4", function.Args[3])); //always 0
                    break;

                //5,<x> //outfit strings
                //15,0 //after big man parenthesis

                case (201, 0):
                    tagName = "wordInfo";
                    argList.Add(new MsbtTagArgument("gender", tag.Args[0]));
                    //arg[1..3] always 0xFFFF00
                    break;
                case (201, 3):
                    tagName = "capitalizeNextWord";
                    break;
                //201,4 //probably leftover function
                case (201, 5):
                    tagName = "gender";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("n", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (201, 7): //only in KRko
                    tagName = "gender2";
                    argList.Add(new MsbtTagArgument("arg1", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("arg2", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (201, 8): //only in KRko
                    tagName = "gender3";
                    argList.Add(new MsbtTagArgument("arg1", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("arg2", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (201, 9): //only in BynameAdjective
                    tagName = "titleSubject";
                    break;

                default:
                    tagName = string.Format(TagFormat, tag.Group, tag.Type);
                    UnknownTags.Add(tagName);
                    if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
            }
        }
        catch //a bit conversion has failed
        {
            if (string.IsNullOrEmpty(tagName))
            {
                tagName = string.Format(TagFormat, tag.Group, tag.Type);
                UnknownTags.Add(tagName);
            }
            argList.Clear();
            if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
        }

        functionArgs = argList;
    }
    #endregion

    #region private methods
    private static string ReadString(byte[] data, ref int offset, Encoding encoding)
    {
        if (offset >= data.Length) return string.Empty;

        var length = (int) data[offset];
        offset += 2;
        var result = encoding.GetString(data, offset, length > data.Length - offset ? data.Length - offset : length);
        offset += length;
        return result;
    }

    private static string GetListValue(IDictionary<uint, string> list, uint index) => list.ContainsKey(index) ? list[index] : index.ToString();
    #endregion
}