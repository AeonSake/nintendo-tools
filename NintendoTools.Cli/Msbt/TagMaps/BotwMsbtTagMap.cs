﻿using System.Text;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class BotwMsbtTagMap : ICliMsbtTagMap
{
    #region public properties
    public string TagFormat { get; set; } = "fun_{0:X4}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();
    #endregion

    #region IMsbtFunctionTable interface
    public void GetTag(MsbtTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<MsbtTagArgument> functionArgs)
    {
        tagName = string.Empty;
        var argList = new List<MsbtTagArgument>();
        var argOffset = 0;

        try
        {
            switch (tag.Group, tag.Type)
            {
                case (0, 0):
                    tagName = "ruby";
                    argList.Add(new MsbtTagArgument("charSpan", BitConverter.ToUInt16(tag.Args) / 2));
                    argOffset = 2;
                    argList.Add(new MsbtTagArgument("value", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (0, 1):
                    tagName = "font";
                    argList.Add(new MsbtTagArgument("id", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 2):
                    tagName = "size";
                    argList.Add(new MsbtTagArgument("value", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 3):
                    tagName = "color";
                    argList.Add(new MsbtTagArgument("id", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 4):
                    tagName = "pageBreak";
                    break;

                case (1, 0):
                    tagName = "delay";
                    argList.Add(new MsbtTagArgument("frames", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (1, 2):
                    tagName = "noTextScroll"; //??
                    break;
                case (1, 3):
                    tagName = "autoAdvance"; //??
                    argList.Add(new MsbtTagArgument("frames", BitConverter.ToUInt16(tag.Args)));
                    break;

                case (2, 1):
                case (2, 2):
                    tagName = "number";
                    argList.Add(new MsbtTagArgument("ref", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (2, 3):
                    tagName = "currentHorseName";
                    break;
                case (2, 4):
                    tagName = "selectedHorseName";
                    break;
                case (2, 7):
                    tagName = "cookingAdjective";
                    break;
                case (2, 8):
                    tagName = "cookingEffectCaption";
                    break;
                case (2, 9):
                    tagName = "number";
                    argList.Add(new MsbtTagArgument("ref", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (2, 10):
                    tagName = "shrineMonkName";
                    break;
                case (2, 11):
                case (2, 12):
                    tagName = "string";
                    argList.Add(new MsbtTagArgument("ref", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (2, 13):
                    tagName = "modifierLevel";
                    break;
                case (2, 14):
                case (2, 15):
                case (2, 16):
                case (2, 17):
                case (2, 18):
                case (2, 19):
                    tagName = "number";
                    argList.Add(new MsbtTagArgument("ref", ReadString(tag.Args, ref argOffset, encoding)));
                    break;

                case (4, 0):
                    tagName = "anim";
                    argList.Add(new MsbtTagArgument("type", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (4, 2):
                    tagName = "anim2";
                    argList.Add(new MsbtTagArgument("type", ReadString(tag.Args, ref argOffset, encoding)));
                    break;

                case (5, 0):
                    tagName = "delay1";
                    break;
                case (5, 1):
                    tagName = "delay2";
                    break;
                case (5, 2):
                    tagName = "delay3";
                    break;

                case (201, 0):
                    tagName = "wordInfo";
                    argList.Add(new MsbtTagArgument("gender", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("defArticle", tag.Args[1]));
                    argList.Add(new MsbtTagArgument("indefArticle", tag.Args[2]));
                    argList.Add(new MsbtTagArgument("isPlural", tag.Args[3]));
                    break;
                case (201, 1):
                    tagName = "defArticle"; //definite article from StaticMsg/GrammarArticle determined by a wordInfo function
                    break;
                case (201, 2):
                    tagName = "indefArticle"; //indefinite article from StaticMsg/GrammarArticle determined by a wordInfo function
                    break;
                case (201, 3):
                    tagName = "uppercaseNextWord";
                    break;
                case (201, 4):
                    tagName = "lowercaseNextWord";
                    break;
                case (201, 5):
                    tagName = "gender";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("n", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (201, 6):
                    tagName = "pluralCase";
                    argList.Add(new MsbtTagArgument("arg1", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("arg2", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("arg3", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (201, 7): //only in KRko
                    tagName = "gender7";
                    argList.Add(new MsbtTagArgument("arg1", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("arg2", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (201, 8): //only in KRko
                    tagName = "gender8";
                    argList.Add(new MsbtTagArgument("arg1", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("arg2", ReadString(tag.Args, ref argOffset, encoding)));
                    break;

                default:
                    tagName = string.Format(TagFormat, tag.Group, tag.Type);
                    UnknownTags.Add(tagName);
                    if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
            }
        }
        catch //a bit conversion has failed
        {
            if (string.IsNullOrEmpty(tagName))
            {
                tagName = string.Format(TagFormat, tag.Group, tag.Type);
                UnknownTags.Add(tagName);
            }
            argList.Clear();
            if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
        }

        functionArgs = argList;
    }
    #endregion

    #region private methods
    private static string ReadString(byte[] data, ref int offset, Encoding encoding)
    {
        if (offset >= data.Length) return string.Empty;

        var length = (int) data[offset];
        offset += 2;
        var result = encoding.GetString(data, offset, length > data.Length - offset ? data.Length - offset : length);
        offset += length;
        return result;
    }
    #endregion
}