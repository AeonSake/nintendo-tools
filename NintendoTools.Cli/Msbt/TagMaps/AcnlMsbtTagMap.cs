﻿using System.Text;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class AcnlMsbtTagMap : ICliMsbtTagMap
{
    #region public properties
    public string TagFormat { get; set; } = "fun_{0:X4}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();
    #endregion

    #region IMsbtFunctionTable interface
    public void GetTag(MsbtTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<MsbtTagArgument> functionArgs)
    {
        tagName = string.Empty;
        var argList = new List<MsbtTagArgument>();
        var argOffset = 0;

        try
        {
            switch (tag.Group, tag.Type)
            {
                case (0, 0):
                    tagName = "ruby";
                    argList.Add(new MsbtTagArgument("charSpan", BitConverter.ToUInt16(tag.Args) / 2));
                    argOffset = 2;
                    argList.Add(new MsbtTagArgument("value", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (0, 2):
                    tagName = "size";
                    argList.Add(new MsbtTagArgument("value", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 3):
                    tagName = "color";
                    argList.Add(new MsbtTagArgument("id", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 4):
                    tagName = "pageBreak";
                    break;

                case (3, var anim):
                    tagName = "anim";
                    argList.Add(new MsbtTagArgument("id", anim));
                    break;

                case (7, 0):
                    tagName = "delay";
                    argList.Add(new MsbtTagArgument("frames", BitConverter.ToUInt16(tag.Args)));
                    break;

                case (10, 4):
                    tagName = "i10_4";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 5):
                    tagName = "i10_5";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 6):
                    tagName = "i10_6";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 7):
                    tagName = "i10_7";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 8):
                    tagName = "i10_8";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 9):
                    tagName = "i10_9";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 10):
                    tagName = "i10_10";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 11):
                    tagName = "i10_11";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 12):
                    tagName = "i10_12";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 13):
                    tagName = "i10_13";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (10, 16):
                    tagName = "i10_16";
                    argList.Add(new MsbtTagArgument("arg", BitConverter.ToUInt16(tag.Args)));
                    break;

                case (14, 0):
                    tagName = "wordInfo";
                    argList.Add(new MsbtTagArgument("gender", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("indefArticle", tag.Args[1])); //indefinite article (index to STR_Article)
                    argList.Add(new MsbtTagArgument("defArticle", tag.Args[2]));   //definite article (index to STR_Article)
                    break;

                case (15, _):
                    tagName = "endTag";
                    break;

                case (20, 0):
                    tagName = "i20_0";
                    break;
                case (20, 1):
                    tagName = "i20_1";
                    break;
                case (20, 2):
                    tagName = "i20_2";
                    argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
                case (20, 3):
                    tagName = "i20_3";
                    break;

                default:
                    tagName = string.Format(TagFormat, tag.Group, tag.Type);
                    UnknownTags.Add(tagName);
                    if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
            }
        }
        catch //a bit conversion has failed
        {
            if (string.IsNullOrEmpty(tagName))
            {
                tagName = string.Format(TagFormat, tag.Group, tag.Type);
                UnknownTags.Add(tagName);
            }
            argList.Clear();
            if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
        }

        functionArgs = argList;
    }
    #endregion

    #region private methods
    private static string ReadString(byte[] data, ref int offset, Encoding encoding)
    {
        if (offset >= data.Length) return string.Empty;

        var length = (int) data[offset];
        offset += 2;
        var result = encoding.GetString(data, offset, length > data.Length - offset ? data.Length - offset : length);
        offset += length;
        return result;
    }
    #endregion
}