﻿using System.Text;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class AcnhMsbtTagMap : ICliMsbtTagMap
{
    #region private members
    private static readonly Dictionary<ushort, string> AnimId = new()
    {
        {0, "EmotEnd"},
        {1, "EmotEnd"},
        {2, "Smiling"},
        {3, "HappyFlower"},
        {4, "Love"},
        {5, "WrySmile"},
        {6, "FaintSmile"},
        {7, "BigSmile"},
        {8, "Angry"},
        {9, "Outraged"},
        {10, "Fuming"},
        {11, "Worried"},
        {12, "Sighing"},
        {13, "Thinking"},
        {14, "SadSpiral"},
        {15, "Frantic"},
        {16, "Crying"},
        {17, "BrokenHeart"},
        {18, "OhGeez"},
        {19, "Confused"},
        {20, "Laughing"},
        {21, "HappyDance"},
        {22, "Clapping"},
        {23, "Dance"},
        {24, "EmotEnd"},
        {25, "Shaki"},
        {26, "Shocked"},
        {27, "Aha"},
        {28, "TakenAback"},
        {29, "Surprised"},
        {30, "IdeaBulb"},
        {31, "Oops"},
        {32, "Eh"},
        {33, "Cheering"},
        {34, "Greeting"},
        {35, "Apologize"},
        {36, "Hello"},
        {37, "Nodding"},
        {38, "QuestionMark"},
        {39, "Negative"},
        {40, "Assent"},
        {41, "Explain"},
        {42, "ColdChill"},
        {43, "Shaking"},
        {44, "Sneezing"},
        {45, "Hot"},
        {46, "Sleepy"},
        {47, "Sleep"},
        {48, "EmotEnd"},
        {49, "AbsentMindedness"},
        {50, "Blushing"},
        {51, "Scheming"},
        {52, "Hesitate"},
        {53, "SmugFace"},
        {54, "Grin"},
        {55, "ShyBoy"},
        {56, "Restless"},
        {57, "Serious"},
        {58, "Silent"},
        {59, "Pardon"},
        {60, "NpcShuffle"},
        {61, "NpcHoldout"}
    };
    private static readonly Dictionary<ushort, string> SnpcAnimId = new()
    {
        {0, "GulCollapsed"},
        {1, "GstSurprised"},
        {2, "DodWireless"},
        {3, "DodRoger"},
        {4, "TkkEh"},
        {5, "TkkNodding"},
        {6, "TkkQuestionMark"}
    };
    private static readonly Dictionary<ushort, string> AnimTarget = new()
    {
        {0, "actor"},
        {1, "player"},
        {2, "mainNpc"},
        {3, "subNpc"}
    };
    private static readonly Dictionary<ushort, string> PlaneAnnounce = new()
    {
        {0, "Katsu"},
        {1, "PlaneAnnounceOn"},
        {2, "PlaneAnnounceOff"}
    };
    private static readonly Dictionary<ushort, string> Number = new()
    {
        //{1, "List"}, //used e.g. as date array, quest numbers
        {2, "Count"},
        //{3, "LargeNumber"}, //used during first tent bill dialog, villagers talking about push-ups
        {8, "HHAPoints"},
        {17, "Bells"},
        {20, "TurnipPrice"},
        {22, "Amount"},
        {26, "BuyPrice"},
        {34, "Year"},
        {35, "Month"},
        {36, "Day"},
        {37, "Hour"},
        {38, "Minute"}
    };
    private static readonly Dictionary<ushort, string> Strings = new()
    {
        {0, "..."},
        {1, "PlayerName"},
        {3, "NickName"},
        {5, "Catchphrase"},
        {8, "OtherIslandName"},
        {9, "IslandName"},
        {10, "VillagerName"},
        {15, "VillagerRace"}
    };
    private static readonly Dictionary<ushort, string> Selection = new()
    {
        {0, "SelectedName"},
        {1, "SelectedVillager"},
        {2, "SelectedIslandName"},
        {3, "SelectedDesignName"}
    };
    #endregion

    #region public properties
    public string TagFormat { get; set; } = "fun_{0:X4}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();
    #endregion

    #region IMsbtFunctionTable interface
    public void GetTag(MsbtTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<MsbtTagArgument> functionArgs)
    {
        tagName = string.Empty;
        var argList = new List<MsbtTagArgument>();
        var argOffset = 0;

        try
        {
            switch (tag.Group, tag.Type)
            {
                case (0, 0):
                    tagName = "ruby";
                    argList.Add(new MsbtTagArgument("charSpan", BitConverter.ToUInt16(tag.Args) / 2));
                    argOffset = 2;
                    argList.Add(new MsbtTagArgument("value", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (0, 2):
                    tagName = "size";
                    argList.Add(new MsbtTagArgument("value", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 3):
                    tagName = "color";
                    argList.Add(new MsbtTagArgument("id", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0, 4):
                    tagName = "pageBreak";
                    break;

                case (10, 0):
                    tagName = "delay";
                    argList.Add(new MsbtTagArgument("frames", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (10, 1):
                    tagName = "delay8";
                    break;
                case (10, 2):
                    tagName = "delay20";
                    break;
                case (10, 3):
                    tagName = "delay40";
                    break;
                case (10, 4):
                    tagName = "i10_4";
                    break;
                case (10, 5):
                    tagName = "i10_5";
                    break;
                case (10, 6):
                    tagName = "i10_6";
                    break;
                case (10, 7):
                    tagName = "i10_7";
                    argList.Add(new MsbtTagArgument("arg", BitConverter.ToUInt32(tag.Args)));
                    break;
                case (10, 8):
                    tagName = "i10_8";
                    argList.Add(new MsbtTagArgument("arg", BitConverter.ToUInt32(tag.Args)));
                    break;
                case (10, 9):
                    tagName = "i10_9";
                    break;
                case (10, 10):
                    tagName = "i10_10";
                    break;
                case (10, 11):
                    tagName = "i10_11";
                    break;
                case (10, 12):
                    tagName = "i10_12";
                    break;
                case (10, 13):
                    tagName = "i10_13";
                    break;
                case (10, 14):
                    tagName = "i10_14";
                    argList.Add(new MsbtTagArgument("arg", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (10, 15):
                    tagName = "setNpcTalkChoices";
                    argList.Add(new MsbtTagArgument("chatOption", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("cancelOption", tag.Args[1]));
                    break;
                case (10, 16):
                    tagName = "i10_16";
                    break;

                case (15, _):
                    tagName = "endTag";
                    //args[0..1] has additional data
                    break;

                case (20, 0):
                    tagName = "i20_0";
                    argList.Add(new MsbtTagArgument("arg", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (20, 1):
                    tagName = "i20_1";
                    argList.Add(new MsbtTagArgument("arg", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (20, 2):
                    tagName = "i20_2";
                    argList.Add(new MsbtTagArgument("arg", tag.Args[0]));
                    break;
                case (20, 3):
                    tagName = "i20_3";
                    argList.Add(new MsbtTagArgument("arg", tag.Args[0]));
                    break;

                case (40, var anim):
                    tagName = "anim";
                    argList.Add(new MsbtTagArgument("id", GetListValue(AnimId, anim)));
                    argList.Add(new MsbtTagArgument("target", GetListValue(AnimTarget, tag.Args[0])));
                    argOffset  = 2;
                    argList.Add(new MsbtTagArgument("targetSpec", ReadString(tag.Args, ref argOffset, encoding)));
                    break;

                case (45, var snpcAnim):
                    tagName = "snpcAnim";
                    argList.Add(new MsbtTagArgument("id", GetListValue(SnpcAnimId, snpcAnim)));
                    argList.Add(new MsbtTagArgument("target", GetListValue(AnimTarget, tag.Args[0])));
                    argOffset  = 2;
                    argList.Add(new MsbtTagArgument("targetSpec", ReadString(tag.Args, ref argOffset, encoding)));
                    break;

                case (50, 0):
                    tagName = "wordInfo";
                    argList.Add(new MsbtTagArgument("gender", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("indefArticle", tag.Args[1])); //indefinite article (index to STR_Article)
                    argList.Add(new MsbtTagArgument("defArticle", tag.Args[2]));   //definite article (index to STR_Article)
                    break;
                case (50, 2):
                    tagName = "wordArticle";
                    break;
                case (50, 3):
                    tagName = "capitalizeNextLetter"; //unconfirmed
                    break;
                case (50, 4):
                    tagName = "capitalizeNextWord";
                    break;
                case (50, 5):
                    tagName = "gender1";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 6):
                    tagName = "gender2";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 7):
                    tagName = "gender3";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 8):
                    tagName = "i50_8";
                    argList.Add(new MsbtTagArgument("x", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("y", tag.Args[1]));
                    argOffset  = 2;
                    argList.Add(new MsbtTagArgument("a", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("b", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("c", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 9):
                    tagName = "i50_9";
                    argList.Add(new MsbtTagArgument("x", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("y", tag.Args[1]));
                    argOffset  = 2;
                    argList.Add(new MsbtTagArgument("a", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("b", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("c", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 10):
                    tagName = "gender4";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 14):
                    tagName = "gender5"; //only in KRko
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 17):
                    tagName = "gender8";
                    //first 3 bytes contain more info
                    //byte 0: usually 18, other values possible too
                    //byte 1: only 0-5
                    //byte 2: only 0-2
                    //argList.Add(new MsbtFunctionArgument("arg", args[..2].ToHexString(true)));
                    argOffset  = 4;
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("n", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 20):
                    tagName = "gender6";
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 22):
                    tagName = "wordCase"; //only in EUde, EUnl and EUru
                    argList.Add(new MsbtTagArgument("nom", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("acc", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("dat", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("gen", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 24):
                    tagName = "gender7"; //only in EUru
                    //first 3 bytes contain more info
                    //byte 0: only 1-6
                    //byte 1: only 0-4
                    //byte 2: only 0-3
                    //argList.Add(new MsbtFunctionArgument("arg", args[..2].ToHexString(true)));
                    argOffset  = 4;
                    argList.Add(new MsbtTagArgument("m", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("f", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("n", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("pl", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 25):
                    tagName = "weatherArea";
                    argList.Add(new MsbtTagArgument("north", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("south", ReadString(tag.Args, ref argOffset, encoding)));
                    break;
                case (50, 27):
                    tagName = "i50_27";
                    argList.Add(new MsbtTagArgument("x", tag.Args[0]));
                    argList.Add(new MsbtTagArgument("y", tag.Args[1]));
                    argOffset  = 2;
                    argList.Add(new MsbtTagArgument("a", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("b", ReadString(tag.Args, ref argOffset, encoding)));
                    argList.Add(new MsbtTagArgument("c", ReadString(tag.Args, ref argOffset, encoding)));
                    break;

                case (90, var number):
                    tagName = "number";
                    argList.Add(new MsbtTagArgument("value", GetListValue(Number, number)));
                    if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("index", tag.Args[0])); //sometimes has no args!
                    //args[1] has additional data
                    break;

                case (100, 3):
                    tagName = "planeAnnounce";
                    argList.Add(new MsbtTagArgument("value", GetListValue(PlaneAnnounce, tag.Args[0])));
                    break;

                case (110, var name):
                    tagName = "string";
                    argList.Add(new MsbtTagArgument("value", GetListValue(Strings, name)));
                    break;

                case (115, var selection):
                    tagName = "selection";
                    argList.Add(new MsbtTagArgument("value", GetListValue(Selection, selection)));
                    break;

                case (125, 7):
                    tagName = "ftrColor";
                    break;

                case (135, 0):
                    tagName = "randomBook";
                    break;
                case (135, 1):
                    tagName = "randomDrink";
                    break;
                case (135, 2):
                    tagName = "randomFlower";
                    break;
                case (135, 3):
                    tagName = "randomFood";
                    break;
                case (135, 4):
                    tagName = "randomMusic";
                    break;
                case (135, 5):
                    tagName = "randomObj";
                    break;
                case (135, 6):
                    tagName = "randomSports";
                    break;
                case (135, 7):
                    tagName = "randomSweets";
                    break;
                case (135, 8):
                    tagName = "randomHobby";
                    break;
                case (135, 9):
                    tagName = "randomKKSong";
                    break;
                case (135, 13):
                    tagName = "randomMuseumItem";
                    {
                        var type = BitConverter.ToUInt16(tag.Args);
                        object val = type switch
                        {
                            0 => "fish",
                            1 => "insect",
                            2 => "fossil",
                            3 => "art",
                            _ => type
                        };
                        argList.Add(new MsbtTagArgument("type", val));
                    }
                    break;

                case (140, 4):
                    tagName = "hhaRank";
                    break;
                case (140, 5):
                    tagName = "hhaSituation";
                    break;
                case (140, 6):
                    tagName = "hhaSet";
                    break;
                case (140, 7):
                    tagName = "hhaTheme";
                    break;
                case (140, 8):
                    tagName = "hhaRoomLocation";
                    break;
                case (140, 15):
                    tagName = "creatorId";
                    break;
                case (140, 17):
                    tagName = "designId";
                    break;

                case (198, 0):
                    tagName = "delayByPersonality";
                    argList.Add(new MsbtTagArgument("boy_normal", BitConverter.ToUInt16(tag.Args)));
                    argList.Add(new MsbtTagArgument("boy_active", BitConverter.ToUInt16(tag.Args, 2)));
                    argList.Add(new MsbtTagArgument("boy_pride", BitConverter.ToUInt16(tag.Args, 4)));
                    argList.Add(new MsbtTagArgument("boy_snobby", BitConverter.ToUInt16(tag.Args, 6)));
                    argList.Add(new MsbtTagArgument("girl_normal", BitConverter.ToUInt16(tag.Args, 8)));
                    argList.Add(new MsbtTagArgument("girl_active", BitConverter.ToUInt16(tag.Args, 10)));
                    argList.Add(new MsbtTagArgument("girl_pride", BitConverter.ToUInt16(tag.Args, 12)));
                    argList.Add(new MsbtTagArgument("girl_big_sis", BitConverter.ToUInt16(tag.Args, 14)));
                    break;

                case (199, 3):
                    tagName = "i199_3";
                    argList.Add(new MsbtTagArgument("a", BitConverter.ToUInt16(tag.Args)));
                    break;

                default:
                    tagName = string.Format(TagFormat, tag.Group, tag.Type);
                    UnknownTags.Add(tagName);
                    if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
            }
        }
        catch //a bit conversion has failed
        {
            if (string.IsNullOrEmpty(tagName))
            {
                tagName = string.Format(TagFormat, tag.Group, tag.Type);
                UnknownTags.Add(tagName);
            }
            argList.Clear();
            if (tag.Args.Length > 0) argList.Add(new MsbtTagArgument("arg", tag.Args.ToHexString(true)));
        }

        //fun_0000007D -> bytes[125,0,0,0] -> seems to be random or nearby room furniture
        //fun_0005007D -> bytes[125,0,5,0] -> seems to be clothing the player is wearing
        //fun_0006007D -> bytes[125,0,6,0] -> seems to be clothing a villager owns (or wears?)

        functionArgs = argList;
    }
    #endregion

    #region private methods
    private static string ReadString(byte[] data, ref int offset, Encoding encoding)
    {
        if (offset >= data.Length) return string.Empty;

        var length = (int) data[offset];
        offset += 2;
        var result = encoding.GetString(data, offset, length > data.Length - offset ? data.Length - offset : length);
        offset += length;
        return result;
    }

    private static string GetListValue(IDictionary<ushort, string> list, ushort index) => list.ContainsKey(index) ? list[index] : index.ToString();
    #endregion
}