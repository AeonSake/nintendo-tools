﻿using NintendoTools.FileFormats.Msbt;

namespace NintendoTools.Cli;

internal class CleanMsbtFormatProvider : IMsbtFormatProvider
{
    public string FormatMessage(MsbtMessage message, string rawText) => rawText.Trim();

    public string FormatTag(MsbtMessage message, string tagName, IEnumerable<MsbtTagArgument> arguments) => string.Empty;
}