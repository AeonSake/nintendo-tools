﻿using NintendoTools.FileFormats.Msbt;

namespace NintendoTools.Cli;

internal class CustomMsbtFormatProvider : IMsbtFormatProvider
{
    private readonly IMsbtFormatProvider _formatProvider;
    private readonly string _functionFormat;

    public CustomMsbtFormatProvider(string functionFormat, IMsbtFormatProvider defaultFormatProvider)
    {
        _formatProvider = defaultFormatProvider;
        _functionFormat = functionFormat;
    }

    public string FormatMessage(MsbtMessage message, string rawText) => _formatProvider.FormatMessage(message, rawText);

    public string FormatTag(MsbtMessage message, string functionName, IEnumerable<MsbtTagArgument> arguments) => string.Format(_functionFormat, functionName, arguments.First().Value);
}