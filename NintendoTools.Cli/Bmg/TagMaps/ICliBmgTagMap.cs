﻿using NintendoTools.FileFormats.Bmg;

namespace NintendoTools.Cli;

internal interface ICliBmgTagMap : IBmgTagMap
{
    public string TagFormat { get; set; }

    public ConcurrentCollection<string> UnknownTags { get; }
}