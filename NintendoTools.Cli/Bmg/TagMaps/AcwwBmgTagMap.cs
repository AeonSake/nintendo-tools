﻿using System.Text;
using NintendoTools.FileFormats.Bmg;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class AcwwBmgTagMap : ICliBmgTagMap
{
    #region private members
    private static readonly string[] ReactionValues = {
        "Reset", "Annoyed", "Shocked", "Laughter", "Amazed", "Aggravation", "Distress",
        "Fearful", "Sorrow", "ColdChill", "Joy", "Curiosity", "Inspiration", "Glee",
        "Thought", "Sadness", "Heartbreak", "Mischief", "Sleepy", "Love", "Happiness",
        "Intense", "Worry", "Bashfulness", "Shaken", "Agreement", "Delight", "Resignation",
        "Sighing", "Surprise", "SpreadCards", "PickCard", "PutawayCards", "ReadFortune",
        "GatekeeperSalute", "GatekeeperAttention", "GatekeeperSurprise", "GatekeeperCuriosity",
        "GulliverJoy", "GulliverSleepy", "SleepingYawn", "ResettiAggravation", "ResettiWorry",
        "ResettiColdChill", "ResettiSurprise", "ResettiIntense", "ResettiDistrust",
        "ResettiRage", "Kappn1", "Kappn2", "Kappn3"
        //52-57 seem to be normal animations but sitting and with head turned (npcs at the roost)
        //53 Annoyed but with head turned?, https://youtu.be/Uf3XAKpHrcg?t=43
        //54 Worry but with head turned?, https://youtu.be/Uf3XAKpHrcg?t=47
        //57 only in dog_ and missing1_, maybe sneezing or shocked?
        //58 only in missing1_, animation specific to Katie, https://youtu.be/ABbIqyy45CE?t=106
        //59 only in astro_ and owl_, animation specific to Blathers/Celeste
    };
    private static readonly Dictionary<ushort, string> StringValues = new()
    {
        {0x00, "PlayerName"},
        {0x01, "VillagerName"},
        {0x02, "Catchphrase"},
        {0x03, "TownName"},
        {0x04, "Year"},
        {0x05, "Month"},
        {0x06, "Day"},
        {0x07, "Weekday"},
        {0x08, "Hour"},
        {0x09, "Minute"},
        {0x0A, "Second"},
        {0x0B, "GossipVillager1"},
        {0x0C, "GossipVillager2"},
        {0x0D, "GossipVillager3"},
        {0x0E, "GossipVillager4"},
        //0x0F-0x19 -> string index values
        //0x1A-0x1D -> item names
        {0x1E, "PlayerRememberedAction"},
        {0x1F, "PlayerNickname"},
        //{0x20, "StringDummpyProc32"},
        {0x21, "YayDayCompliment"},
        {0x22, "Hobby"}
    };
    #endregion

    #region public properties
    public string TagFormat { get; set; } = "fun_{0:X2}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();
    #endregion

    #region IMsbtFunctionTable interface
    public void GetTag(BmgTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<BmgTagArgument> functionArgs)
    {
        tagName = string.Empty;
        var argList = new List<BmgTagArgument>();

        try
        {
            switch (tag.Group, tag.Type)
            {
                case (0x00, 0x00):
                    tagName = "heartSymbol";
                    break;
                case (0x00, 0x01):
                    tagName = "musicNoteSymbol";
                    break;
                case (0x00, 0x02):
                    tagName = "playerInitial";
                    break;
                //0x00,0x03-0x05 JPja
                case (0x00, 0x06):
                    tagName = "arrowSymbol";
                    break;
                case (0x00, 0x07):
                    tagName = "starSymbol";
                    break;
                case (0x00, 0x08):
                    tagName = "dropletSymbol";
                    break;
                //0x00,0x09-0x0A only in st_staffroll/JPja

                case (0x01, 0x00):
                    tagName = "pause";
                    argList.Add(new BmgTagArgument("frames", BitConverter.ToUInt16(tag.Args)));
                    break;
                case (0x01, 0x01):
                    tagName = "pressButton";
                    break;
                case (0x01, 0x02):
                    tagName = "disableTextDelay";
                    break;
                case (0x01, 0x03):
                    tagName = "enableTextDelay";
                    break;
                //0x01,0x04 //clearDialogIntro
                //0x01,0x05 //playDialogIntro
                //0x01,0x06 //increaseTextSpeed
                //0x01,0x07 //resetTextSpeed
                case (0x01, 0x08):
                    tagName = "vibrateWeak";
                    break;
                case (0x01, 0x09):
                    tagName = "vibrateMedium";
                    break;
                case (0x01, 0x0A):
                    tagName = "vibrateStrong";
                    break;
                case (0x01, 0x0B):
                    tagName = "shakeDialog";
                    break;
                case (0x01, 0x0C):
                    tagName = "resettiThunderSound";
                    break;
                case (0x01, 0x0D):
                    tagName = "resettiSnareSound";
                    break;
                case (0x01, 0x10):
                    tagName = "resettiInputPrompt";
                    break;

                //fun_02,0x00-0x07 start of dialog options

                //fun_03,0x00-0x03

                case (0x04, var index and >= 0x0F and <= 0x19):
                    tagName = "string";
                    argList.Add(new BmgTagArgument("index", index - 15));
                    break;
                case (0x04, var index and >= 0x1A and <= 0x1D):
                    tagName = "itemName";
                    argList.Add(new BmgTagArgument("index", index - 26));
                    break;
                case (0x04, var index):
                    tagName = "string";
                    argList.Add(new BmgTagArgument("value", GetListValue(StringValues, index)));
                    break;

                //fun_05,0x05 start of dialog options

                //fun_06,0x00-0x0D

                case (0x07, 0x00):
                    tagName = "resetReaction";
                    break;
                case (0x07, var index):
                    tagName = "reaction";
                    argList.Add(new BmgTagArgument("type", GetListValue(ReactionValues, index)));
                    break;

                //0x08,0x00 only in JPja in test_
                //0x08,0x01 only in JPja in test_
                //0x08,0x02

                //0x09,0x00-0x09

                //0x0A,0x00 dialog option? args[2] option ID

                //0x0B,0x00
                //0x0B,0x01
                case (0x0B, 0x02):
                    tagName = "ignoreArticle";
                    break;
                case (0x0B, 0x03):
                    tagName = "capitalizeLetter";
                    break;
                case (0x0B, 0x04):
                    tagName = "gender";
                    var genderStr1 = encoding.GetString(tag.Args).Split('\0');
                    argList.Add(new BmgTagArgument("m", genderStr1[0]));
                    argList.Add(new BmgTagArgument("f", genderStr1[1]));
                    break;
                case (0x0B, >= 0x05 and <= 0x13):
                    tagName = "gender";
                    var genderStr2 = encoding.GetString(tag.Args).Split('\0');
                    argList.Add(new BmgTagArgument("m", genderStr2[0]));
                    argList.Add(new BmgTagArgument("f", genderStr2[1]));
                    argList.Add(new BmgTagArgument("n", genderStr2[2]));
                    break;
                //0x0B,0x14 only in KRko, args[2] has number

                case (0xFF, 0x00):
                    tagName = "color";
                    argList.Add(new BmgTagArgument("id", tag.Args[0]));
                    break;
                case (0xFF, 0x02):
                    tagName = "kanjiReplace";
                    argList.Add(new BmgTagArgument("charNum", tag.Args[0]));
                    argList.Add(new BmgTagArgument("value", encoding.GetString(tag.Args[1..])));
                    break;

                default:
                    tagName = string.Format(TagFormat, tag.Group, tag.Type);
                    UnknownTags.Add(tagName);
                    if (tag.Args.Length > 0) argList.Add(new BmgTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
            }
        }
        catch //a bit conversion has failed
        {
            if (string.IsNullOrEmpty(tagName))
            {
                tagName = string.Format(TagFormat, tag.Group, tag.Type);
                UnknownTags.Add($"{tagName}:{tag.Args[0]}");
            }
            argList.Clear();
            if (tag.Args.Length > 0) argList.Add(new BmgTagArgument("arg", tag.Args.ToHexString(true)));
        }

        functionArgs = argList;
    }
    #endregion

    #region private methods
    private static string GetListValue(IReadOnlyList<string> array, ushort index) => index >= array.Count ? index.ToString() : array[index];

    private static string GetListValue(IDictionary<ushort, string> list, ushort index) => list.ContainsKey(index) ? list[index] : index.ToString();
    #endregion
}