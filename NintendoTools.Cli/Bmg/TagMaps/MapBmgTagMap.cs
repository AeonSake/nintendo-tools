﻿using System.Text;
using NintendoTools.FileFormats.Bmg;

namespace NintendoTools.Cli;

internal class MapBmgTagMap : ICliBmgTagMap
{
    private readonly BmgDynamicTagMap _tagMap;

    public MapBmgTagMap(BmgDynamicTagMap tagMap) => _tagMap = tagMap;

    public string TagFormat { get; set; } = string.Empty;

    public ConcurrentCollection<string> UnknownTags { get; } = new();

    public void GetTag(BmgTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<BmgTagArgument> functionArgs)
    {
        _tagMap.GetTag(tag, bigEndian, encoding, out tagName, out functionArgs);

        if (!_tagMap.TryGetInfo(tag.Group, tag.Type, out _)) UnknownTags.Add(tagName);
    }
}