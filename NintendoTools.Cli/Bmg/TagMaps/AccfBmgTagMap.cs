﻿using System.Text;
using NintendoTools.FileFormats.Bmg;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class AccfBmgTagMap : ICliBmgTagMap
{
    #region private members
    private static readonly Dictionary<ushort, string> ReactionValues = new();
    private static readonly Dictionary<ushort, string> StringValues = new()
    {
        {0x00, "PlayerName"},
        {0x01, "VillagerName"},
        //{0x02, "OtherVillagerName"},
        //{0x03, "OtherVillagerName"}, //???
        //0x04 unused
        {0x05, "TownName"},
        {0x06, "Catchphrase"},
        {0x07, "NickName"},
        //0x08 unused
        //0x09 hobby ???
        //0x0A unused
        //0x0B unused
        {0x0C, "ZodiacSign"}
        //0x0D location ???
        //0x0E+ unused
    };
    private static readonly string[] NpcSpecies = {
        "Cat", "Elephant", "Sheep", "Bear", "Dog", "Squirrel", "Rabbit", "Duck",
        "Hippo", "Wolf", "Mouse", "Pig", "Chicken", "Bull", "Cow", "Bird",
        "Frog", "Crocodile", "Goat", "Tiger", "Anteater", "Koala", "Horse", "Octopus",
        "Lion", "Cub", "Rhino", "Gorilla", "Ostrich", "Kangaroo", "Eagle", "Penguin",
        "Monkey"
    };
    #endregion

    #region public properties
    public string TagFormat { get; set; } = "fun_{0:X2}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();
    #endregion

    #region IMsbtFunctionTable interface
    public void GetTag(BmgTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<BmgTagArgument> functionArgs)
    {
        tagName = string.Empty;
        var argList = new List<BmgTagArgument>();

        try
        {
            switch (tag.Group, tag.Type)
            {
                case (0x02, 0x00):
                    tagName = "resetReaction";
                    break;
                case (0x02, var index):
                    tagName = "reaction";
                    argList.Add(new BmgTagArgument("type", GetListValue(ReactionValues, index)));
                    break;

                case (0x05, var index):
                    tagName = "string";
                    argList.Add(new BmgTagArgument("value", GetListValue(StringValues, index)));
                    break;

                case (0x06, 0x00):
                    tagName = "pause";
                    argList.Add(new BmgTagArgument("frames", BitConverter.ToUInt16(Reverse(tag.Args))));
                    break;
                case (0x06, 0x01):
                    tagName = "pressButton";
                    break;

                //0x0C, 0x02 capitalizeWord ???
                case (0x0C, 0x03):
                    tagName = "capitalizeLetter";
                    break;
                case (0x0C, 0x04):
                    tagName = "gender";
                    var genderStr = encoding.GetString(tag.Args[2..]).Split('\0');
                    argList.Add(new BmgTagArgument("m", genderStr[0]));
                    argList.Add(new BmgTagArgument("f", genderStr[1]));
                    break;

                case (0x0D, var index):
                    tagName = "npcSpecies";
                    argList.Add(new BmgTagArgument("type", GetListValue(NpcSpecies, index)));
                    break;

                case (0xFF, 0x00):
                    tagName = "color";
                    argList.Add(new BmgTagArgument("id", tag.Args[0]));
                    break;
                case (0xFF, 0x01):
                    tagName = "size";
                    argList.Add(new BmgTagArgument("value", BitConverter.ToUInt16(Reverse(tag.Args))));
                    break;
                case (0xFF, 0x02):
                    tagName = "kanjiReplace";
                    argList.Add(new BmgTagArgument("charNum", tag.Args[0]));
                    argList.Add(new BmgTagArgument("value", encoding.GetString(tag.Args[1..]).TrimEnd('\0')));
                    break;

                default:
                    tagName = string.Format(TagFormat, tag.Group, tag.Type);
                    UnknownTags.Add(tagName);
                    if (tag.Args.Length > 0) argList.Add(new BmgTagArgument("arg", tag.Args.ToHexString(true)));
                    break;
            }
        }
        catch //a bit conversion has failed
        {
            if (string.IsNullOrEmpty(tagName))
            {
                tagName = string.Format(TagFormat, tag.Group, tag.Type);
                UnknownTags.Add(tagName);
            }
            argList.Clear();
            if (tag.Args.Length > 0) argList.Add(new BmgTagArgument("arg", tag.Args.ToHexString(true)));
        }

        functionArgs = argList;
    }
    #endregion

    #region private methods
    private static string GetListValue(IReadOnlyList<string> array, ushort index) => index > array.Count ? index.ToString() : array[index];

    private static string GetListValue(IDictionary<ushort, string> list, ushort index) => list.ContainsKey(index) ? list[index] : index.ToString();

    private static T[] Reverse<T>(T[] array)
    {
        Array.Reverse(array);
        return array;
    }
    #endregion
}