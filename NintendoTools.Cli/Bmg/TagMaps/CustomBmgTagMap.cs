﻿using System.Text;
using NintendoTools.FileFormats.Bmg;
using NintendoTools.Utils;

namespace NintendoTools.Cli;

internal class CustomBmgTagMap : ICliBmgTagMap
{
    public string TagFormat { get; set; } = "fun_{0:X2}_{1:X4}";

    public ConcurrentCollection<string> UnknownTags { get; } = new();

    public void GetTag(BmgTag tag, bool bigEndian, Encoding encoding, out string tagName, out IEnumerable<BmgTagArgument> functionArgs)
    {
        tagName = string.Format(TagFormat, tag.Group, tag.Type);
        UnknownTags.Add(tagName);

        var argList = new List<BmgTagArgument>();
        if (tag.Args.Length > 0) argList.Add(new BmgTagArgument("arg", tag.Args.ToHexString(true)));
        functionArgs = argList;
    }
}