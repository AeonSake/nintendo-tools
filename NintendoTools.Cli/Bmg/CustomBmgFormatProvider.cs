﻿using NintendoTools.FileFormats.Bmg;

namespace NintendoTools.Cli;

internal class CustomBmgFormatProvider : IBmgFormatProvider
{
    private readonly IBmgFormatProvider _formatProvider;
    private readonly string _functionFormat;

    public CustomBmgFormatProvider(string functionFormat, IBmgFormatProvider defaultFormatProvider)
    {
        _formatProvider = defaultFormatProvider;
        _functionFormat = functionFormat;
    }

    public string FormatMessage(BmgMessage message, string rawText) => _formatProvider.FormatMessage(message, rawText);

    public string FormatTag(BmgMessage message, string tagName, IEnumerable<BmgTagArgument> arguments) => string.Format(_functionFormat, tagName, arguments.First().Value);
}