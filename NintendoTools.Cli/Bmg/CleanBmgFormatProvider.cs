﻿using NintendoTools.FileFormats.Bmg;

namespace NintendoTools.Cli;

internal class CleanBmgFormatProvider : IBmgFormatProvider
{
    public string FormatMessage(BmgMessage message, string rawText) => rawText.Trim();

    public string FormatTag(BmgMessage message, string tagName, IEnumerable<BmgTagArgument> arguments) => string.Empty;
}