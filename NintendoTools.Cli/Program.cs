using System.Collections.Concurrent;
using System.Globalization;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;
using NintendoTools.Compression.Lz10;
using NintendoTools.Compression.Lz11;
using NintendoTools.Compression.Lz77;
using NintendoTools.Compression.Yay0;
using NintendoTools.Compression.Yaz0;
using NintendoTools.Compression.ZLib;
using NintendoTools.Compression.Zstd;
using NintendoTools.FileFormats;
using NintendoTools.FileFormats.Aamp;
using NintendoTools.FileFormats.Arc;
using NintendoTools.FileFormats.Bcsv;
using NintendoTools.FileFormats.Bmg;
using NintendoTools.FileFormats.Byml;
using NintendoTools.FileFormats.Darc;
using NintendoTools.FileFormats.Msbt;
using NintendoTools.FileFormats.Narc;
using NintendoTools.FileFormats.Rarc;
using NintendoTools.FileFormats.Sarc;
using NintendoTools.FileFormats.Umsbt;
using NintendoTools.Hashing;

namespace NintendoTools.Cli;

internal static class Program
{
    #region main
    private static void Main(string[] args)
    {
        //make sure floating point serialization is done with dot
        Thread.CurrentThread.CurrentCulture = CultureInfo.InvariantCulture;

        var data = CliArgs.Parse(args);

        switch (data.Commands.FirstOrDefault()?.Clean())
        {
            case "bcsv":
                BcsvMain(data);
                break;
            case "bmg":
                BmgMain(data);
                break;
            case "msbt":
                MsbtMain(data);
                break;
            case "umsbt":
                UmsbtMain(data);
                break;
            case "aamp":
                AampMain(data);
                break;
            case "byml" or "byaml":
                BymlMain(data);
                break;
            case "arc" or "u8":
                ArcMain(data);
                break;
            case "darc":
                DarcMain(data);
                break;
            case "narc":
                NarcMain(data);
                break;
            case "rarc":
                RarcMain(data);
                break;
            case "sarc":
                SarcMain(data);
                break;
            case "lz10":
                Lz10Main(data);
                break;
            case "lz11":
                Lz11Main(data);
                break;
            case "lz77":
                Lz77Main(data);
                break;
            case "yay0":
                Yay0Main(data);
                break;
            case "yaz0":
                Yaz0Main(data);
                break;
            case "zlib":
                ZLibMain(data);
                break;
            case "zs" or "zstd":
                ZsMain(data);
                break;
            case "cmd":
                RunCmdFile(data);
                break;
            case "help":
                PrintMainHelp();
                break;
            case "version":
                Console.WriteLine($"NintendoTools version: {Assembly.GetAssembly(typeof(IFileParser))!.GetName().Version}\nCLI version: {Assembly.GetExecutingAssembly().GetName().Version}");
                break;
            case "" or null:
                Console.Write("Missing command. ");
                PrintMainHelp();
                break;
            default:
                Console.Write("Invalid command. ");
                PrintMainHelp();
                break;
        }
    }

    private static void RunCmdFile(CliArgs args)
    {
        if (args.Commands.Count < 2)
        {
            Console.WriteLine("Missing command file.");
            Environment.Exit(1);
        }

        var cmdFile = args.Commands[1];
        if (!File.Exists(cmdFile))
        {
            Console.WriteLine("Command file does not exist.");
            Environment.Exit(1);
        }

        var validCommands = new List<string[]>();
        foreach (var line in File.ReadAllLines(cmdFile))
        {
            if (line.TrimStart().StartsWith('#')) continue;
            validCommands.Add(line.Trim().Split(' '));
        }

        for (var i = 0; i < validCommands.Count; ++i)
        {
            Console.WriteLine($"========== Executing Command {i+1} ==========");
            Main(validCommands[i]);
        }
    }

    private static void PrintMainHelp()
    {
        Console.WriteLine("Available commands:");
        Console.WriteLine("bcsv  : Parse and convert BCSV files");
        Console.WriteLine("bmg   : Parse and convert BMG files");
        Console.WriteLine("msbt  : Parse and convert MSBT files");
        Console.WriteLine("umsbt : Parse and convert UMSBT files");
        Console.WriteLine("aamp  : Parse and convert AAMP files");
        Console.WriteLine("byml  : Parse and convert BYML files");
        Console.WriteLine("arc   : Extract ARC/U8 archives");
        Console.WriteLine("darc  : Extract DARC archives");
        Console.WriteLine("narc  : Extract NARC archives");
        Console.WriteLine("rarc  : Extract RARC archives");
        Console.WriteLine("sarc  : Extract SARC archives");
        Console.WriteLine("lz10  : Decompress LZ10 files");
        Console.WriteLine("lz11  : Decompress LZ11 files");
        Console.WriteLine("lz77  : Decompress LZ77 files");
        Console.WriteLine("yay0  : Decompress Yay0 files");
        Console.WriteLine("yaz0  : Decompress Yaz0 files");
        Console.WriteLine("zlib  : Decompress ZLib files");
        Console.WriteLine("zs    : Decompress ZS/Zstd files");
        Console.WriteLine();
        Console.WriteLine("cmd <file> : Run each line of the given file as command");
        Console.WriteLine();
        Console.WriteLine("Use \"<command> help\" to get help for individual commands.");
    }
    #endregion

    #region bcsv
    private static void BcsvMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "bcsv");
        CheckForHelp(args, PrintBcsvHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);
        var format = GetFormat(args, "json", "json", "xml", "csv");
        var csvSeparator = GetCsvSeparator(args);
        var includeBom = args.Flags.Contains("bom", StringComparer.OrdinalIgnoreCase);

        var headerFile = args.Arguments.GetValue("headers");
        if (headerFile is not null && !File.Exists(headerFile))
        {
            Console.WriteLine("BCSV header file does not exist.");
            Environment.Exit(1);
        }

        var hashFile = args.Arguments.GetValue("hashes");
        if (hashFile is not null && !File.Exists(hashFile))
        {
            Console.WriteLine("BCSV hash file does not exist.");
            Environment.Exit(1);
        }

        //prepare serializer
        IFileSerializer<BcsvFile> serializer = format switch
        {
            "json" => new BcsvJsonSerializer(),
            "xml"  => new BcsvXmlSerializer(),
            "csv"  => new BcsvCsvSerializer {Separator = csvSeparator},
            _      => new BcsvJsonSerializer()
        };

        //parse header file
        var headerInfo = new List<BcsvHeaderInfo>();
        if (headerFile is not null)
        {
            foreach (var line in File.ReadAllLines(headerFile))
            {
                var parts = line.Split(',', ';', '|');
                if (parts.Length is < 2 or > 3)
                {
                    Console.WriteLine("Invalid format for BCSV header file. Please use \"<hash>;<header name>;<format>\" to specify header information.");
                    Console.WriteLine("<header name> is optional. ',' and '|' can also be used as separators.");
                    Environment.Exit(1);
                }

                var headerName = parts[0].Trim();

                if (!Enum.TryParse<BcsvDataType>(parts[^1].Trim(), true, out var headerType))
                {
                    Console.WriteLine("Invalid header data format in BCSV header file.");
                    Console.WriteLine("Available header data formats are: S8, U8, S16, U16, S32, U32, S64, U64, F32, F64, CRC32, MMH3, STRING, DEFAULT");
                    Environment.Exit(1);
                }

                headerInfo.Add(parts.Length == 3 ? new BcsvHeaderInfo(headerName, parts[1].Trim(), headerType) : new BcsvHeaderInfo(headerName, headerType));
            }
        }

        //prepare hash map
        var hashMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        if (hashFile is not null)
        {
            var hashAlg = new Crc32Hash();
            foreach (var line in File.ReadAllLines(hashFile))
            {
                if (string.IsNullOrWhiteSpace(line)) continue;
                hashMap[hashAlg.Compute(line)] = line;
            }
        }

        //parse files
        var parser = new BcsvFileParser(headerInfo);
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Parsing {filePath}...");
            BcsvFile bcsvFile;
            try
            {
                bcsvFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //replace hashes
            for (var i = 0; i < bcsvFile.HeaderInfo.Length; ++i)
            {
                if (bcsvFile.HeaderInfo[i].DataType != BcsvDataType.Crc32) continue;

                for (var j = 0; j < bcsvFile.Rows; ++j)
                {
                    var value = bcsvFile[j, i]?.ToString();
                    if (string.IsNullOrEmpty(value)) continue;

                    if (hashMap.TryGetValue(value, out var name)) bcsvFile[j, i] = name;
                }
            }

            //serialize
            var fileName = Path.Combine(outPath, filePath + "." + format);
            serializer.Serialize(GetTextFileWriter(fileName, includeBom), bcsvFile);
        });
        Console.WriteLine("Done");
    }

    private static void PrintBcsvHelp()
    {
        Console.WriteLine("Usage: \"bcsv <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter>  : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>       : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-format <format>       : Specifies a format for the parsed output file. Available formats are: JSON (default), XML, CSV");
        Console.WriteLine("-separator <separator> : Specifies the CSV separator to use. Only used if format is set to CSV.");
        Console.WriteLine("-headers <file>        : Specifies the path to a header file to use for serialization.");
        Console.WriteLine("                         Each line in the header file contains header information in the format of \"<hash>;<header name>;<format>\".");
        Console.WriteLine("                         <header name> is optional. ',' and '|' can also be used as separators.");
        Console.WriteLine("                         Available header data formats are: S8, U8, S16, U16, S32, U32, S64, U64, F32, F64, CRC32, MMH3, STRING, DEFAULT (default)");
        Console.WriteLine("-hashes <file>         : Specifies the path to a file containing CRC32 hashes to replace cell values with.");
        Console.WriteLine();
        Console.WriteLine("Optional flags:");
        Console.WriteLine("--bom : Write output with BOM (Byte Order Mark).");
    }
    #endregion

    #region bmg
    private static void BmgMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "bmg");
        CheckForHelp(args, PrintBmgHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);
        var format = GetFormat(args, "json", "json", "xml", "csv");
        var csvSeparator = GetCsvSeparator(args);
        var includeBom = args.Flags.Contains("bom", StringComparer.OrdinalIgnoreCase);

        var game = GetGame(args, "acww", "accf");

        var tagMapFile = args.Arguments.GetValue("map");
        ICliBmgTagMap? tagMap = null;
        if (!string.IsNullOrEmpty(tagMapFile))
        {
            if (!File.Exists(tagMapFile))
            {
                Console.WriteLine($"Function map file at {tagMapFile} does not exist.");
                return;
            }

            try
            {
                var map = DynamicTagMapParser.ParseBmgTagMap(File.ReadAllText(tagMapFile));
                tagMap = new MapBmgTagMap(map);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to parse function map file: {ex.Message}");
                return;
            }
        }

        var tagNameFormat = args.Arguments.GetValue("tagName");
        var tagFormat = args.Arguments.GetValue("tagFormat");
        var skipMetaData = !args.Flags.Contains("metadata", StringComparer.OrdinalIgnoreCase);
        var ignoreAttributes = !args.Flags.Contains("attributes", StringComparer.OrdinalIgnoreCase);
        var merge = args.Flags.Contains("merge", StringComparer.OrdinalIgnoreCase);
        var cleanFormat = args.Flags.Contains("clean", StringComparer.OrdinalIgnoreCase);
        var printUnknownTags = args.Flags.Contains("printUnknownTags", StringComparer.OrdinalIgnoreCase);

        //prepare serializer
        IBmgSerializer serializer = format switch
        {
            "json" => new BmgJsonSerializer {SkipMetadata = skipMetaData, IgnoreAttributes = ignoreAttributes},
            "xml"  => new BmgXmlSerializer {SkipMetadata = skipMetaData, IgnoreAttributes = ignoreAttributes},
            "csv"  => new BmgCsvSerializer {Separator = csvSeparator, IgnoreAttributes = ignoreAttributes},
            _      => new BmgJsonSerializer {SkipMetadata = skipMetaData, IgnoreAttributes = ignoreAttributes}
        };
        tagMap ??= game switch
        {
            "acww" => new AcwwBmgTagMap(),
            "accf" => new AccfBmgTagMap(),
            _      => new CustomBmgTagMap()
        };
        if (!string.IsNullOrEmpty(tagNameFormat)) tagMap.TagFormat = tagNameFormat;
        serializer.TagMap = tagMap;
        if (cleanFormat) serializer.FormatProvider = new CleanBmgFormatProvider();
        else if (!string.IsNullOrEmpty(tagFormat)) serializer.FormatProvider = new CustomBmgFormatProvider(tagFormat, serializer.FormatProvider);

        //parse files
        var parser = new BmgFileParser();
        if (merge && Directory.Exists(sourcePath))
        {
            var files = new ConcurrentDictionary<string, byte>();
            var data = new ConcurrentDictionary<string, BmgFile>();

            Parallel.ForEach(Directory.GetDirectories(sourcePath), languageFolder =>
            {
                var language = ParseLanguageFolder(Path.GetFileName(languageFolder));

                Parallel.ForEach(GetFiles(languageFolder, filter), file =>
                {
                    var readStream = File.OpenRead(file);
                    if (!parser.CanParse(readStream))
                    {
                        readStream.Close();
                        return;
                    }

                    //parse file
                    var filePath = Path.GetRelativePath(languageFolder, file);
                    Console.WriteLine($"Parsing {filePath}...");
                    BmgFile bmgFile;
                    try
                    {
                        bmgFile = parser.Parse(readStream);
                    }
                    catch
                    {
                        Console.WriteLine($"Failed to parse {filePath}");
                        readStream.Close();
                        return;
                    }

                    var fileName = Path.Combine(outPath, filePath + "." + format);
                    files.TryAdd(fileName, 0);
                    data.TryAdd(language + fileName, bmgFile);
                });
            });

            Parallel.ForEach(files.Keys, fileName =>
            {
                var bmgFiles = new Dictionary<string, BmgFile>();
                foreach (var languageFolder in Directory.GetDirectories(sourcePath))
                {
                    var language = ParseLanguageFolder(Path.GetFileName(languageFolder))!;
                    var languageFile = language + fileName;
                    if (data.TryGetValue(languageFile, out var file)) bmgFiles.Add(language, file);
                }

                //serialize
                serializer.Serialize(GetTextFileWriter(fileName, includeBom), bmgFiles);
            });
        }
        else
        {
            Parallel.ForEach(GetFiles(sourcePath, filter), file =>
            {
                var readStream = File.OpenRead(file);
                if (!parser.CanParse(readStream))
                {
                    readStream.Close();
                    return;
                }

                //parse file
                var filePath = GetRelativeFilePath(sourcePath, file);
                Console.WriteLine($"Parsing {filePath}...");
                BmgFile bmgFile;
                try
                {
                    bmgFile = parser.Parse(readStream);
                }
                catch
                {
                    Console.WriteLine($"Failed to parse {filePath}");
                    readStream.Close();
                    return;
                }

                //serialize
                var fileName = Path.Combine(outPath, filePath + "." + format);
                serializer.Serialize(GetTextFileWriter(fileName, includeBom), bmgFile);
            });
        }
        Console.WriteLine("Done");

        if (printUnknownTags)
        {
            Console.WriteLine($"\nUnknown tags: {tagMap.UnknownTags.Count}");
            var sortedList = tagMap.UnknownTags.ToList();
            sortedList.Sort(StringComparer.OrdinalIgnoreCase);
            foreach (var function in sortedList) Console.WriteLine(function);
        }
    }

    private static void PrintBmgHelp()
    {
        Console.WriteLine("Usage: \"bmg <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter>  : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>       : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-format <format>       : Specifies a format for the parsed output file. Available formats are: JSON (default), XML, CSV");
        Console.WriteLine("-separator <separator> : Specifies the CSV separator to use. Only used if format is set to CSV.");
        Console.WriteLine("-game <game>           : Specifies a game to use for the tag map. Available values are: ACWW, ACCF");
        Console.WriteLine("-tagName <format>      : Specifies a string format to use for the BMG tag names. This argument is ignored if a game is specified.");
        Console.WriteLine("-tagFormat <format>    : Specifies a string format to use for the BMG tag. This argument is ignored if output is cleaned.");
        Console.WriteLine("-map <file>            : Specifies a custom function map (.mfm) to use. This overrides the -game, -tagName and -tagFormat arguments.");
        Console.WriteLine();
        Console.WriteLine("Optional flags:");
        Console.WriteLine("--metadata         : Include BMG metadata in output. Not available for CSV.");
        Console.WriteLine("--attributes       : Include BMG attributes in output.");
        Console.WriteLine("--merge            : Merges all languages into a single output file. This process requires separate folders for each language in the specified source directory.");
        Console.WriteLine("--clean            : Cleans output from all BMG tag calls or replaces them with relevant values.");
        Console.WriteLine("--printUnknownTags : Print a list of all unknown tags after parsing.");
        Console.WriteLine("--bom              : Write output with BOM (Byte Order Mark).");
    }
    #endregion

    #region msbt
    private static void MsbtMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "msbt");
        CheckForHelp(args, PrintMsbtHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);
        var format = GetFormat(args, "json", "json", "xml", "csv");
        var csvSeparator = GetCsvSeparator(args);
        var includeBom = args.Flags.Contains("bom", StringComparer.OrdinalIgnoreCase);

        var game = GetGame(args, "acnh", "acnl", "splat3", "botw", "totk");

        var tagMapFile = args.Arguments.GetValue("map");
        ICliMsbtTagMap? tagMap = null;
        if (!string.IsNullOrEmpty(tagMapFile))
        {
            if (!File.Exists(tagMapFile))
            {
                Console.WriteLine($"Function map file at {tagMapFile} does not exist.");
                return;
            }

            try
            {
                var map = DynamicTagMapParser.ParseMsbtTagMap(File.ReadAllText(tagMapFile));
                tagMap = new MapMsbtTagMap(map);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to parse function map file: {ex.Message}");
                return;
            }
        }

        var tagNameFormat = args.Arguments.GetValue("tagName");
        var tagFormat = args.Arguments.GetValue("tagFormat");
        var skipMetaData = !args.Flags.Contains("metadata", StringComparer.OrdinalIgnoreCase);
        var skipMessageMetadata = !args.Flags.Contains("messageMetadata", StringComparer.OrdinalIgnoreCase);
        var merge = args.Flags.Contains("merge", StringComparer.OrdinalIgnoreCase);
        var cleanFormat = args.Flags.Contains("clean", StringComparer.OrdinalIgnoreCase);
        var printUnknownTags = args.Flags.Contains("printUnknownTags", StringComparer.OrdinalIgnoreCase);

        //prepare serializer
        IMsbtSerializer serializer = format switch
        {
            "json" => new MsbtJsonSerializer {SkipMetadata = skipMetaData, SkipMessageMetadata = skipMessageMetadata},
            "xml"  => new MsbtXmlSerializer {SkipMetadata = skipMetaData, SkipMessageMetadata = skipMessageMetadata},
            "csv"  => new MsbtCsvSerializer {Separator = csvSeparator, SkipMessageMetadata = skipMessageMetadata},
            _      => new MsbtJsonSerializer {SkipMetadata = skipMetaData, SkipMessageMetadata = skipMessageMetadata}
        };
        tagMap ??= game switch
        {
            "acnh"   => new AcnhMsbtTagMap(),
            "acnl"   => new AcnlMsbtTagMap(),
            "splat3" => new Splat3MsbtTagMap(),
            "botw"   => new BotwMsbtTagMap(),
            "totk"   => new TotkMsbtTagMap(),
            _        => new CustomMsbtTagMap()
        };
        if (!string.IsNullOrEmpty(tagNameFormat)) tagMap.TagFormat = tagNameFormat;
        serializer.TagMap = tagMap;
        if (cleanFormat) serializer.FormatProvider = new CleanMsbtFormatProvider();
        else if (!string.IsNullOrEmpty(tagFormat)) serializer.FormatProvider = new CustomMsbtFormatProvider(tagFormat, serializer.FormatProvider);

        //parse files
        var parser = new MsbtFileParser();
        if (merge && Directory.Exists(sourcePath))
        {
            var files = new ConcurrentDictionary<string, byte>();
            var data = new ConcurrentDictionary<string, MsbtFile>();

            Parallel.ForEach(Directory.GetDirectories(sourcePath), languageFolder =>
            {
                var language = ParseLanguageFolder(Path.GetFileName(languageFolder));

                Parallel.ForEach(GetFiles(languageFolder, filter), file =>
                {
                    var readStream = File.OpenRead(file);
                    if (!parser.CanParse(readStream))
                    {
                        readStream.Close();
                        return;
                    }

                    //parse file
                    var filePath = Path.GetRelativePath(languageFolder, file);
                    Console.WriteLine($"Parsing {filePath}...");
                    MsbtFile msbtFile;
                    try
                    {
                        msbtFile = parser.Parse(readStream);
                    }
                    catch
                    {
                        Console.WriteLine($"Failed to parse {filePath}");
                        readStream.Close();
                        return;
                    }

                    var fileName = Path.Combine(outPath, filePath + "." + format);
                    files.TryAdd(fileName, 0);
                    data.TryAdd(language + fileName, msbtFile);
                });
            });

            Parallel.ForEach(files.Keys, fileName =>
            {
                var msbtFiles = new Dictionary<string, MsbtFile>();
                foreach (var languageFolder in Directory.GetDirectories(sourcePath))
                {
                    var language = ParseLanguageFolder(Path.GetFileName(languageFolder))!;
                    var languageFile = language + fileName;
                    if (data.TryGetValue(languageFile, out var file)) msbtFiles.Add(language, file);
                }

                //serialize
                serializer.Serialize(GetTextFileWriter(fileName, includeBom), msbtFiles);
            });
        }
        else
        {
            Parallel.ForEach(GetFiles(sourcePath, filter), file =>
            {
                var readStream = File.OpenRead(file);
                if (!parser.CanParse(readStream))
                {
                    readStream.Close();
                    return;
                }

                //parse file
                var filePath = GetRelativeFilePath(sourcePath, file);
                Console.WriteLine($"Parsing {filePath}...");
                MsbtFile msbtFile;
                try
                {
                    msbtFile = parser.Parse(readStream);
                }
                catch
                {
                    Console.WriteLine($"Failed to parse {filePath}");
                    readStream.Close();
                    return;
                }

                //serialize
                var fileName = Path.Combine(outPath, filePath + "." + format);
                serializer.Serialize(GetTextFileWriter(fileName, includeBom), msbtFile);
            });
        }
        Console.WriteLine("Done");

        if (printUnknownTags)
        {
            Console.WriteLine($"\nUnknown tags: {tagMap.UnknownTags.Count}");
            var sortedList = tagMap.UnknownTags.ToList();
            sortedList.Sort(StringComparer.OrdinalIgnoreCase);
            foreach (var function in sortedList) Console.WriteLine(function);
        }
    }

    private static void PrintMsbtHelp()
    {
        Console.WriteLine("Usage: \"msbt <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter>  : Specifies a regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>       : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-format <format>       : Specifies a format for the parsed output file. Available formats are: JSON (default), XML, CSV");
        Console.WriteLine("-separator <separator> : Specifies the CSV separator to use. Only used if format is set to CSV.");
        Console.WriteLine("-game <game>           : Specifies a game to use for the tag map. Available values are: ACNH, ACNL, SPLAT3, BOTW, TOTK");
        Console.WriteLine("-tagName <format>      : Specifies a string format to use for the MSBT tag names. This argument is ignored if a game is specified.");
        Console.WriteLine("-tagFormat <format>    : Specifies a string format to use for the MSBT tag. This argument is ignored if output is cleaned.");
        Console.WriteLine("-map <file>            : Specifies a custom function map (.mfm) to use. This overrides the -game, -tagName and -tagFormat arguments.");
        Console.WriteLine();
        Console.WriteLine("Optional flags:");
        Console.WriteLine("--metadata         : Include file metadata in output. Not available for CSV.");
        Console.WriteLine("--messageMetadata  : Include message metadata such as message index, attributes and style index in output, if available.");
        Console.WriteLine("--merge            : Merges all languages into a single output file. This process requires separate folders for each language in the specified source directory.");
        Console.WriteLine("--clean            : Cleans output from all MSBT tag calls or replaces them with relevant values.");
        Console.WriteLine("--printUnknownTags : Print a list of all unknown tags after parsing.");
        Console.WriteLine("--bom              : Write output with BOM (Byte Order Mark).");
    }
    #endregion

    #region umsbt
    private static void UmsbtMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "umsbt");
        CheckForHelp(args, PrintUmsbtHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);
        var format = GetFormat(args, "json", "json", "xml", "csv");
        var csvSeparator = GetCsvSeparator(args);
        var includeBom = args.Flags.Contains("bom", StringComparer.OrdinalIgnoreCase);

        var languages = args.Arguments.GetValue("languages")?.Split(';') ?? new[] {"001", "002", "003", "004", "005", "006", "007", "008", "009", "010", "011", "012", "013", "014", "015", "016", "017", "018", "019", "020"};
        if (languages.Length == 0)
        {
            Console.WriteLine("Missing languages.");
            Environment.Exit(1);
        }

        var game = GetGame(args, "acnl");

        var tagMapFile = args.Arguments.GetValue("map");
        ICliMsbtTagMap? tagMap = null;
        if (!string.IsNullOrEmpty(tagMapFile))
        {
            if (!File.Exists(tagMapFile))
            {
                Console.WriteLine($"Function map file at {tagMapFile} does not exist.");
                return;
            }

            try
            {
                var map = DynamicTagMapParser.ParseMsbtTagMap(File.ReadAllText(tagMapFile));
                tagMap = new MapMsbtTagMap(map);
            }
            catch (Exception ex)
            {
                Console.WriteLine($"Failed to parse function map file: {ex.Message}");
                return;
            }
        }

        var tagNameFormat = args.Arguments.GetValue("tagName");
        var tagFormat = args.Arguments.GetValue("tagFormat");
        var skipMetaData = !args.Flags.Contains("metadata", StringComparer.OrdinalIgnoreCase);
        var skipMessageMetadata = !args.Flags.Contains("messageMetadata", StringComparer.OrdinalIgnoreCase);
        var merge = args.Flags.Contains("merge", StringComparer.OrdinalIgnoreCase);
        var cleanFormat = args.Flags.Contains("clean", StringComparer.OrdinalIgnoreCase);
        var printUnknownTags = args.Flags.Contains("printUnknownTags", StringComparer.OrdinalIgnoreCase);

        //prepare serializer
        IMsbtSerializer serializer = format switch
        {
            "json" => new MsbtJsonSerializer {SkipMetadata = skipMetaData, SkipMessageMetadata = skipMessageMetadata},
            "xml"  => new MsbtXmlSerializer {SkipMetadata = skipMetaData, SkipMessageMetadata = skipMessageMetadata},
            "csv"  => new MsbtCsvSerializer {Separator = csvSeparator, SkipMessageMetadata = skipMessageMetadata},
            _      => new MsbtJsonSerializer {SkipMetadata = skipMetaData, SkipMessageMetadata = skipMessageMetadata}
        };
        tagMap ??= game switch
        {
            "acnl" => new AcnlMsbtTagMap(),
            _      => new CustomMsbtTagMap()
        };
        if (!string.IsNullOrEmpty(tagNameFormat)) tagMap.TagFormat = tagNameFormat;
        serializer.TagMap = tagMap;
        if (cleanFormat) serializer.FormatProvider = new CleanMsbtFormatProvider();
        else if (!string.IsNullOrEmpty(tagFormat)) serializer.FormatProvider = new CustomMsbtFormatProvider(tagFormat, serializer.FormatProvider);

        //parse files
        var parser = new UmsbtFileParser();
        if (merge)
        {
            Parallel.ForEach(GetFiles(sourcePath, filter), file =>
            {
                var readStream = File.OpenRead(file);
                if (!parser.CanParse(readStream))
                {
                    readStream.Close();
                    return;
                }

                //parse file
                var filePath = GetRelativeFilePath(sourcePath, file);
                Console.WriteLine($"Parsing {filePath}...");
                IList<MsbtFile> msbtFiles;
                try
                {
                    msbtFiles = parser.Parse(readStream);
                }
                catch
                {
                    Console.WriteLine($"Failed to parse {filePath}");
                    readStream.Close();
                    return;
                }
                var files = new Dictionary<string, MsbtFile>();
                for (var i = 0; i < msbtFiles.Count; ++i) files.Add(languages[i], msbtFiles[i]);

                //serialize
                var fileName = Path.Combine(outPath, filePath + "." + format);
                serializer.Serialize(GetTextFileWriter(fileName, includeBom), files);
            });
        }
        else
        {
            Parallel.ForEach(GetFiles(sourcePath, filter), file =>
            {
                var readStream = File.OpenRead(file);
                if (!parser.CanParse(readStream))
                {
                    readStream.Close();
                    return;
                }

                //parse file
                var filePath = GetRelativeFilePath(sourcePath, file);
                Console.WriteLine($"Parsing {filePath}...");
                IList<MsbtFile> msbtFiles;
                try
                {
                    msbtFiles = parser.Parse(readStream);
                }
                catch
                {
                    Console.WriteLine($"Failed to parse {filePath}");
                    readStream.Close();
                    return;
                }
                var files = new Dictionary<string, MsbtFile>();
                for (var i = 0; i < msbtFiles.Count; ++i) files.Add(languages[i], msbtFiles[i]);

                //serialize
                Parallel.ForEach(files, kv =>
                {
                    var (language, msbtFile) = kv;
                    var fileName = Path.Combine(outPath, language, filePath + "." + format);
                    serializer.Serialize(GetTextFileWriter(fileName, includeBom), msbtFile);
                });
            });
        }
        Console.WriteLine("Done");

        if (printUnknownTags)
        {
            Console.WriteLine($"\nUnknown tags: {tagMap.UnknownTags.Count}");
            var sortedList = tagMap.UnknownTags.ToList();
            sortedList.Sort(StringComparer.OrdinalIgnoreCase);
            foreach (var function in sortedList) Console.WriteLine(function);
        }
    }

    private static void PrintUmsbtHelp()
    {
        Console.WriteLine("Usage: \"umsbt <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter>  : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>       : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-format <format>       : Specifies a format for the parsed output file. Available formats are: JSON (default), XML, CSV");
        Console.WriteLine("-separator <separator> : Specifies the CSV separator to use. Only used if format is set to CSV.");
        Console.WriteLine("-languages <languages> : Specifies the languages of the UMSBT file in order, separated by a ';'. Example: \"USen;USfr;USes\"");
        Console.WriteLine("-game <game>           : Specifies a game to use for the tag map. Available values are: ACNL");
        Console.WriteLine("-tagName <format>      : Specifies a string format to use for the MSBT tag names. This argument is ignored if a game is specified.");
        Console.WriteLine("-tagFormat <format>    : Specifies a string format to use for the MSBT tag. This argument is ignored if output is cleaned.");
        Console.WriteLine("-map <file>            : Specifies a custom function map (.mfm) to use. This overrides the -game, -tagName and -tagFormat arguments.");
        Console.WriteLine();
        Console.WriteLine("Optional flags:");
        Console.WriteLine("--metadata         : Include file metadata in output. Not available for CSV.");
        Console.WriteLine("--messageMetadata  : Include message metadata such as message index, attributes and style index in output, if available.");
        Console.WriteLine("--merge            : Merges all languages into a single output file.");
        Console.WriteLine("--clean            : Cleans output from all MSBT tag calls or replaces them with relevant values.");
        Console.WriteLine("--printUnknownTags : Print a list of all unknown tags after parsing.");
        Console.WriteLine("--bom              : Write output with BOM (Byte Order Mark).");
    }
    #endregion

    #region aamp
    private static void AampMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "aamp");
        CheckForHelp(args, PrintAampHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);
        var format = GetFormat(args, "json", "json", "yaml");
        var includeBom = args.Flags.Contains("bom", StringComparer.OrdinalIgnoreCase);

        var parameterFile = args.Arguments.GetValue("parameters");
        if (parameterFile is not null && !File.Exists(parameterFile))
        {
            Console.WriteLine("AAMP parameter file does not exist.");
            Environment.Exit(1);
        }

        var hashFile = args.Arguments.GetValue("hashes");
        if (hashFile is not null && !File.Exists(hashFile))
        {
            Console.WriteLine("AAMP hash file does not exist.");
            Environment.Exit(1);
        }

        //prepare serializer
        IFileSerializer<AampFile> serializer = format switch
        {
            "json" => new AampJsonSerializer(),
            "yaml" => new AampYamlSerializer(),
            _      => new AampJsonSerializer()
        };

        //prepare hash maps
        var parameterMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        if (parameterFile is not null)
        {
            var hashAlg = new Crc32Hash();
            foreach (var line in File.ReadAllLines(parameterFile))
            {
                if (string.IsNullOrWhiteSpace(line)) continue;
                parameterMap[hashAlg.Compute(line)] = line;
            }
        }
        var hashMap = new Dictionary<string, string>(StringComparer.OrdinalIgnoreCase);
        if (hashFile is not null)
        {
            var hashAlg = new Crc32Hash();
            foreach (var line in File.ReadAllLines(hashFile))
            {
                if (string.IsNullOrWhiteSpace(line)) continue;
                hashMap[hashAlg.Compute(line)] = line;
            }
        }

        //parse files
        var parser = new AampFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Parsing {filePath}...");
            AampFile aampFile;
            try
            {
                aampFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //replace hashes
            ReplaceListNames(aampFile.Root);

            //serialize
            var fileName = Path.Combine(outPath, filePath + "." + format);
            serializer.Serialize(GetTextFileWriter(fileName, includeBom), aampFile);
        });
        Console.WriteLine("Done");
        return;

        void ReplaceListNames(ParameterList list)
        {
            if (parameterMap.ContainsKey(list.Name)) list.Name = parameterMap[list.Name];

            foreach (var childObj in list.Objects) ReplaceObjectNames(childObj);
            foreach (var childList in list.Lists) ReplaceListNames(childList);
        }
        void ReplaceObjectNames(ParameterObject obj)
        {
            if (parameterMap.ContainsKey(obj.Name)) obj.Name = parameterMap[obj.Name];

            foreach (var parameter in obj.Parameters)
            {
                if (parameterMap.ContainsKey(parameter.Name)) parameter.Name = parameterMap[parameter.Name];

                if (parameter is ValueParameter {Type: ParameterTypes.StringReference, Value: string hash} valueParameter && hashMap.TryGetValue(hash, out var value))
                {
                    valueParameter.Value = value;
                }
            }
        }
    }

    private static void PrintAampHelp()
    {
        Console.WriteLine("Usage: \"aamp <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-format <format>      : Specifies a format for the parsed output file. Available formats are: JSON (default), YAML");
        Console.WriteLine("-parameters <file>    : Specifies the path to a file containing strings to be replace parameter names (CRC32 hashed).");
        Console.WriteLine("-hashes <file>        : Specifies the path to a file containing strings to be replace string reference values (CRC32 hashed).");
        Console.WriteLine();
        Console.WriteLine("Optional flags:");
        Console.WriteLine("--bom : Write output with BOM (Byte Order Mark).");
    }
    #endregion

    #region byml
    private static void BymlMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "byml");
        CheckForHelp(args, PrintBymlHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);
        var format = GetFormat(args, "yaml", "yaml", "json", "xml");
        var includeBom = args.Flags.Contains("bom", StringComparer.OrdinalIgnoreCase);

        //prepare serializer
        IFileSerializer<BymlFile> serializer = format switch
        {
            "yaml" => new BymlYamlSerializer(),
            "json" => new BymlJsonSerializer(),
            "xml"  => new BymlXmlSerializer(),
            _      => new BymlYamlSerializer()
        };

        //parse files
        var parser = new BymlFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Parsing {filePath}...");
            BymlFile bymlFile;
            try
            {
                bymlFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //serialize
            var fileName = Path.Combine(outPath, filePath + "." + format);
            serializer.Serialize(GetTextFileWriter(fileName, includeBom), bymlFile);
        });
        Console.WriteLine("Done");
    }

    private static void PrintBymlHelp()
    {
        Console.WriteLine("Usage: \"byml <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-format <format>      : Specifies a format for the parsed output file. Available formats are: YAML (default), JSON, XML");
        Console.WriteLine();
        Console.WriteLine("Optional flags:");
        Console.WriteLine("--bom : Write output with BOM (Byte Order Mark).");
    }
    #endregion

    #region arc
    private static void ArcMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "arc");
        CheckForHelp(args, PrintArcHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var parser = new ArcFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Extracting {filePath}...");
            ArcFile arcFile;
            try
            {
                arcFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //write files
            var rootPath = Path.Combine(outPath, Path.GetDirectoryName(Path.GetRelativePath(sourcePath, file))!, Path.GetFileNameWithoutExtension(file));
            WriteNodes(arcFile.Content, rootPath);
        });
        Console.WriteLine("Done");
        return;

        void WriteNodes(IEnumerable<IArcNode> nodes, string path)
        {
            Parallel.ForEach(nodes, node =>
            {
                var filePath = Path.Combine(path, node.Name);
                if (node is ArcDirectoryNode dirNode) WriteNodes(dirNode.Children, filePath);
                else if (node is ArcFileNode fileNode) WriteFile(filePath, fileNode.Data);
            });
        }
    }

    private static void PrintArcHelp()
    {
        Console.WriteLine("Usage: \"arc <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region darc
    private static void DarcMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "darc");
        CheckForHelp(args, PrintDarcHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var parser = new DarcFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Extracting {filePath}...");
            DarcFile darcFile;
            try
            {
                darcFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //write files
            var rootPath = Path.Combine(outPath, Path.GetDirectoryName(Path.GetRelativePath(sourcePath, file))!, Path.GetFileNameWithoutExtension(file));
            WriteNodes(darcFile.Content, rootPath);
        });
        Console.WriteLine("Done");
        return;

        void WriteNodes(IEnumerable<IDarcNode> nodes, string path)
        {
            Parallel.ForEach(nodes, node =>
            {
                var filePath = Path.Combine(path, node.Name);
                if (node is DarcDirectoryNode dirNode) WriteNodes(dirNode.Children, filePath);
                else if (node is DarcFileNode fileNode) WriteFile(filePath, fileNode.Data);
            });
        }
    }

    private static void PrintDarcHelp()
    {
        Console.WriteLine("Usage: \"narc <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region narc
    private static void NarcMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "narc");
        CheckForHelp(args, PrintNarcHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var parser = new NarcFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Extracting {filePath}...");
            NarcFile narcFile;
            try
            {
                narcFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //write files
            var rootPath = Path.Combine(outPath, Path.GetDirectoryName(Path.GetRelativePath(sourcePath, file))!, Path.GetFileNameWithoutExtension(file));
            WriteNodes(narcFile.Content, rootPath);
        });
        Console.WriteLine("Done");
        return;

        void WriteNodes(IEnumerable<INarcNode> nodes, string path)
        {
            Parallel.ForEach(nodes, node =>
            {
                var filePath = Path.Combine(path, node.Name);
                if (node is NarcDirectoryNode dirNode) WriteNodes(dirNode.Children, filePath);
                else if (node is NarcFileNode fileNode) WriteFile(filePath, fileNode.Data);
            });
        }
    }

    private static void PrintNarcHelp()
    {
        Console.WriteLine("Usage: \"narc <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region rarc
    private static void RarcMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "rarc");
        CheckForHelp(args, PrintRarcHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var parser = new RarcFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Extracting {filePath}...");
            RarcFile rarcFile;
            try
            {
                rarcFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //write files
            var rootPath = Path.Combine(outPath, Path.GetDirectoryName(Path.GetRelativePath(sourcePath, file))!, Path.GetFileNameWithoutExtension(file));
            WriteNodes(new IRarcNode[] {rarcFile.RootNode}, rootPath);
        });
        Console.WriteLine("Done");
        return;

        void WriteNodes(IEnumerable<IRarcNode> nodes, string path)
        {
            Parallel.ForEach(nodes, node =>
            {
                var filePath = Path.Combine(path, node.Name);
                if (node is RarcDirectoryNode dirNode) WriteNodes(dirNode.Children, filePath);
                else if (node is RarcFileNode fileNode) WriteFile(filePath, fileNode.Data);
            });
        }
    }

    private static void PrintRarcHelp()
    {
        Console.WriteLine("Usage: \"rarc <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region sarc
    private static void SarcMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "sarc");
        CheckForHelp(args, PrintSarcHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var parser = new SarcFileParser();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!parser.CanParse(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Extracting {filePath}...");
            SarcFile sarcFile;
            try
            {
                sarcFile = parser.Parse(readStream);
            }
            catch
            {
                Console.WriteLine($"Failed to parse {filePath}");
                readStream.Close();
                return;
            }

            //write files
            Parallel.ForEach(sarcFile.Files, content =>
            {
                var fileName = Path.Combine(outPath, Path.GetDirectoryName(Path.GetRelativePath(sourcePath, file))!, Path.GetFileNameWithoutExtension(file), content.Name);
                WriteFile(fileName, content.Data);
            });
        });
        Console.WriteLine("Done");
    }

    private static void PrintSarcHelp()
    {
        Console.WriteLine("Usage: \"sarc <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region lz10
    private static void Lz10Main(CliArgs args)
    {
        VerifyMinCommands(args, 2, "lz10");
        CheckForHelp(args, PrintLz10Help);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var decompressor = new Lz10Decompressor();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed", ".lz", ".lz10"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintLz10Help()
    {
        Console.WriteLine("Usage: \"lz10 <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region lz11
    private static void Lz11Main(CliArgs args)
    {
        VerifyMinCommands(args, 2, "lz11");
        CheckForHelp(args, PrintLz11Help);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var decompressor = new Lz11Decompressor();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed", ".lz", ".lz11"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintLz11Help()
    {
        Console.WriteLine("Usage: \"lz11 <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region lz77
    private static void Lz77Main(CliArgs args)
    {
        VerifyMinCommands(args, 2, "lz77");
        CheckForHelp(args, PrintLz77Help);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var decompressor = new Lz77Decompressor();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed", ".lz", ".lz77"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintLz77Help()
    {
        Console.WriteLine("Usage: \"lz77 <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region yay0
    private static void Yay0Main(CliArgs args)
    {
        VerifyMinCommands(args, 2, "yay0");
        CheckForHelp(args, PrintYay0Help);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var decompressor = new Yay0Decompressor();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed", ".yay0"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintYay0Help()
    {
        Console.WriteLine("Usage: \"yay0 <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region yaz0
    private static void Yaz0Main(CliArgs args)
    {
        VerifyMinCommands(args, 2, "yaz0");
        CheckForHelp(args, PrintYaz0Help);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var decompressor = new Yaz0Decompressor();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed", ".yaz0"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintYaz0Help()
    {
        Console.WriteLine("Usage: \"yaz0 <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region zlib
    private static void ZLibMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "zlib");
        CheckForHelp(args, PrintZLibHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        //parse files
        var decompressor = new ZLibDecompressor();
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintZLibHelp()
    {
        Console.WriteLine("Usage: \"zlib <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
    }
    #endregion

    #region zs
    private static void ZsMain(CliArgs args)
    {
        VerifyMinCommands(args, 2, "zs");
        CheckForHelp(args, PrintZsHelp);

        //validate args
        var sourcePath = ValidateSource(args.Commands[1]);
        var filter = GetFilterArg(args);
        var outPath = GetOutArg(args, sourcePath);

        var dictFile = args.Arguments.GetValue("dict");
        if (dictFile is not null && !File.Exists(dictFile))
        {
            Console.WriteLine("Compression dictionary file does not exist.");
            Environment.Exit(1);
        }

        //parse files
        var decompressor = new ZstdDecompressor();
        if (dictFile is not null) decompressor.Dict = File.ReadAllBytes(dictFile);
        Parallel.ForEach(GetFiles(sourcePath, filter), file =>
        {
            var readStream = File.OpenRead(file);
            if (!decompressor.CanDecompress(readStream))
            {
                readStream.Close();
                return;
            }

            //parse file
            var filePath = GetRelativeFilePath(sourcePath, file);
            Console.WriteLine($"Decompressing {filePath}...");
            var decompressedStream = new MemoryStream();
            try
            {
                decompressor.Decompress(readStream, decompressedStream);
            }
            catch
            {
                Console.WriteLine($"Failed to decompress {filePath}");
                readStream.Close();
                return;
            }

            //write file
            var fileName = Path.Combine(outPath, filePath.RemoveExtensions(".decompressed", ".zs", ".zstd"));
            WriteFile(fileName, decompressedStream);
        });
        Console.WriteLine("Done");
    }

    private static void PrintZsHelp()
    {
        Console.WriteLine("Usage: \"zs <file or directory>\"");
        Console.WriteLine();
        Console.WriteLine("Optional arguments:");
        Console.WriteLine("-filter <file filter> : Specifies regex filter to use when searching for files to process (defaults to \".+\").");
        Console.WriteLine("-out <directory>      : Specifies the output directory for the parsed file. If not provided, the parsed file will be in the same location as the input file.");
        Console.WriteLine("-dict <file>          : Specifies a file to read in as Zstandard compression dictionary.");
    }
    #endregion

    #region utils
    private static void VerifyMinCommands(CliArgs args, int min, string command)
    {
        if (args.Commands.Count < min)
        {
            Console.WriteLine($"Missing arguments. Use \"{command} help\" for more information.");
            Environment.Exit(1);
        }
    }

    private static void CheckForHelp(CliArgs args, Action printHelp)
    {
        if (args.Commands[1].Clean() == "help")
        {
            printHelp.Invoke();
            Environment.Exit(0);
        }
    }

    private static string ValidateSource(string source)
    {
        if (!File.Exists(source) && !Directory.Exists(source))
        {
            Console.WriteLine("Source file or directory does not exist.");
            Environment.Exit(1);
        }

        return Path.GetFullPath(source);
    }

    private static Regex GetFilterArg(CliArgs args)
    {
        var filter = args.Arguments.GetValue("filter");
        return new Regex(string.IsNullOrEmpty(filter) ? ".+" : filter);
    }

    private static string GetOutArg(CliArgs args, string sourceFallback)
    {
        var output = args.Arguments.GetValue("out");
        if (string.IsNullOrEmpty(output))
        {
            return Path.GetFullPath(File.Exists(sourceFallback) ? Path.GetDirectoryName(sourceFallback) ?? string.Empty : sourceFallback);
        }

        if (File.Exists(output))
        {
            Console.WriteLine("Specified output cannot be a file.");
            Environment.Exit(1);
        }

        return Path.GetFullPath(output);
    }

    private static string GetFormat(CliArgs args, string fallback, params string[] validFormats)
    {
        var format = args.Arguments.GetValue("format")?.Clean();
        if (string.IsNullOrEmpty(format)) return fallback;

        if (Array.IndexOf(validFormats, format) == -1)
        {
            Console.WriteLine("Invalid output format.");
            Environment.Exit(1);
        }

        return format;
    }

    private static string? GetGame(CliArgs args, params string[] validGames)
    {
        var game = args.Arguments.GetValue("game")?.Clean();
        if (string.IsNullOrEmpty(game)) return null;

        if (Array.IndexOf(validGames, game) == -1)
        {
            Console.WriteLine("Invalid game type.");
            Environment.Exit(1);
        }

        return game;
    }

    private static string GetCsvSeparator(CliArgs args)
    {
        var separator = args.Arguments.GetValue("separator");
        if (string.IsNullOrEmpty(separator)) return ",";

        if (separator.Contains('"'))
        {
            Console.WriteLine("Invalid CSV separator.");
            Environment.Exit(1);
        }

        return separator;
    }

    private static string GetRelativeFilePath(string source, string file) => file == source ? Path.GetFileName(file) : Path.GetRelativePath(source, file);

    private static string RemoveExtensions(this string fileName, string fallback, params string[] extensions)
    {
        var extensionIndex = fileName.LastIndexOf('.');
        if (extensionIndex == -1 || fileName.LastIndexOf('.', extensionIndex) == -1) return fileName + fallback;

        foreach (var extension in extensions)
        {
            if (fileName[extensionIndex..] == extension) return fileName[..extensionIndex];
        }

        return fileName + fallback;
    }

    private static void WriteFile(string fileName, Stream content)
    {
        Directory.CreateDirectory(Path.GetDirectoryName(fileName)!);
        using var writeStream = File.Create(fileName);
        content.Seek(0, SeekOrigin.Begin);
        content.CopyTo(writeStream);
        writeStream.Flush();
        writeStream.Close();
    }

    private static void WriteFile(string fileName, byte[] content)
    {
        Directory.CreateDirectory(Path.GetDirectoryName(fileName)!);
        File.WriteAllBytes(fileName, content);
    }

    private static StreamWriter GetTextFileWriter(string fileName, bool includeBom)
    {
        Directory.CreateDirectory(Path.GetDirectoryName(fileName)!);
        return new StreamWriter(File.Create(fileName), new UTF8Encoding(includeBom));
    }

    private static string Clean(this string str) => str.ToLowerInvariant().Replace("_", "");

    private static string? GetValue(this IDictionary<string, string> dict, string key) => dict.ContainsKey(key) ? dict[key] : default;

    private static IEnumerable<string> GetFiles(string directory, Regex filter)
    {
        var files = new List<string>();
        if (File.Exists(directory)) files.Add(directory);
        else files.AddRange(Directory.GetFiles(directory, "*", SearchOption.AllDirectories).Where(file => filter.IsMatch(file)));
        return files;
    }

    private static string? ParseLanguage(string? language)
    {
        switch (language?.ToLowerInvariant().Replace("-", "").Replace("_", ""))
        {
            case "en":
            case "usen":
            case "enus":
                return "USen";
            case "usfr":
            case "frus":
            case "frca":
                return "USfr";
            case "uses":
            case "esus":
            case "esmx":
                return "USes";
            case "euen":
            case "eneu":
            case "engb":
                return "EUen";
            case "de":
            case "eude":
            case "deeu":
            case "dede":
                return "EUde";
            case "fr":
            case "eufr":
            case "freu":
                return "EUfr";
            case "es":
            case "eues":
            case "eseu":
            case "eses":
                return "EUes";
            case "it":
            case "euit":
            case "iteu":
            case "itit":
                return "EUit";
            case "nl":
            case "eunl":
            case "nleu":
            case "nlnl":
                return "EUnl";
            case "ru":
            case "euru":
            case "rueu":
            case "ruru":
                return "EUru";
            case "ko":
            case "kr":
            case "krko":
            case "kokr":
                return "KRko";
            case "jp":
            case "ja":
            case "jpja":
            case "jajp":
                return "JPja";
            case "cn":
            case "zh":
            case "cnzh":
            case "zhcn":
                return "CNzh";
            case "tw":
            case "twzh":
            case "zhtw":
                return "TWzh";
            default:
                return language;
        }
    }

    private static string? ParseLanguageFolder(string folderName)
    {
        var match = Regex.Match(folderName, @"(\w{4})\.Product\.\d{3,4}");
        return ParseLanguage(match.Success ? match.Groups[1].Value : folderName);
    }
    #endregion
}