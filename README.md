# Nintendo Tools
General purpose extraction, parsing and deserialization tools for Nintendo file formats.

## Features
The CLI currently only supports parsing/unpacking files.

| File Type | Read | Write | CLI |
|-----------|------|-------|-----|
| AAMP*     | Yes  | No    | No  |
| ARC/U8    | Yes  | Yes   | Yes |
| BCSV      | Yes  | No    | No  |
| BMG       | Yes  | Yes   | Yes |
| BYML      | Yes  | No    | No  |
| DARC      | Yes  | Yes   | Yes |
| MSBP      | Yes  | No    | No  |
| MSBT      | Yes  | Yes   | Yes |
| NARC      | Yes  | Yes   | Yes |
| RARC      | Yes  | Yes   | Yes |
| SARC      | Yes  | Yes   | Yes |
| UMSBT     | Yes  | Yes   | Yes |

| Compression Type    | Decompress | Compress | CLI |
|---------------------|------------|----------|-----|
| LZ10                | Yes        | Yes      | Yes |
| LZ11                | Yes        | Yes      | Yes |
| LZ77                | Yes        | Yes      | Yes |
| Yay0                | Yes        | Yes      | Yes |
| Yaz0                | Yes        | Yes      | Yes |
| ZLib                | Yes        | Yes      | Yes |
| ZSTD                | Yes        | Yes      | Yes |

Hashing Algorithms: CRC32, CRC32C, MMH3

[*] Not yet fully implemented.

## Requirements
- `.NET Standard 2.1` or later

## NuGet Package
This library is available as a NuGet package. There are two ways of including this library in your project.

### Global NuGet Source
1. Create a [personal access token](https://gitlab.com/-/profile/personal_access_tokens) with the `api` scope
2. Open console (cmd, PowerShell, etc.) and add a new NuGet source with the following command:
```shell
nuget source add -Name <source_name> -Source "https://gitlab.com/api/v4/projects/25780465/packages/nuget/index.json" -UserName <user_name> -Password <access_token>
```
`source_name` is the displayed name for the NuGet source in the source list dropdown, `user_name` is your GitLab username.  
If `nuget` is not configured as a global path variable you can also use `dotnet nuget` with the following command:
```shell
dotnet nuget add source "https://gitlab.com/api/v4/projects/25780465/packages/nuget/index.json" --name <source_name> --username <user_name> --password <access_token>
```

### Project NuGet Source
Add a new file called `nuget.config` to the root of your .NET solution (same folder as the `.sln` file) with the following content:
```xml
<?xml version="1.0" encoding="utf-8"?>
<configuration>
    <packageSources>
        <add key="source_name" value="https://gitlab.com/api/v4/projects/25780465/packages/nuget/index.json" />
    </packageSources>
</configuration>
```
`source_name` is the displayed name for the NuGet source in the source list dropdown.



After these steps the `NintendoTools` NuGet package will be available in your project to fetch/update just like a normal NuGet package. Make sure to also select the correct source (the name you have selected for it) to see the package and any updates.

For further information see the [full documentation](https://docs.gitlab.com/ee/user/packages/nuget_repository/#add-the-package-registry-as-a-source-for-nuget-packages).

## Building from Source
Building this project requires `.NET 5.0 SDK` or newer, or `.NET Core 3.0` or newer. Refer to the [full list of supported frameworks](https://learn.microsoft.com/en-us/dotnet/standard/net-standard?tabs=net-standard-2-1) to see more.  
Some packages are loaded from external sources (nuget.org) and are required for compilation. Make sure to run `nuget restore` (or `Restore NuGet packages` from inside Visual Studio) before building.

## Credits and Licenses
The following list contains all external tools and libraries. The project itself is licensed under [GPL-3.0](LICENSE).
- [AuroraLib.Compression](https://github.com/Venomalia/AuroraLib.Compression) ([MIT](https://github.com/Venomalia/AuroraLib.Compression/blob/main/LICENSE))
- [Crc32.NET](https://github.com/force-net/Crc32.NET) ([MIT](https://github.com/force-net/Crc32.NET/blob/develop/LICENSE))
- [ZstdSharp](https://github.com/oleg-st/ZstdSharp) ([MIT](https://github.com/oleg-st/ZstdSharp/blob/master/LICENSE))
- [Json.NET](https://github.com/JamesNK/Newtonsoft.Json) ([MIT](https://github.com/JamesNK/Newtonsoft.Json/blob/master/LICENSE.md))